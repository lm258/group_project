-- phpMyAdmin SQL Dump
-- version 3.4.10.1deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Feb 25, 2014 at 11:29 PM
-- Server version: 5.5.34
-- PHP Version: 5.3.10-1ubuntu3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `raven`
--

-- --------------------------------------------------------

--
-- Table structure for table `configuration`
--

CREATE TABLE IF NOT EXISTS `configuration` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `setting` varchar(200) NOT NULL,
  `value` varchar(200) NOT NULL,
  `default_value` varchar(200) NOT NULL,
  `description` text NOT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Table structure for table `job`
--

CREATE TABLE IF NOT EXISTS `job` (
  `id` int(11) NOT NULL,
  `name` varchar(200) NOT NULL,
  `description` varchar(500) NOT NULL,
  `rating` int(11) NOT NULL DEFAULT '3',
  `flagged` tinyint(1) NOT NULL DEFAULT '0',
  `total_simulations` int(11) NOT NULL DEFAULT '0',
  `completed_simulations` int(11) NOT NULL DEFAULT '0',
  `rejected_simulations` int(11) NOT NULL DEFAULT '0',
  `failed_simulations` int(11) NOT NULL DEFAULT '0',
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `started_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `finished_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `job_objective`
--

CREATE TABLE IF NOT EXISTS `job_objective` (
  `job_id` int(11) NOT NULL,
  `objective_id` int(11) NOT NULL,
  `id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `job_id` (`job_id`),
  KEY `objective_id` (`objective_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `job_parameter`
--

CREATE TABLE IF NOT EXISTS `job_parameter` (
  `id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `parameter_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `job_id` (`job_id`),
  KEY `parameter_id` (`parameter_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Stand-in structure for view `job_users`
--
CREATE TABLE IF NOT EXISTS `job_users` (
`id` bigint(20) unsigned
,`job_id` int(11)
,`username` varchar(300)
,`email` varchar(500)
,`role` varchar(200)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `job_view`
--
CREATE TABLE IF NOT EXISTS `job_view` (
`id` int(11)
,`name` varchar(200)
,`description` varchar(500)
,`rating` int(11)
,`flagged` tinyint(1)
,`total_simulations` int(11)
,`completed_simulations` int(11)
,`rejected_simulations` int(11)
,`failed_simulations` int(11)
,`created_time` timestamp
,`started_time` timestamp
,`finished_time` timestamp
,`users` bigint(21)
);
-- --------------------------------------------------------

--
-- Table structure for table `objective`
--

CREATE TABLE IF NOT EXISTS `objective` (
  `id` int(11) NOT NULL,
  `name` text NOT NULL,
  `maximise` tinyint(1) NOT NULL DEFAULT '0',
  `expression` varchar(255) DEFAULT '',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Stand-in structure for view `objective_names`
--
CREATE TABLE IF NOT EXISTS `objective_names` (
`job_id` int(11)
,`name` text
,`objective_id` int(11)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `param`
--
CREATE TABLE IF NOT EXISTS `param` (
`value` double
,`job_id` int(11)
,`simulation_id` int(11)
,`name` varchar(5)
,`Bleh` varchar(341)
);
-- --------------------------------------------------------

--
-- Table structure for table `parameter`
--

CREATE TABLE IF NOT EXISTS `parameter` (
  `id` int(11) NOT NULL,
  `name` varchar(5) DEFAULT NULL,
  `distribution` varchar(255) DEFAULT '',
  `base_parameter` tinyint(1) NOT NULL DEFAULT '0',
  `distribution_type` varchar(255) DEFAULT 'UNIFORM_REAL',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `parameter_id_name_index` (`id`,`name`),
  KEY `parameter_id_name_index2` (`id`,`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `parameter_value`
--

CREATE TABLE IF NOT EXISTS `parameter_value` (
  `id` int(11) NOT NULL,
  `simulation_id` int(11) NOT NULL,
  `parameter_id` int(11) NOT NULL,
  `value` double NOT NULL,
  PRIMARY KEY (`id`),
  KEY `simulation_id` (`simulation_id`),
  KEY `parameter_id` (`parameter_id`),
  KEY `simulation_parameter_index` (`simulation_id`,`parameter_id`),
  KEY `simulation_parameter_index2` (`simulation_id`,`parameter_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Stand-in structure for view `param_names`
--
CREATE TABLE IF NOT EXISTS `param_names` (
`job_id` int(11)
,`parameter_id` int(11)
,`name` varchar(5)
,`distribution` varchar(255)
);
-- --------------------------------------------------------

--
-- Table structure for table `score`
--

CREATE TABLE IF NOT EXISTS `score` (
  `id` int(11) NOT NULL,
  `simulation_id` int(11) NOT NULL,
  `objective_id` int(11) NOT NULL,
  `score` double NOT NULL,
  PRIMARY KEY (`id`),
  KEY `simulation_id` (`simulation_id`),
  KEY `objective_id` (`objective_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `simulation`
--

CREATE TABLE IF NOT EXISTS `simulation` (
  `id` int(11) NOT NULL,
  `job_id` int(11) NOT NULL,
  `probability` double DEFAULT NULL,
  `error_details` text,
  `total_misfit` double DEFAULT NULL,
  `issue_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `started_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `finished_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `uploaded_time` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `is_finished` tinyint(1) NOT NULL DEFAULT '1',
  `state` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  KEY `job_id` (`job_id`),
  KEY `simulation_id_job_index` (`id`,`job_id`),
  KEY `simulation_id_job_index3` (`id`,`job_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(300) NOT NULL,
  `password` varchar(400) NOT NULL,
  `email` varchar(500) NOT NULL,
  `role` varchar(200) NOT NULL,
  `security_token` varchar(200) NOT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=38 ;

-- --------------------------------------------------------

--
-- Table structure for table `users_jobs`
--

CREATE TABLE IF NOT EXISTS `users_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `job_id` int(11) NOT NULL,
  `user_id` bigint(20) unsigned NOT NULL,
  UNIQUE KEY `id` (`id`),
  UNIQUE KEY `job_id` (`job_id`,`user_id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=418 ;

-- --------------------------------------------------------

--
-- Stand-in structure for view `users_view`
--
CREATE TABLE IF NOT EXISTS `users_view` (
`id` bigint(20) unsigned
,`username` varchar(300)
,`password` varchar(400)
,`email` varchar(500)
,`role` varchar(200)
,`security_token` varchar(200)
,`jobs` bigint(21)
);
-- --------------------------------------------------------

--
-- Stand-in structure for view `user_jobs`
--
CREATE TABLE IF NOT EXISTS `user_jobs` (
`id` int(11)
,`name` varchar(200)
,`description` varchar(500)
,`rating` int(11)
,`flagged` tinyint(1)
,`total_simulations` int(11)
,`completed_simulations` int(11)
,`rejected_simulations` int(11)
,`failed_simulations` int(11)
,`created_time` timestamp
,`started_time` timestamp
,`finished_time` timestamp
,`user_id` bigint(20) unsigned
);
-- --------------------------------------------------------

--
-- Structure for view `job_users`
--
DROP TABLE IF EXISTS `job_users`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `job_users` AS select `users`.`id` AS `id`,`users_jobs`.`job_id` AS `job_id`,`users`.`username` AS `username`,`users`.`email` AS `email`,`users`.`role` AS `role` from (`users` join `users_jobs`) where (`users`.`id` = `users_jobs`.`user_id`);

-- --------------------------------------------------------

--
-- Structure for view `job_view`
--
DROP TABLE IF EXISTS `job_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `job_view` AS select `job`.`id` AS `id`,`job`.`name` AS `name`,`job`.`description` AS `description`,`job`.`rating` AS `rating`,`job`.`flagged` AS `flagged`,`job`.`total_simulations` AS `total_simulations`,`job`.`completed_simulations` AS `completed_simulations`,`job`.`rejected_simulations` AS `rejected_simulations`,`job`.`failed_simulations` AS `failed_simulations`,`job`.`created_time` AS `created_time`,`job`.`started_time` AS `started_time`,`job`.`finished_time` AS `finished_time`,count(`users_jobs`.`user_id`) AS `users` from (`job` left join `users_jobs` on((`job`.`id` = `users_jobs`.`job_id`))) group by `job`.`id`;

-- --------------------------------------------------------

--
-- Structure for view `objective_names`
--
DROP TABLE IF EXISTS `objective_names`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `objective_names` AS select `job_objective`.`job_id` AS `job_id`,`objective`.`name` AS `name`,`job_objective`.`objective_id` AS `objective_id` from (`job_objective` join `objective`) where (`job_objective`.`objective_id` = `objective`.`id`);

-- --------------------------------------------------------

--
-- Structure for view `param`
--
DROP TABLE IF EXISTS `param`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `param` AS select `parameter_value`.`value` AS `value`,`simulation`.`job_id` AS `job_id`,`parameter_value`.`simulation_id` AS `simulation_id`,`parameter`.`name` AS `name`,(select group_concat(`score`.`score` separator ',') from `score` where (`score`.`simulation_id` = `parameter_value`.`simulation_id`)) AS `Bleh` from ((`parameter_value` join `simulation` on((`parameter_value`.`simulation_id` = `simulation`.`id`))) join `parameter` on((`parameter_value`.`parameter_id` = `parameter`.`id`)));

-- --------------------------------------------------------

--
-- Structure for view `param_names`
--
DROP TABLE IF EXISTS `param_names`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `param_names` AS select `job_parameter`.`job_id` AS `job_id`,`job_parameter`.`parameter_id` AS `parameter_id`,`parameter`.`name` AS `name`,`parameter`.`distribution` AS `distribution` from (`job_parameter` join `parameter`) where (`job_parameter`.`parameter_id` = `parameter`.`id`);

-- --------------------------------------------------------

--
-- Structure for view `users_view`
--
DROP TABLE IF EXISTS `users_view`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `users_view` AS select `users`.`id` AS `id`,`users`.`username` AS `username`,`users`.`password` AS `password`,`users`.`email` AS `email`,`users`.`role` AS `role`,`users`.`security_token` AS `security_token`,count(`users_jobs`.`user_id`) AS `jobs` from (`users` left join `users_jobs` on((`users_jobs`.`user_id` = `users`.`id`))) group by `users`.`id`;

-- --------------------------------------------------------

--
-- Structure for view `user_jobs`
--
DROP TABLE IF EXISTS `user_jobs`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `user_jobs` AS select `job`.`id` AS `id`,`job`.`name` AS `name`,`job`.`description` AS `description`,`job`.`rating` AS `rating`,`job`.`flagged` AS `flagged`,`job`.`total_simulations` AS `total_simulations`,`job`.`completed_simulations` AS `completed_simulations`,`job`.`rejected_simulations` AS `rejected_simulations`,`job`.`failed_simulations` AS `failed_simulations`,`job`.`created_time` AS `created_time`,`job`.`started_time` AS `started_time`,`job`.`finished_time` AS `finished_time`,`users_jobs`.`user_id` AS `user_id` from (`users_jobs` left join `job` on((`job`.`id` = `users_jobs`.`job_id`)));

--
-- Constraints for dumped tables
--

--
-- Constraints for table `job_objective`
--
ALTER TABLE `job_objective`
  ADD CONSTRAINT `job_objective_ibfk_1` FOREIGN KEY (`job_id`) REFERENCES `job` (`id`),
  ADD CONSTRAINT `job_objective_ibfk_2` FOREIGN KEY (`objective_id`) REFERENCES `objective` (`id`);

--
-- Constraints for table `job_parameter`
--
ALTER TABLE `job_parameter`
  ADD CONSTRAINT `job_parameter_ibfk_1` FOREIGN KEY (`job_id`) REFERENCES `job` (`id`),
  ADD CONSTRAINT `job_parameter_ibfk_2` FOREIGN KEY (`parameter_id`) REFERENCES `parameter` (`id`);

--
-- Constraints for table `parameter_value`
--
ALTER TABLE `parameter_value`
  ADD CONSTRAINT `parameter_value_ibfk_1` FOREIGN KEY (`simulation_id`) REFERENCES `simulation` (`id`),
  ADD CONSTRAINT `parameter_value_ibfk_2` FOREIGN KEY (`parameter_id`) REFERENCES `parameter` (`id`);

--
-- Constraints for table `simulation`
--
ALTER TABLE `simulation`
  ADD CONSTRAINT `simulation_ibfk_1` FOREIGN KEY (`job_id`) REFERENCES `job` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
