!!!IMPORTANT!!!!
---------------------------
After cloning the repo it is important to run the makefile.
just type make in the top level folder.
This will automagically create the admin user with details
username: admin
password: elephant

It will also create the simulation database ( with no data! ) with all the views for optimization etc.

This is the group project repository.
Here i have created two folders.

java: This holds the java daemon program.

www: This holds the www folder of our project.

The project has been tested on ubuntu 12.04 LTS Server.
Cake PHP Version is 2.4.2
Java version is 1.7
Javac Version is 1.7
PHP Version is 5.3.10
MySQL Version is 5.5.34

When making AJAX requests the following urls can be used

This is to get points
/ajax/getPoints
Arguments in POST request are:
x,y,width,height,auth_token,graph_id,userid

This is to delete a security token.
Its used on logout , so next time they login the user can get a new token
/ajax/deleteToken
Arguments in POST
userid,auth_token

This is to initiate the graph .. this MUST be called first before any points can be retreived
/ajax/initGraph 
Arguments in POST are:
user_id,
job_id,
paramOne,
paramTwo,
auth_token,



-----------NOTE---------------
In order for the application to work on the server
mod_proxy MUST be enabled!
sudo a2enmod proxy_http

After this a director in /etc/apache2/sites-available/default
Must be added that is:

ProxyRequests off

<proxy *>
	Order deny,allow
	Allow from all
</proxy>

ProxyPass /ajax http://localhost:8080/
ProxyPassReverse /ajax http://localhost:8080/

This should go just below the DocumentRoot directive.

Another important thing to note is that
www/app/Config
www/app/Model
www/app/tmp

should all be readable by all so that will be
sudo chmod -R 775 www/app/Config
sudo chmod -R 775 www/app/Model
etc 
