/*
 * (c) Lewis Maitland 2013
 * --------------------------------------------------
 * This class is used to represent a Cluster in the system.
 * It will have a mass ( ie the number of points it represents ).
 * And it will also have a score , which is the TOTAL score of 
 * all points within it.
 *  
*/
class Cluster implements java.io.Serializable {
	
	private float score;
	private int mass;
	private float avgScore;
	private float[] coords;
	
	//set our score and our mass
	public Cluster ( float[] coords , float score , int mass ){
		this.score = score;
		this.mass = mass;
		this.coords = coords;
	}
	
	//this is our getters
	public int getMass(){
		return this.mass;
	}
	
	public float[] getCoords(){
		return coords;
	}
	
	public float getScore(){
		return this.score;
	}
	
	public float getAverageScore(){
		return (this.score/this.mass);
	}
}
