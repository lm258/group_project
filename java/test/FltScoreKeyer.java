/*
 * (c) Lewis Maitland 2013
 * --------------------------------------------------
 * This class is used for merging and adding keys to our
 * clusters of points. It is designed so that when merged
 * the keys will be added , ie the score added together.
 *  
 * [ scores1 , scores 2 , .... ]
*/
import com.tomgibara.cluster.gvm.flt.*;

public class FltScoreKeyer implements FltKeyer< float[]> {

	//this function is effectivley used to add keys to a clusters
	//here we will jus add the scores
	@Override
	public float[] addKey( FltCluster< float[] > cluster, float[] key) {
		
		//get the score array from the cluster key
		float[] k = cluster.getKey();

		//if we didnt have a key for the cluster just return our current one
		if ( k == null ){
			return key;
		}
		
		//i put a try catch here incase we hit an array index out of bounds error
		try{
			
			//otherwise loop through our array adding one to the other
			for ( int  i = 0 ; i < k.length ; i ++ ){
				
				//add the values together
				k[i]+=key[i];
			}
			
		}catch ( Exception e ){
			
			//print the error and return the new key
			System.out.println("ERROR: Key sizes are different exception has been thrown.");
			
			return key;
		}
		
		//return the new key value
		return k;
	}

	//this function is used for merging the keys of two clusters
	//in this case we would want to add them together
	@Override
	public float[] mergeKeys( FltCluster< float[] > c1, FltCluster<float[]> c2) {
		
		if ( c1.getKey().length != 0 && c2.getKey().length != 0 ){
			
			try {
				
				//create our new score key
				float [] k = new float[ c1.getKey().length ];
				
				//for the scores in the key
				for ( int i = 0 ; i < c1.getKey().length ; i ++ ){
					
					//add the keys
					k[i] = c1.getKey()[i] + c2.getKey()[i];
				}
				
				//return our array
				return k;
				
			}catch ( Exception e ){
				
				//log the error
				System.err.println("ERROR: Major error could not merge arrays.");
				
				//reeturn the first one , i return here to stop it exploding
				return c1.getKey();
			}
		}
		
		//tell the system error log about it
		System.err.println("ERROR: Using failback method to merge keys.");
		
		//this is only a failback situation where we dont know what key to get
		return (c1.getKey().length==0) ? c2.getKey() : c1.getKey();
	}
        
}
