import java.util.*;
import java.awt.Graphics;
import java.awt.Color;
import java.awt.Canvas;
import javax.swing.*;
import java.awt.event.*;
import javax.swing.event.*;
import java.io.*;

class test extends JLabel implements ChangeListener {
	
	//private Vector<Cross> points;
	//private LayeredGraph graph;
	LayeredQuadTree q;
	List< float[] > points;
	JSlider zoom;
	float zoomF = 0.05f;
	//QuadTree q;

	public void paintComponent ( Graphics g ){
		
		if ( points != null ){
			for ( float[] r : points ){
				int x = (int)((r[0]+5.12f) * (100.0f+(zoomF*2.0f)));
				int y = (int)((r[1]+5.12f) * (100.0f+(zoomF*2.0f)));
				
				//x %= 730;
				//y %= 500;
				drawPoint(x,y,g);
			}
		}
		/*for( int i = 0 ; i < points.size() ; i ++ ){
			points.get(i).draw ( g );
		}//
		* 
		*
		
		zoom.repaint();*/
		//q.draw ( g );
	}//*/

	public test(){
		
		//graph = new LayeredGraph( 1 , "" , "" );
		long time = System.currentTimeMillis();
		try {q = new LayeredQuadTree ( 12 , "$x1" , "$x2" );}
		catch ( Exception e ){ System.exit(0); };
		
		System.out.println("Time:" + (System.currentTimeMillis() - time) );
		//q = new QuadTree ( 8 );
		
		//if our graph is null
		if ( q == null ){
			System.out.println("Error");
			System.exit(0);
		}
		
		points = q.getPoints( 0.0f , 0.0f , 1.0f , 1.0f );
		
		String s = printArray ( points );
		System.out.println( s );
		
		int max = 0;
		for ( int i = 0 ; i < points.size() ; i++ ){
			if ( points.get(i)[2] > max ){
				max = (int)points.get(i)[2];
			}
			
		}
		
		System.out.println("Max: " + max );
		/*try {
		PrintWriter out = new PrintWriter("out.json");
		String s = printArray ( points );
		out.println(s);
		out.close();}catch ( Exception e ){};*/
		
		this.setBounds ( 0 , 0 , 730 , 500 );
		this.setBorder(BorderFactory.createCompoundBorder(
                BorderFactory.createLoweredBevelBorder(),
                BorderFactory.createEmptyBorder(10,10,10,10)));
		
		JFrame frame = new JFrame();
        frame.setSize(800, 600);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLayout( null );
		
        zoom = new JSlider(JSlider.HORIZONTAL , 0, 24, 0);
        zoom.setBounds( 50 , 5 , 700 , 16 );
        zoom.addChangeListener(this);

        frame.add(zoom);
        frame.add ( this );
        
        frame.setVisible(true);
	
	}
	
    public static void main( String args[] ){
		
		test canvas = new test();
	}

	public void stateChanged(ChangeEvent e) {
		
		JSlider source = (JSlider)e.getSource();
		
		if (!source.getValueIsAdjusting()) {
			
			int value = (int)source.getValue();

			if ( value >= 0 && value <= 24 ){
				
				zoomF += 0.01f;
				
				//zoomF = ( (float)value / 26.0f );
				//System.out.println( zoomF );
				points.clear();
				points =  null;
				long t = System.nanoTime();
				
				points = q.getPoints( 0f + (zoomF/2.0f) , 0f + (zoomF/2.0f), 1.0f - (zoomF), 1.0f - (zoomF) );
				String s = printArray ( points );
				int max = 0;
				for ( int i = 0 ; i < points.size() ; i++ ){
					if ( points.get(i)[2] > max ){
						max = (int)points.get(i)[2];
					}
					
				}
					
				System.out.println("Max Mass: " + max );
		
				System.out.println( "Nano:" + (System.nanoTime() - t)/1000000 );
				
				System.out.println( "Points" + points.size() );
				this.repaint();
			}
		}
	}
	
	
	public void drawPoint ( int x , int y , Graphics g)
	{
		int shapeRadius = 2;
		g.setColor( Color.black );
		g.drawLine( x - shapeRadius,  y,  x + shapeRadius,  y);
		g.drawLine( x,  y - shapeRadius,  x,  y + shapeRadius);
    }
    
 	String printArray ( List< float[] > e ){
	
		StringBuilder builder = new StringBuilder( "[" );
		
		for ( int j = 0 ; j < e.size() ; j ++ ){
			
			builder.append( "[" );
			
			float[] p = e.get(j);
			float mass = 1.0f;
			
			for ( int i = 0 ; i < p.length ; i ++ ){
				
				if ( i == 2 )
					mass = p[i];
					
				if ( i > 2 )
					p[i] /= mass;

				if ( i < (p.length-1) ){
					
					builder.append(p[i] + "," );
				}else{
					
					builder.append(p[i]);
				}
				
			}
			
			builder.append( "]" );
			
			if ( j < (e.size()-1) ){
					builder.append( "," );
			}
		}
		
		builder.append("]");
		
		return builder.toString();
	}
	
}

