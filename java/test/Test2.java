import java.util.*;
import java.io.*;
import java.awt.Canvas;
import java.awt.Graphics;
import java.awt.Color;
import java.awt.Dimension;
import javax.swing.*;
import com.tomgibara.cluster.gvm.dbl.DblClusters;
import com.tomgibara.cluster.gvm.dbl.DblListKeyer;
import com.tomgibara.cluster.gvm.dbl.DblResult;
import java.awt.event.*;

class Test2 extends Canvas {
	
	//this is our list of points
	public Vector< double[] > points;
	List<DblResult<List<double[]>>> results;
	
	int cluster_count = 400;
	
	public Test2() {
		
		points = new Vector< double[] >();
		
		generateTestPoints();
		
		DblClusters<List<double[]>> clusters = new DblClusters<List<double[]>>( 2, cluster_count );
		clusters.setKeyer(new DblListKeyer< double[] >());
		
		for (double[] pt : points ) {
			ArrayList<double[]> key = new ArrayList<double[]>();
            key.add(pt);
			clusters.add(1.0, pt, key );
		}
		
		results = clusters.results();
		
		System.out.println( "Results: " + results.size());	
				
		JFrame frame = new JFrame();
		frame.setSize(800, 600);
		frame.getContentPane().add( this );	
		frame.setVisible(true);
		
	}
	
	
	public static void main( String args[] ){

		
		Test2 canvas = new Test2();
	}
	
	private void generateTestPoints(){
			
		Vector  GaussianList;
		GaussianList = new Vector();
		Random rand = new Random();
		int clusterCount = 10;
		
		//we want 100 clusters
		for (int i = 0; i< clusterCount ;i++)
		{
			Gaussian gaus = new Gaussian();  

			gaus.mux = 50 + Math.abs(rand.nextInt() % 650);
			gaus.muy = 75 + Math.abs(rand.nextInt() % 425);

			gaus.sigma = 10 + Math.abs(30 * rand.nextDouble());

			GaussianList.addElement(gaus);
		}
		
		for (int i = 0; i< clusterCount ;i++)
		{

			Gaussian gaus;
			gaus = (Gaussian) GaussianList.elementAt(i);

			for (int j = 0;j<100000/clusterCount;j++)
			{
				double r = 5*gaus.sigma*Math.pow(rand.nextDouble(),2);
				double alpha = 2*Math.PI*rand.nextDouble();
				int x = gaus.mux + (int) Math.round(r*Math.cos(alpha));
				int y = gaus.muy + (int) Math.round(r*Math.sin(alpha));
				
					double[] point = new double[2];
					point[0] = (double)x;
					point[1] = (double)y;
					points.addElement( point );
			  }
		}
		
		Random r = new Random();
		int i = r.nextInt();
		try{
		BufferedWriter writer = new BufferedWriter(new FileWriter( "out/test" + i + ".txt" ));
        for ( double point[] : points )
        {      
			writer.write( point[0] + "," + point[1] + "," + 2.0f );
			writer.newLine();
        }
        writer.close();
		}catch ( Exception e ){
		}
	}
	
	public void paint ( Graphics g ){
		
		for( int i = 0 ; i < points.size() ; i++ ){
			double[] p = points.get(i);
			
			drawPoint( (int)p[0] , (int)p[1] , 2 , g );
		}
		
		for (int i = 0; i < results.size(); i++) {
			
			int x = (int)results.get(i).getCoords()[0];
			int y = (int)results.get(i).getCoords()[1];
			double pop = results.get(i).getMass();

			drawCluster ( x , y , pop , g );
		}
	}
	
	void drawCluster ( int x ,int y, double mass, Graphics g )
	{
		int rad = 4 + (int)(mass/300);
		g.setColor ( Color.red );
		
		g.fillOval(x - rad , y - rad , rad*2 , rad*2 );
		g.setColor(Color.black);
		g.drawOval(x - rad , y - rad , rad*2 , rad*2 );
	}
	
	void drawPoint ( int x ,int y, int shapeRadius ,  Graphics g )
	{		
		g.setColor(Color.black );
		g.drawLine(x - shapeRadius, y, x + shapeRadius, y);
		g.drawLine(x, y - shapeRadius, x, y + shapeRadius);
	}
}
