/*
 * 
 * (c) Lewis Maitland 2013
 * ----------------------------------------------------
 * This class is used to our quad tree. Essentialy it is
 * just a reduced version of the quadnode and without any points.
 * This has 4 children of whom are quadnodes. This class
 * can be used to add or search for points. The constructor
 * value is given to specifcy the depths from which divides should take place.
 * The number of leafs is 4^(depth).
 * 
*/
import java.awt.Graphics;
import java.util.*;

class QuadTree implements java.io.Serializable  {
	
	//this is our nodes
	QuadNode[] nodes;
	
	//we can store our leafs so that we can quickly loop though
	//and cluster them
	List < QuadNode > leafs;
	
	//this is our x,y,width and height
	float x,y,width,height;
	
	//this is our number of points within
	int size;
	
	//this is used to store the length of our scores list ie the number of objectives
	//we calculate this based on the first insert , thereafter varying score lengths will throw an excpeiton
	int score_count = -1;
	
	//This our constructor
	public QuadTree( float x , float y , float width , float height , int depth ){
		
		//init our array
		nodes = new QuadNode[4];
		
		//set our size
		size = 0;
		
		//init our leafs
		leafs = new ArrayList< QuadNode >();
		
		//create our objects
		nodes[0] = new QuadNode ( x , y , width/2.0f , height/2.0f , depth - 1 );
		nodes[1] = new QuadNode ( x + (width/2.0f) , y , width/2.0f , height/2.0f , depth - 1 );
		nodes[2] = new QuadNode ( x , y + (height/2.0f) , width/2.0f , height/2.0f , depth - 1 );
		nodes[3] = new QuadNode ( x + (width/2.0f), y + (height/2.0f) , width/2.0f , height/2.0f , depth - 1 );
		
		//now set our dimensions
		this.x = x;
		this.y = y;
		this.width = width;
		this.height = height;
	}
	
	//this function is used to check if a point is within this nodes bounding box
	public boolean inBoundingBox ( float x , float y ){
		
		//return if we are within the rectangle
		return ( x >= this.x && x <= (this.x+this.width) && y >= this.y && y<= (this.y+this.height) );
	}
	
	//add a point to this quad tree =)
	public void addPoint ( float x , float y , float score[]  , float mass ) throws Exception {
		
		//set our score length for first insert
		if ( score_count == -1 )
			score_count = score.length;
			
		//check our score length
		if ( score_count != score.length ){
			
			//throw the new exception
			throw new Exception( "ERROR: Score length was invalid!" );
		}		
		
		//make sure its actually a valid point
		if ( !inBoundingBox ( x , y ) ){
			
			//throw an exception if it wasnt a valid point!
			throw new Exception("ERROR: Point was not valid! { " + x + " , " + y + "}");
		}
		
		//for all of our nodes
		for ( QuadNode q : nodes ){
			
			//make sure nothing is null
			if ( q == null )
				continue;

			//if it will contain the point .. then add it :)
			if ( q.inBoundingBox ( x , y ) ){
				
				//add the point
				q.addPoint ( x , y ,score , mass );
				
				//increment our size
				size++;
				
				return;
			}
		}
	}
	
	//this function is used to get our size
	public int size(){
		return size;
	}
	
	//this is our slightly more accuarte version of our point count getter
	public int getPointCount ( float x , float y , float width , float height ){
		
		//get our count
		int count = 0;
		
		//loop through our nodes
		for ( QuadNode q : nodes ){
			
			//get the point count for each of our nodes
			if ( q.intersectsRectangle ( x , y ,width , height ) ){
				
				//get the point count from our kids
				count += q.getPointCount ( x , y ,width, height );
			}
		}
		
		//return the number of points we counted
		return count;
	}
	
	//this is our function for searching the quad tree
	public List< float[] > search ( float x , float y , float width , float height ){
		
		//init our points
		List< float[] > points = new ArrayList< float[] >();
		
		if ( nodes != null ){
			
			//for each of our nodes
			for ( QuadNode q : nodes ){
				
				//make sure we have a node
				if( q== null )
					continue;
				
				//make sure we contain the rectangle of search
				if ( q.intersectsRectangle ( x , y ,width, height ) )
				{
					//search for the points
					q.search ( x , y , width , height , points );
				}
			}
		}
		
		//return the points
		return points;
	}
	
	//this should be called to optimize the GVM clustering algorithm
	public void storeLeafs(){
		
		//if we have nodes then
		if ( nodes != null ){
			
			//get all of our nodes and them to our leafs
			for ( QuadNode q : nodes ){
				
				if ( q!= null )
					leafs.addAll ( q.getLeafs() );
			}
		}
	}
	
	//get our leafs
	public List<QuadNode> getLeafs(){
		
		//this is used to get our lefs from the quadtree
		return this.leafs;
	}
	
	//get our width
	public float getWidth(){
		return width;
	}
	
	//get the height
	public float getHeight(){
		return height;
	}
	
	//get the x
	public float getX(){
		return x;
	}
	
	//get the y
	public float getY(){
		return y;
	}
	
	//this is just used for testing :)
	public void draw ( Graphics g ){
		
		for ( QuadNode q : nodes ){
			
			if ( q!= null )
				q.draw ( g );
		}
	}
}
