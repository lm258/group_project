/*
 * 
 * (c) Lewis Maitland 2013
 * ----------------------------------------------------
 * This class is used to create and store our layered
 * QuadTree for fast clustering and point fetching.
 * 
 * [ x , y, mass, scores1 , scores 2 , .... ]
*/
import java.util.List;
import java.util.ArrayList;
import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
import java.io.FileInputStream;
import java.util.Scanner;
import com.tomgibara.cluster.gvm.flt.*;
import java.sql.*;

class LayeredQuadTree implements java.io.Serializable {
	
	//this is our layers of quad trees
	List< QuadTree > layers;
	
	//This is our 'vanilla' quad tree
	QuadTree raw_tree;
	
	
	//this is our job id and paramter pairs for this graph
	int jobId;
	String paramOne,paramTwo;
	
	//this is our point threshold for when to fetch the next layer
	//ie if layer+1 is <2500 points then get it
	int thresh_hold = 2500;
	
	//this is the number we use to calculate how many layers we need
	float layer_div_factor = 5000.0f;
	
	
	//This is our constructor
	public LayeredQuadTree ( int job_id , String paramOne , String paramTwo ) throws Exception {
		
		//first we want to initalize our quadtree layers list
		layers = new ArrayList< QuadTree > ();
		
		//next set our job id and paramters etc
		jobId = job_id;
		this.paramOne = paramOne;
		this.paramTwo = paramTwo;

		try {
			
			//load our points from the database and get the layer count
			int layer_count = generateRawFromDatabase();
		
			//generate our clusters
			for ( int i = 0 ; i < layer_count ; i ++ ){
				
				//add our clusters
				addCluster ( i+1 , ((i>15) ? 5 : 4) );
			}
			
			//finally add the raw_tree
			layers.add ( raw_tree );

		}catch( Exception e ){
			
			//print the exception to the console
			System.err.println(e);
			
			//throw a new exception
			throw new Exception("ERROR:Could not initalize the graph!");
		}
	}
	
	//this is our primitive get points method with the zoom taken into account
	public List< float[] > getPoints ( float xP , float yP , float wP , float hP , int zoom ){
		
		//get our x and y
		float x = (raw_tree.getWidth() * xP) + raw_tree.getX();
		float y = (raw_tree.getHeight() * yP) + raw_tree.getY();
		
		//get our width and height
		float w = (raw_tree.getWidth() * wP);
		float h = (raw_tree.getHeight() * hP);
		
		//make sure we keep within our limits lol
		zoom = Math.max ( Math.min( zoom , layers.size() - 1 ) , 0 );
		
		//now do a search on out tree
		return layers.get(zoom).search( x , y ,w , h );
	}
	
	//this function is used to calculate the zoom factor if its wrong
	public int calculateZoom ( float xP , float yP , float wP , float hP ){
		
		//get our x and y
		float x = (raw_tree.getWidth() * xP) + raw_tree.getX();
		float y = (raw_tree.getHeight() * yP) + raw_tree.getY();
		
		//get our width and height
		float w = (raw_tree.getWidth() * wP);
		float h = (raw_tree.getHeight() * hP);
		
		int zoom = 0;
		
		//get the zoom factor
		for ( zoom = (layers.size()-1); zoom >= 0 ; zoom-- ){
			
			//get the point count
			int count = layers.get( zoom ).getPointCount ( x , y, w , h );
			
			//if its lower than 2500 then return it
			if ( count < thresh_hold )
				break;
		}
		
		//return our zoom factor
		return zoom;
	}
	
	//this is our get points function that doesnt take a zoom factor
	public List<float[]> getPoints ( float xP , float yP , float wP, float hP ){

		//int zoom = Math.min ( layers.size() - (int)(p*(float)layers.size()+5.0f) , layers.size()-1 );
		int zoom = calculateZoom ( xP , yP , wP , hP );
	
		//return the points we found
		return getPoints ( xP , yP , wP , hP , zoom );
	}
	
	/* 
	 * This function is used to add a cluster to our layered graph.
	*/
	public void addCluster ( int size  , int depth ){
		
		try{
			//get our leafs
			List< QuadNode > leafs = raw_tree.getLeafs();
			
			//create our new quadtree for storing our calculations here
			QuadTree clusterTree = new QuadTree ( raw_tree.getX() , raw_tree.getY() , raw_tree.getWidth() , raw_tree.getHeight() , depth  );
			
			//loop through our leafs
			for ( QuadNode leaf : leafs ){
			
				//first of all create a new cluster object
				FltClusters< float[] > fltClusters = new FltClusters< float[] >( 2 , size );

				//now set our keyer
				fltClusters.setKeyer( new FltScoreKeyer() );

				//get the points of the current leaf
				List< float[] > leafPoints = leaf.getPoints();
				
				//loop through our points
				for ( float[] pt : leafPoints ){
					
					//add them to our cluster
					//remember the key is the score of the simulation
					float[] co = new float[2];
					co[0] = pt[0];
					co[1] = pt[1];
					
					float[] score;
					
					if ( (pt.length-2) > 0 ){
						score = new float[ pt.length - 2 ];
						
						for ( int i = 2 ; i < pt.length ; i ++ ){
							score[i-2] = pt[i];
						}
						
					}else{
						score = new float[1];
						score[0] = 0.0f;
					}
					
					//add our cluster
					fltClusters.add ( 1.0f ,  co , score );
				}

				//get the results of our cluster
				List< FltResult< float[] >> leafClusters = fltClusters.results();
				
				//go through our leaf clusters
				for ( FltResult< float[]> leafCluster : leafClusters ){
						
					//add the point into our results tree
					clusterTree.addPoint( leafCluster.getCoords()[0] , leafCluster.getCoords()[1] , leafCluster.getKey() , leafCluster.getMass() );
				}
			}
			
			//add the cluster to our layer
			layers.add ( clusterTree );
				
		}catch ( Exception e ){
			
			//log our error
			System.err.println( "ERROR:" + e );
		}
	}
	
	//This function is used to save our object to a cache file
	//Overwrite is used to determine if we should overwrite if we find another version in the cache
	private void save ( boolean overwrite ){
		
		//check if the file exists that we would save our cache to
		File f = new File( new String( "cache/" + jobId + paramOne + paramTwo + ".cache" ).toLowerCase() );
				
		//in this case we want to write our object to file
		try {
			
			//check if it exists
			if ( !f.exists() || overwrite ){
				
				f.createNewFile();

				FileOutputStream fos = new FileOutputStream(f);
				ObjectOutputStream oos = new ObjectOutputStream(fos);
				
				//write our object
				oos.writeObject( this );
				
				//close the streams
				oos.close();
				fos.close();
			}
        
		}catch ( Exception e ){
						
			//if we got an exception then tell the user
			System.err.println( e );
		}
	}
	
	//this function is used to load our clusters from a cache file
	public static LayeredQuadTree loadFromCache( int job_id , String paramOne , String paramTwo ){
		
		//check if the file exists that we would save our cache to
		File f = new File( new String( "cache/" + job_id + paramOne + paramTwo + ".cache" ).toLowerCase() );
		
		//if it exists then load it from the file and return it
		if ( f.exists() ){
			
			//load our object here
			try {
				FileInputStream fis = new FileInputStream(f);
				ObjectInputStream ois = new ObjectInputStream(fis);
				
				//read our object
				LayeredQuadTree lObj = (LayeredQuadTree)ois.readObject();
				ois.close();
				fis.close();
				
				//return the object
				return lObj;
				
			}catch ( Exception e ){
				
				//print our error
				System.err.println( e );
				
				//return our null
				return null;
			}
		}else{
			
			//if it doesnt exist then just return null;
			return null;
		}
	}
	
	/*
	 * This function is used to get points from the database , it will take in the job id and
	 * the paramter pairs. and fetch as appropriate from the database.
	*/
	private int generateRawFromDatabase () throws Exception {
		
		//register our driver
		Class.forName("com.mysql.jdbc.Driver");
		
		//get the connection
		Connection conn = DriverManager.getConnection( "jdbc:mysql://localhost/cs3gp" , "root" , "elephant" );

		//here we are getting the number of objectives for our 
		//create our prepared statement
		//TODO this part could be potentially SLOW
		//this NEEDS work doe to it
		//SELECT * FROM `paramNames` WHERE job_id = 17 AND name='$x1'
		PreparedStatement stmtOne = conn.prepareStatement( "SELECT parameter_value.simulation_id , value , GROUP_CONCAT(score.score ORDER BY objective_id ASC ) as 'score' FROM parameter_value,simulation,score WHERE simulation.id=parameter_value.simulation_id AND simulation.job_id = ? AND parameter_value.parameter_id = ? AND score.simulation_id = simulation.id GROUP BY simulation.id ORDER BY simulation.id;" );
		PreparedStatement stmtTwo = conn.prepareStatement( "SELECT parameter_value.simulation_id , value , GROUP_CONCAT(score.score ORDER BY objective_id ASC ) as 'score' FROM parameter_value,simulation,score WHERE simulation.id=parameter_value.simulation_id AND simulation.job_id = ? AND parameter_value.parameter_id = ? AND score.simulation_id = simulation.id GROUP BY simulation.id ORDER BY simulation.id;" );
		
		//this is used for getting our parameter ids , it greatly speeds up the bigger select
		PreparedStatement stmPOne = conn.prepareStatement ( "SELECT * FROM `param_names` WHERE job_id = ? AND name=?" );
		PreparedStatement stmPTwo = conn.prepareStatement ( "SELECT * FROM `param_names` WHERE job_id = ? AND name=?" );
		
		//now set our parameter statement values
		stmPOne.setInt ( 1, this.jobId );
		stmPOne.setString( 2 , this.paramOne );
		stmPTwo.setInt ( 1, this.jobId );
		stmPTwo.setString( 2 , this.paramTwo );
		
		//execute our statments
		ResultSet pOne = stmPOne.executeQuery();
		ResultSet pTwo = stmPTwo.executeQuery();
		
		//check we have results
		if ( !pOne.next() || !pTwo.next() )
			throw new Exception("ERROR: Inccorect parameter supplied.");
		
		//now get our ids from it
		int pOneId = pOne.getInt("parameter_id");
		int pTwoId = pTwo.getInt("parameter_id");
		
		//now get our dimensions or distribution
		String[] pOneDims = pOne.getString("distribution").replace("(","").replace(")","").split(",");
		String[] pTwoDims = pTwo.getString("distribution").replace("(","").replace(")","").split(",");
		
		//parse our strings and get the dimensions
		float x = Float.parseFloat( pTwoDims[0] );
		float width = Float.parseFloat( pTwoDims[1] ) - x;
		float y = Float.parseFloat( pOneDims[0] );
		float height = Float.parseFloat( pOneDims[1] ) - y;
		
		System.out.println(x);
		System.out.println(y);
		System.out.println(width);
		System.out.println(height);
		
		//set our variables
		stmtOne.setInt ( 1, this.jobId );
		stmtOne.setString( 2 , Integer.toString(pOneId) );
		stmtTwo.setInt ( 1, this.jobId );
		stmtTwo.setString( 2 , Integer.toString(pTwoId) );
		
		//create our raw tree
		raw_tree = new QuadTree ( x-0.2f , y-0.2f , width+0.4f , height+0.4f , 6  );
		
		//execute our query on the data base
		ResultSet paramOneSet = stmtOne.executeQuery();
		ResultSet paramTwoSet = stmtTwo.executeQuery();
		
		int count = 0;
		
		//while we have results in our queies
		while ( paramOneSet.next() && paramTwoSet.next() ) {
 
			//get our parameter one and parameter two values
			float p_x = paramOneSet.getFloat("value");
			float p_y = paramTwoSet.getFloat("value");
			
			//split our string
			String score_s[] = paramOneSet.getString("score").split(",");
			
			//create our scores array
			float[] scores = new float [ score_s.length ];
			
			//for each of our scores add it to the score array
			for ( int i = 0 ; i < score_s.length ; i ++ ){
				
				//parse the float
				scores[i] = Float.parseFloat ( score_s[i] );
			}
			
			//add the point to our graph
			raw_tree.addPoint ( p_x , p_y , scores  , 1.0f );
		}
		
		//we NEED to do this stage or else we will get no points
		raw_tree.storeLeafs();
		
		//calculate our number of layers
		int layer_count = Math.round ( raw_tree.size()/layer_div_factor );
		
		//this is for testing
		System.out.println("Layers:" + layer_count );
		
		//return the number of layers
		return layer_count;
	}
	
	//im using this just to convert a series of points to a string :)
 	public static String toString ( List< float[] > e ){
	
		//create the string builder
		StringBuilder builder = new StringBuilder( "[" );
		
		//for each of our points
		for ( int j = 0 ; j < e.size() ; j ++ ){
			
			builder.append( "[" );
			
			float[] p = e.get(j);
			float mass = 1.0f;
			
			//for each of the scores etc
			for ( int i = 0 ; i < p.length ; i ++ ){
				
				if ( i == 2 )
					mass = p[i];
					
				if ( i > 2 )
					p[i] /= mass;

				if ( i < (p.length-1) ){
					
					builder.append(p[i] + "," );
				}else{
					
					builder.append(p[i]);
				}
				
			}
			
			builder.append( "]" );
			
			if ( j < (e.size()-1) ){
					builder.append( "," );
			}
		}
		
		builder.append("]");
		
		return builder.toString();
	}
}
