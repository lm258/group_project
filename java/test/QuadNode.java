/*
 * 
 * (c) Lewis Maitland 2013
 * ----------------------------------------------------
 * This class is used to store a quadnode in our quadtree.
 * Essentially it has 4 children ( if it is a branch )
 * who can either be branches or children. If this node
 * is a branch then it can only have points and no children.
 * 
 * [ x , y, mass , score1 , score2 , score3 ]
 * 
*/
import java.awt.Graphics;
import java.awt.Color;
import java.util.*;

class QuadNode implements java.io.Serializable {
	
	//this is our rectangle
	float x,y,width,height;
	
	//this is our points if we have any
	List < float[] > points;
	
	//this is our other quadnodes
	QuadNode[] nodes;
	
	//are we a leaf?
	boolean isLeaf;
	
	//the constructor is recursive so needs the depth to know when to stop
	QuadNode ( float x , float y , float width , float height  , int depth ){
		
		//set our x,y values etc
		this.x=x;
		this.y=y;
		this.width=width;
		this.height=height;
		
		//see if we are are at the end or not
		if ( depth == 0 ){
			
			//initalize our vectors etc
			isLeaf = true;
			points = new ArrayList< float[] >();
			
		}else{
			
			//otherwise we need to create our four nodes
			nodes = new QuadNode[4];
			
			//create each quarter
			nodes[0] = new QuadNode ( x , y , width/2.0f , height/2.0f , depth - 1 );
			nodes[1] = new QuadNode ( x + (width/2.0f) , y , width/2.0f , height/2.0f , depth - 1 );
			nodes[2] = new QuadNode ( x , y + (height/2.0f) , width/2.0f , height/2.0f , depth - 1 );
			nodes[3] = new QuadNode ( x + (width/2.0f), y + (height/2.0f) , width/2.0f , height/2.0f , depth - 1  );
		}
	}
	
	//this function is used to check if a point is within this nodes bounding box
	public boolean inBoundingBox ( float x , float y ){
		
		//return if we are within the rectangle
		return ( (x >= this.x) && (x <= (this.x+this.width)) && y >= this.y && y<= (this.y+this.height) );
	}
	
	//this function is used to find out if we are a leaf or not
	public boolean isNodeLeaf (){
		return isLeaf;
	}
	
	//return our points
	public List< float[] > getPoints(){
		
		//return our points
		return this.points;
	}
	
	//this function is used to get the number of points in a given rectangle quickly
	int getPointCount( float x , float y , float width , float height ){
		
		//if we are a leaf then count the number of points within
		if ( isLeaf ){
			
			//if we are totally inside the rectangle then
			if ( insideRectangle ( x , y , width , height ) ){
				
				//just get the size
				return points.size();

			}else{

				int count = 0;
				
				//for each of our points check if we are inside it or not
				for ( float[] p : points ){
					
					//check if its within the rectangle
					if ( p[0] >= x && p[0] <= (x+width) && p[1] >= y && p[1] <= (y+height) )
						count++;
				}
				
				//return the point count that we found while traversing our node
				return count;
			}
			
		}else{

			int count = 0;
			
			//loop through our nodes
			for ( QuadNode q : nodes ){
				
				//get the point count for each of our nodes
				if ( q.intersectsRectangle ( x , y ,width , height ) ){
					
					//get the point count from our kids
					count += q.getPointCount ( x , y ,width, height );
				}
			}
			
			//return the number of points we counted
			return count;
		}
	}
	
	//this is our search function
	//will search for the points within the given rectangle
	public void search ( float x , float y , float width , float height , List< float[] > points ){
		
		//check if we are a leaf or node :)
		if ( isLeaf ){
			
			//if we are totaly inside the rectangle then just add all of our points
			if ( insideRectangle ( x , y , width, height ) ){
				
				//add all of our points
				points.addAll ( this.points );
				
			}else{
				
				//otherwise get all of the points that are within the intersection
				//if we are add all the points to the points array lol
				for ( float[] p : this.points ){
					
					//check if its within the rectangle
					if ( p[0] >= x && p[0] <= (x+width) && p[1] >= y && p[1] <= (y+height) )
						points.add( p );
				}
			}

		}else{
			
			if ( nodes != null ){
				//otherwise we want to find out who this rectangle intersects the add the points etc
				for ( QuadNode q : nodes ){
					
					//if it intersects a child then get it
					if ( q.intersectsRectangle( x , y , width , height ) ){
						
						//search our children for the points
						q.search ( x , y , width , height , points );
					}
				}
			}
		}
	}
	
	//this function is used to check wether this node contains a rectangle
	public boolean intersectsRectangle ( float x , float y , float width , float height ){

		/*A.X1 < B.X2:	false
		A.X2 > B.X1:	true
		A.Y1 < B.Y2:	true
		A.Y2 > B.Y1*/
		
		//return if we intersect or not
		return ((this.x < (x+width)) && 
				 ( (this.x+this.width) > x ) && 
				 ( this.y < (y+height) ) &&
				 ( (this.y+this.height) > y ));
	}
	
	//this method is used to determine if we are within a rectangle
	public boolean insideRectangle ( float x , float y , float width , float height ){
		
		//return if we are inside the rectangle or not
		return ( this.x > x && this.y > y && (this.x+this.width)  < (x+width) && (this.y+this.height) < (y+height) );
	}
		
	//this is used to get our leafs from the quadtree
	public List < QuadNode > getLeafs(){
		
		//make our new quad
		List< QuadNode > node = new ArrayList < QuadNode >();
	
		//return 
		for ( QuadNode q : nodes ){
			
			if ( q.isNodeLeaf () ){
				
				//add the node to our nodes list
				node.add(q);
			}else{
				
				//otherwise we want to add our get leafs from the node
				List< QuadNode > n = q.getLeafs();
				
				//add our n to our node
				node.addAll ( n );
			}
		}
		
		//return our leafs
		return node;
	}
	
	//this function is used to add points to the qiad tree
	public void addPoint ( float x , float y , float[] score , float mass ){
		
		//if we are a leaf then add the point
		if ( isLeaf ){
			
			//add the point
			float[] point = new float[ 3 + score.length ];
			
			//x,y coordinates
			point[0] = x;
			point[1] = y;
			point[2] = mass;
			
			//now we need to add the score
			for ( int i = 0 ; i < score.length; i++ ){
				
				//add our score to the thing
				point[i+3] = score[i];
			}
			
			//now add the point :)
			points.add ( point );
			
		}else{
			
			//otherwise if we are are not a leaf find and add the point to our correct children
			for ( QuadNode q : nodes ){
				
				//if it will contain the point .. then add it :)
				if ( q.inBoundingBox ( x , y ) ){
					
					//add the point
					q.addPoint ( x , y ,score  , mass );
					break;
				}
			}
		}
		
	}
	
	//this function is used for drawing a visual representaion of the tree
	public void draw( Graphics g ){
		
		//set the colour
		g.setColor ( Color.black );
		g.drawRect ( (int)x , (int)y , (int)width , (int)height );
		
		if ( nodes!= null ){
			//draw all of our nodes
			for ( QuadNode q : nodes ){
				if ( q != null )
					q.draw( g );
			}
		}
	}
}
