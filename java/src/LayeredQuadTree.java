/*
 * 
 * (c) Lewis Maitland 2013
 * ----------------------------------------------------
 * This class is used to create and store our layered
 * QuadTree for fast clustering and point fetching.
 * 
 * [ x , y, mass, scores1 , scores 2 , .... ]
*/
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;
import java.util.HashMap;
import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
import java.io.FileInputStream;
import java.util.Scanner;
import com.tomgibara.cluster.gvm.flt.*;
import java.sql.*;

class LayeredQuadTree implements java.io.Serializable {
	
	//this is our layers of quad trees
	List< QuadTree > layers;
	
	//This is our 'vanilla' quad tree
	QuadTree raw_tree;
	
	//this map is for holding our objective ids against the
	Map < String , Integer > objectives;
	
	//this is our job id and paramter pairs for this graph
	int jobId;
	String paramOne,paramTwo;
	
	//this is our point threshold for when to fetch the next layer
	//ie if layer+1 is <2500 points then get it
	int thresh_hold = 2500;
	
	//this is the number we use to calculate how many layers we need
	float layer_div_factor = 5000.0f;
	float[] dimensions = { 0 , 0 , 0 , 0 };
	int tree_depth = 4;

	boolean isFiltered = false;
	
	
	//This is our constructor
	public LayeredQuadTree ( int job_id , String paramOne , String paramTwo ) throws Exception {
		
		//here we will set our  thresh_hold etc based on our configuration
		thresh_hold = HttpServer.config.asInt("thresh_hold");
		layer_div_factor = HttpServer.config.asFloat("layer_div_factor");
		tree_depth = HttpServer.config.asInt("tree_depth");

		//first we want to initalize our quadtree layers list
		layers = new ArrayList< QuadTree > ();
		
		//create our objectives hashmap for storing the objectives
		this.objectives = new HashMap< String , Integer >();
		
		//next set our job id and paramters etc
		jobId = job_id;
		this.paramOne = paramOne;
		this.paramTwo = paramTwo;

		try {
			
			//load our points from the database and get the layer count
			int layer_count = generateRawFromDatabase();
		
			//generate our clusters
			for ( int i = 0 ; i < layer_count ; i ++ ){
				
				//add our clusters
				addCluster ( i+1 , tree_depth );
			}
			
			//finally add the raw_tree
			layers.add ( raw_tree );

		}catch( Exception e ){
			
			//print the exception to the console
			System.err.println(e);
			
			//throw a new exception
			throw new Exception("ERROR:Could not initalize the graph!");
		}

		save( true );
	}
	
	//this is our primitive get points method with the zoom taken into account
	public List< float[] > getPoints ( float x , float y , float w , float h , int zoom ){
		
		//make sure we keep within our limits lol
		zoom = Math.max ( Math.min( zoom , layers.size() - 1 ) , 0 );
		
		//now do a search on out tree
		return layers.get(zoom).search( x , y ,w , h );
	}
	
	//this function is used to calculate the zoom factor if its wrong
	public int calculateZoom ( float x , float y , float w , float h ){
		
		int zoom = 0;
		
		//get the zoom factor
		for ( zoom = (layers.size()-1); zoom >= 0 ; zoom-- ){
			
			//get the point count
			int count = layers.get( zoom ).getPointCount ( x , y, w , h );
			
			//if its lower than 2500 then return it
			if ( count < thresh_hold )
				break;
		}
		
		//return our zoom factor
		return zoom;
	}
	
	//this is our get points function that doesnt take a zoom factor
	public List<float[]> getPoints ( float xP , float yP , float wP, float hP ){

		//int zoom = Math.min ( layers.size() - (int)(p*(float)layers.size()+5.0f) , layers.size()-1 );
		int zoom = calculateZoom ( xP , yP , wP , hP );
	
		//return the points we found
		return getPoints ( xP , yP , wP , hP , zoom );
	}
	
	/* 
	 * This function is used to add a cluster to our layered graph.
	*/
	public void addCluster ( int size  , int depth ){
		
		try{
			//get our leafs
			List< QuadNode > leafs = raw_tree.getLeafs();
			
			//create our new quadtree for storing our calculations here
			QuadTree clusterTree = new QuadTree ( raw_tree.getX() , raw_tree.getY() , raw_tree.getWidth() , raw_tree.getHeight() , depth  );
			
			//loop through our leafs
			for ( QuadNode leaf : leafs ){
			
				//first of all create a new cluster object
				FltClusters< float[] > fltClusters = new FltClusters< float[] >( 2 , size );

				//now set our keyer
				fltClusters.setKeyer( new FltScoreKeyer() );

				//get the points of the current leaf
				List< float[] > leafPoints = leaf.getPoints();
				
				//loop through our points
				for ( float[] pt : leafPoints ){
					
					//add them to our cluster
					//remember the key is the score of the simulation
					float[] co = new float[2];
					co[0] = pt[0];
					co[1] = pt[1];
					
					float[] score;
					
					if ( (pt.length-3) > 0 ){
						score = new float[ pt.length - 3 ];
						
						for ( int i = 3 ; i < pt.length ; i ++ ){
							score[i-3] = pt[i];
						}
						
					}else{
						score = new float[1];
						score[0] = 0.0f;
					}
					
					//add our cluster
					fltClusters.add ( 1.0f ,  co , score );
				}

				//get the results of our cluster
				List< FltResult< float[] >> leafClusters = fltClusters.results();
				
				//go through our leaf clusters
				for ( FltResult< float[]> leafCluster : leafClusters ){
					//add the point into our results tree
					clusterTree.addPoint( leafCluster.getCoords()[0] , leafCluster.getCoords()[1] , leafCluster.getKey() , leafCluster.getMass() );
				}
			}
			
			//add the cluster to our layer
			layers.add ( clusterTree );
				
		}catch ( Exception e ){
			
			//log our error
			System.err.println( "ERROR:" + e );
		}
	}
	
	//This function is used to save our object to a cache file
	//Overwrite is used to determine if we should overwrite if we find another version in the cache
	private void save ( boolean overwrite ){
		
		//check if the file exists that we would save our cache to
		File f = new File( new String( "/usr/raven/cache/" + jobId + paramOne + paramTwo + ".cache" ).toLowerCase() );
				
		//in this case we want to write our object to file
		try {
			
			//check if it exists
			if ( !f.exists() || overwrite ){
				
				f.createNewFile();

				FileOutputStream fos = new FileOutputStream(f);
				ObjectOutputStream oos = new ObjectOutputStream(fos);
				
				//write our object
				oos.writeObject( this );
				
				//close the streams
				oos.close();
				fos.close();
			}
        
		}catch ( Exception e ){
						
			//if we got an exception then tell the user
			System.err.println( e );
		}
	}
	
	//this function is used to load our clusters from a cache file
	public static LayeredQuadTree loadFromCache( int job_id , String paramOne , String paramTwo ){
		
		System.out.println("Loading graph from cache { id : " + job_id + "}" );
		
		//check if the file exists that we would save our cache to
		File f = new File( new String( "/usr/raven/cache/" + job_id + paramOne + paramTwo + ".cache" ).toLowerCase() );
		
		//if it exists then load it from the file and return it
		if ( f.exists() ){
			
			//load our object here
			try {
				FileInputStream fis = new FileInputStream(f);
				ObjectInputStream ois = new ObjectInputStream(fis);
				
				//read our object
				LayeredQuadTree lObj = (LayeredQuadTree)ois.readObject();
				ois.close();
				fis.close();
				
				//return the object
				return lObj;
				
			}catch ( Exception e ){
				
				//print our error
				System.err.println( e );
				
				//return our null
				return null;
			}
		}else{
			
			//if it doesnt exist then just return null;
			return null;
		}
	}
	
	/*
	 * This function is used to get points from the database , it will take in the job id and
	 * the paramter pairs. and fetch as appropriate from the database.
	*/
	private int generateRawFromDatabase () throws Exception {
		
		//register our driver
		Class.forName("com.mysql.jdbc.Driver");
		
		//get the connection
		Connection conn = DriverManager.getConnection( "jdbc:mysql://localhost/raven" , "root" , "elephant" );

		//here we are getting the number of objectives for our 
		//create our prepared statement
		//TODO this part could be potentially SLOW
		//this NEEDS work doe to it
		//SELECT * FROM `paramNames` WHERE job_id = 17 AND name='$x1'
		PreparedStatement stmtOne = conn.prepareStatement( "SELECT parameter_value.simulation_id , value , GROUP_CONCAT(score.score ORDER BY objective_id ASC ) as 'score' FROM parameter_value,simulation,score WHERE simulation.id=parameter_value.simulation_id AND simulation.job_id = ? AND parameter_value.parameter_id = ? AND score.simulation_id = simulation.id GROUP BY simulation.id ORDER BY simulation.id ;" );
		PreparedStatement stmtTwo = conn.prepareStatement( "SELECT parameter_value.simulation_id , value , GROUP_CONCAT(score.score ORDER BY objective_id ASC ) as 'score' FROM parameter_value,simulation,score WHERE simulation.id=parameter_value.simulation_id AND simulation.job_id = ? AND parameter_value.parameter_id = ? AND score.simulation_id = simulation.id GROUP BY simulation.id ORDER BY simulation.id ;" );
		
		//this is used to get our list of objectives and the relevant objective_ids for them
		PreparedStatement stmtObjectives = conn.prepareStatement( "SELECT `name`,`objective_id` FROM `objective_names` WHERE job_id = ? ORDER BY objective_id ASC;" );
		
		//this is used for getting our parameter ids , it greatly speeds up the bigger select
		PreparedStatement stmPOne = conn.prepareStatement ( "SELECT * FROM `param_names` WHERE job_id = ? AND name=?" );
		PreparedStatement stmPTwo = conn.prepareStatement ( "SELECT * FROM `param_names` WHERE job_id = ? AND name=?" );
		
		//now set our parameter statement values
		stmPOne.setInt ( 1, this.jobId );
		stmPOne.setString( 2 , this.paramOne );
		stmPTwo.setInt ( 1, this.jobId );
		stmPTwo.setString( 2 , this.paramTwo );
		stmtObjectives.setInt( 1, this.jobId );
		
		//execute our statments
		ResultSet pOne = stmPOne.executeQuery();
		ResultSet pTwo = stmPTwo.executeQuery();
		ResultSet pThree = stmtObjectives.executeQuery();
		
		//check we have results
		if ( !pOne.next() || !pTwo.next() )
			throw new Exception("ERROR: Inccorect parameter supplied.");
		
		//now get our ids from it
		int pOneId = pOne.getInt("parameter_id");
		int pTwoId = pTwo.getInt("parameter_id");
		
		//now get our dimensions or distribution
		String[] pOneDims = pOne.getString("distribution").replace("(","").replace(")","").split(",");
		String[] pTwoDims = pTwo.getString("distribution").replace("(","").replace(")","").split(",");
		
		//parse our strings and get the dimensions
		float x = Float.parseFloat( pOneDims[0] );
		float width = Float.parseFloat( pOneDims[1] ) - x;
		float y = Float.parseFloat( pTwoDims[0] );
		float height = Float.parseFloat( pTwoDims[1] ) - y;

		//make sure we set our dimensions
		dimensions[0] = x*1.05f;
		dimensions[1] = y*1.05f;
		dimensions[2] = width*1.05f;
		dimensions[3] = height*1.05f;
		
		//set our variables
		stmtOne.setInt ( 1, this.jobId );
		stmtOne.setString( 2 , Integer.toString(pOneId) );
		stmtTwo.setInt ( 1, this.jobId );
		stmtTwo.setString( 2 , Integer.toString(pTwoId) );
		
		//create our raw tree
		raw_tree = new QuadTree ( x*1.05f , y*1.05f , width*1.05f , height*1.05f , 6  );
		
		//execute our query on the data base
		ResultSet paramOneSet = stmtOne.executeQuery();
		ResultSet paramTwoSet = stmtTwo.executeQuery();
		
		int count = 0;
		
		//go through and get our objectives out
		while ( pThree.next() ){
			//put our objectives into the map
			objectives.put( pThree.getString("name") , count++ );
		}
		
		count = 0;
		
		//while we have results in our queies
		while ( paramOneSet.next() && paramTwoSet.next() ) {
 
			//get our parameter one and parameter two values
			float p_x = paramOneSet.getFloat("value");
			float p_y = paramTwoSet.getFloat("value");

			//split our string
			String score_s[] = paramOneSet.getString("score").split(",");
			
			//create our scores array
			float[] scores = new float [ score_s.length ];
			
			//for each of our scores add it to the score array
			for ( int i = 0 ; i < score_s.length ; i ++ ){
				
				//parse the float
				scores[i] = Float.parseFloat ( score_s[i] );
			}
			
			//add the point to our graph
			raw_tree.addPoint ( p_x , p_y , scores  , 1.0f );
		}
		
		//we NEED to do this stage or else we will get no points
		raw_tree.storeLeafs();
		
		//calculate our number of layers
		int layer_count = Math.round ( raw_tree.size()/layer_div_factor );
		
		//return the number of layers
		return layer_count;
	}
	
	//this function is used to get our objectives
	public Map< String , Integer > getObjectives(){
		return this.objectives;
	}

	/* This function will return out dimensions */
	public float[] getDimensions(){
		return dimensions;
	}
	
	//im using this just to convert a series of points to a string :)
 	public static String toString ( List< float[] > e  , int score_index ){
		
		//check if we actually have points
		if ( e.size() == 0 )
			return "[[],[]]";
		
		//create the string builder
		StringBuilder builder = new StringBuilder( "[[" );
		
		//get our min and mac scores first
		float min = e.get(0)[ 3 + score_index ];
		float max = e.get(0)[ 3 + score_index ];
		float diff;
		
		//loop through and get min and max mufuka
		for( int i = 0 ; i < e.size() ; i ++ ){
			
			if ( min < e.get(i)[ 3 + score_index ] &&  e.get(i)[ 3 + score_index ] != -1 )
				min = e.get(i)[ 3 + score_index ];
				
			if ( max > e.get(i)[ 3 + score_index ] )
				max = e.get(i)[ 3 + score_index ];
		}
		
		//get our diff
		diff = max - min;
		
		//for each of our points
		for ( int j = 0 ; j < e.size() ; j ++ ){
			
			float[] p = e.get(j);
			
			//make sure its a valid score ( filtering graphs sets it to -1 if the score is not wanted
			if ( p[ 3 + score_index ] < 0 )
				continue;
			
			builder.append( "[" );

			//for each of the scores etc
			//x,y,mass,color,score
			builder.append( p[0] + "," );
			builder.append( p[1] + "," );
			builder.append( p[2] + "," );
			builder.append( Math.round((((p[ 3 + score_index ]-min)/diff)*255)) +"," );
			builder.append( (p[ 3 + score_index ]/p[2]) + "]" );
			
			//now let us continue adding commas
			if ( j < (e.size()-1) ){
					builder.append( "," );
			}
		}
		
		//make sure there is no comma at the end before we add our brakets
		if ( builder.charAt(builder.length()-1)  == ',' )
			builder.deleteCharAt(builder.length()-1);

		//print our min and max , is this needed ??
		builder.append( "],[" + min + "," + max + "]]" );
		
		return builder.toString();
	}
	
	//this is used to check for equality in graphs
	public String toString( int user_id ){
		
		//return the string representation
		return user_id + paramOne + paramTwo + jobId;
	}
	
	//this function is used to apply a filter to the the graph
	//it will also recluster the graph!
	public void filterGraph ( int score_index , int min , int max ) throws Exception
	{
		
		//first of all we will need to reset our layers
		List< QuadTree > layers_tmp = layers;
		layers = new ArrayList< QuadTree > ();
		QuadTree raw_tree_tmp = null;

		try{
				//get our leafs
				List< QuadNode > leafs = raw_tree.getLeafs();
				
				//create our new quadtree for storing our calculations here
				QuadTree filteredTree = new QuadTree ( raw_tree.getX() , raw_tree.getY() , raw_tree.getWidth() , raw_tree.getHeight() , 6  );
				
				//although slightly dirty this is so we can cluster the correct graph
				raw_tree_tmp = raw_tree;
				raw_tree = filteredTree;
				
				//loop through our leafs
				for ( QuadNode leaf : leafs ){

					//get the points of the current leaf
					List< float[] > leafPoints = leaf.getPoints();
					
					//loop through our points
					for ( float[] pt : leafPoints ){
						
						//add them to our cluster
						//remember the key is the score of the simulation
						float[] co = new float[2];
						co[0] = pt[0];
						co[1] = pt[1];
						float[] score;
						
						if ( (pt.length-3) > 0 ){

							score = new float[ pt.length - 3 ];
							
							for ( int i = 3 ; i < pt.length ; i ++ ){
								score[i-3] = pt[i];
								
								//check to see if the score is within our filter bounds
								if ( score[i-3] < min || score[i-3] > max && (score_index+3) == i )
									score[i-3] = -1;
							}
							
						}else{
							score = new float[1];
							score[0] = 0.0f;
						}
						
						//add the point to our graph
						raw_tree.addPoint ( co[0] , co[1] , score  , 1.0f );
					}
				}

				//recalculate our layer count
				int layer_count = Math.round ( raw_tree.size()/layer_div_factor );
				raw_tree.storeLeafs();

				//generate our clusters
				for ( int i = 0 ; i < layer_count ; i ++ ){
					
					//add our clusters
					addCluster ( i+1 , tree_depth );
				}
				
				//finally add the raw_tree
				layers.add ( raw_tree );
				
				//now swap them back over again
				raw_tree = raw_tree_tmp;
				
				//set our is filtered
				isFiltered = true;

			}catch( Exception e ){
				
				//check to see if we have a tmp tree still available
				if ( raw_tree_tmp != null ){
					
					//now swap them back over again
					raw_tree = raw_tree_tmp;
					
					//set our layers back
					layers = layers_tmp;
				}
					
				throw new Exception("ERROR: Could not filter the graph.");
			}
	
	}
	
	//This function is used to remove any filtering from the graph
	public void removeFilter(){
		
		//recalculate our layer count
		int layer_count = Math.round ( raw_tree.size()/layer_div_factor );
		
		//generate our clusters
		for ( int i = 0 ; i < layer_count ; i ++ ){
			
			//add our clusters
			addCluster ( i+1 , tree_depth );
		}
		
		//finally add the raw_tree
		layers.add ( raw_tree );
	}
	
}
