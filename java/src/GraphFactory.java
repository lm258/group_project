/*
 * (c) 2013 Lewis Maitland 
 * ---------------------------------------------------
 * This file is our graph factory , it essentially
 * holds all of our graphs , makes and destorys them.
 * When ever a user makes a request to one such as getting points etc
*/
import java.util.concurrent.ConcurrentHashMap;
import java.util.List;
import java.util.ArrayList;

class GraphFactory {
	
	//this is our hashmap for storing our layered graphs for users
	//the key is the user_id and the object is the layered graph list ( might need two for overlays etc )
	volatile ConcurrentHashMap< Integer , ConcurrentHashMap< String , LayeredQuadTree > > userGraphs;

	//our default constructor
	GraphFactory(){
		
		//instantiate our graphs
		userGraphs = new ConcurrentHashMap< Integer , ConcurrentHashMap< String , LayeredQuadTree > >();
	}
	
	//called when we initalize our graph for a user
	LayeredQuadTree initGraph ( int user_id , int job_id , String paramOne , String paramTwo ){
		
		//check if the user exists
		if ( userGraphs.containsKey ( user_id ) ){
			
			//try and create ze graph
			try {
				
				/* Check to see if we already have the graph or not */
				if ( userGraphs.get( user_id ).containsKey ( makeGraphKey(job_id,user_id,paramOne,paramTwo) ) )
				{
						
						//print a small error saying what happened
						System.err.println( "WARNING: Graph already exits not creating. {" + makeGraphKey(job_id,user_id,paramOne,paramTwo) + "}" );
						
						//just exit out this
						LayeredQuadTree graph = userGraphs.get( user_id ).get ( makeGraphKey(job_id,user_id,paramOne,paramTwo) );
						
						//check to make sure it is not filtered
						if ( graph.isFiltered == true )
							graph.removeFilter();
							
							return graph;
				}
				
				//check to see if we have the graph in cache first :)
				//get the key then add a new layered graph to it
				LayeredQuadTree graph = LayeredQuadTree.loadFromCache ( job_id , paramOne , paramTwo );
				
				if ( graph == null ) 
					graph = new LayeredQuadTree ( job_id , paramOne , paramTwo );
				
				//add our graph for the user
				userGraphs.get( user_id ).put( makeGraphKey(job_id,user_id,paramOne,paramTwo) , graph );

				//return true we done it
				return graph;
				
			}catch ( Exception e ){
				
				//print the error of why this happened
				System.err.println( e );
				
				return null;
			}
		}else{
			
			try {
				
				//create our layered graph lsit
				ConcurrentHashMap< String , LayeredQuadTree > list = new ConcurrentHashMap< String , LayeredQuadTree >();
				
				//get the key then add a new layered graph to it
				LayeredQuadTree graph = LayeredQuadTree.loadFromCache ( job_id , paramOne , paramTwo );
				
				if ( graph == null ) 
					graph = new LayeredQuadTree ( job_id , paramOne , paramTwo );
				
				//add the graph to the list
				list.put( makeGraphKey(job_id,user_id,paramOne,paramTwo) , graph );
				
				//now add our user to the table
				userGraphs.put ( user_id  , list );
				
				//we done it return true
				return graph;
				
			}catch( Exception e ){
				
				//print the error and return false
				System.err.println( e );
				
				//return false
				return null;
			}
		}
	}
	
	//this is called when we want to get points from a graph for a user
	public String getPoints ( int user_id , int job_id , String objective , String paramOne , String paramTwo , float x , float y , float width , float height ){
		
		try {
			
			//try and get our points from the hashmap
			List< float[] > points = userGraphs.get ( user_id ).get( makeGraphKey(job_id,user_id,paramOne,paramTwo) ).getPoints ( x , y , width , height );
			
			//now convert them to a string
			int objIndex = userGraphs.get ( user_id ).get( makeGraphKey(job_id,user_id,paramOne,paramTwo) ).getObjectives().get ( objective );
			String points_s = LayeredQuadTree.toString( points  , objIndex );
			
			//return the points
			return points_s;
			
		}catch ( Exception e ){
			
			//print the error
			System.err.println( "ERROR: Graph does not exist , or inccorect score index given. {" + e + "}" );
			
			//return null
			return null;
		}
		
	}
	
	/*
	 * This function is used to free a graph from RAM 
	*/
	public void freeGraph ( int user_id , int job_id , String paramOne , String paramTwo ){
		
		//check if the user exists
		if ( userGraphs.containsKey ( user_id ) ){
			
			if ( userGraphs.get( user_id ).remove ( makeGraphKey(job_id,user_id,paramOne,paramTwo) ) != null ){
				System.out.println("Freeds Graph from memory.");
			}
			
		}
	}
	
	/*
	 * This function is used to filter the graph
	*/
	void filterGraph ( int user_id , int job_id , String objective , String paramOne , String paramTwo  , int min , int max ){
		
		try {

			//now convert them to a string
			int objIndex = userGraphs.get ( user_id ).get( makeGraphKey(job_id,user_id,paramOne,paramTwo) ).getObjectives().get ( objective );
			userGraphs.get ( user_id ).get( makeGraphKey( job_id, user_id, paramOne, paramTwo) ).filterGraph( objIndex , min , max );
			
		}catch ( Exception e ){
			
			//print the error
			System.err.println( "ERROR: Graph does not exist , or inccorect score index given. {" + e + "}" );
		}

	}
	

	/*
	 * This function is used to filter the graph
	*/
	void removeFilter ( int user_id , int job_id , String paramOne , String paramTwo ){
		
		try {

			//now convert them to a string
			userGraphs.get ( user_id ).get( makeGraphKey(job_id,user_id,paramOne,paramTwo) ).removeFilter();
			
		}catch ( Exception e ){
			
			//print the error
			System.err.println( "ERROR: Graph does not exist , or inccorect score index given. {" + e + "}" );
		}

	}
	
	
	
	/*
	 * This function is used to free all of our graphs for a particular user.
	 * This will also remove the user from the graph factor completley.
	*/
	void freeUser( int user_id ){
		
		//check if the user exists
		if ( userGraphs.containsKey ( user_id ) ){
			
			//clean the graph from the graph
			userGraphs.remove ( user_id );
			
		}
	}
	
	/*
	 * This is used to quickly generate a a graph key for a graph that a user is viewing.
	*/
	private String makeGraphKey ( int job_id ,  int user_id , String paramOne , String paramTwo ){
		
		//make a graph key
		return user_id + paramOne + paramTwo + job_id;
	}
};
