import java.io.File;
import java.util.HashMap;
import java.util.Map;
import java.util.ArrayList;
import java.util.List;
import java.util.Iterator;
import java.io.IOException;
import java.io.InterruptedIOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.URL;
import java.net.URLDecoder;
import java.nio.charset.Charset;
import java.security.KeyStore;
import java.util.Locale;
import org.apache.http.HttpRequest;
import org.apache.http.HttpResponse;
import org.apache.http.protocol.HttpContext;
import org.apache.http.protocol.HttpRequestHandler;
import org.apache.http.MethodNotSupportedException;
import org.apache.http.HttpException;
import org.apache.http.HttpEntity;
import org.apache.http.HttpEntityEnclosingRequest;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.util.EntityUtils;
import org.apache.http.HttpStatus;
import org.apache.http.HttpResponseInterceptor;

/*
 * - Lewis Maitland (c) 2013 
 * --------------------------------------------------------------------
 * This class is used for handling client requests to the java daemon.
 * Authentication needs to be properly handled.
*/

class AjaxClientHandler implements HttpRequestHandler {
	
	//declare our private fields
	private volatile GraphFactory graphFactory;
	private volatile TokenAuthenticator tokenAuth;
	private volatile GraphCleaner cleaner;
	private volatile List < String > initList; //this list is used to make sure we are not initiating a graph several times
	
	//this is our consrtructor , here we will pass in our authentication and graphfactory classes
	public AjaxClientHandler ( GraphFactory graphFact  , TokenAuthenticator auth  , GraphCleaner cleaner ){
		
		//set our private fields
		this.graphFactory = graphFact;
		this.tokenAuth = auth;
		this.cleaner = cleaner;
		
		//create our new array list to store the init list
		initList = new ArrayList < String > ();
		
		//set the cleaners graph factor and token authenticator it should be cleaning
		cleaner.setGraphFactory ( graphFactory );
		cleaner.setTokenAuthenticator ( tokenAuth );
	}
	
	
	//This function is used for handling our request
	public void handle( HttpRequest request, HttpResponse response, HttpContext context) throws HttpException, IOException
	{
		//get our request handle type
		String method = request.getRequestLine().getMethod().toUpperCase( Locale.ENGLISH );			
			
		//make sure the only query type we are getting is a GET
		if ( !method.equals( "POST" ) )
			throw new MethodNotSupportedException( "ERROR only POST is supported.");

		//next get our target file , also remove any stray forward slashes
		String target = URLDecoder.decode( request.getRequestLine().getUri() , "UTF-8").replaceAll("/", "");

		try {
			
			//try and swtich to execute our function
			switch ( target ){
				
				case "getPoints" :
				
					//get and return our points
					getPoints ( request , response );
					
					break;
					
				case "freeUser" :
				
					//this function is used to free a user from the system.
					//ie remove security token and all RAM usage
					freeUser ( request , response );
					
					break;
					
				case "initGraph" :
				
					//initiate the graph
					initGraph ( request , response );
					
					break;
					
				case "freeGraph" :
				
					//free our graph
					freeGraph ( request , response );
				
					break;
					
				case "filterGraph" :
				
					//filter our graph
					filterGraph ( request , response );
					
					break;
					
				default :
					response.setStatusCode(HttpStatus.SC_NOT_FOUND);
					
					StringEntity entity = new StringEntity(
						"<html><body><h1>File not found</h1></body></html>",
						ContentType.create("text/html", "UTF-8"));
					
					response.setEntity(entity);
					break;
			}

		}catch ( Exception e ){
			
			//if there was an error then log it
			System.err.println ("ERROR: " + e.toString() );
			e.printStackTrace(System.out);
			
			//also print out our error screen
			response.setStatusCode(HttpStatus.SC_BAD_REQUEST);
			
			//set our string response
			StringEntity entity = new StringEntity(
				"<html><body><h1>Bad Request</h1>Inccorect Paramters Supplied</body></html>",
				ContentType.create("text/html", "UTF-8"));
			
			//set our response
			response.setEntity(entity);
			
			//just exit this method
			return;			
		}
	}
	
	/*
	 * This function is used for handling requests to access graph data , for example fetching points 
	*/
	private void getPoints( HttpRequest request , HttpResponse response ) throws Exception
	{	
		//declare our variables
		float x,y,width,height = 0;
		int job_id = -1;
		int userid = -1;
		String auth_token = "";
		String paramOne = "";
		String paramTwo = "";
		String objective = "";
			
		//get our map of paramters from the request
		Map<String,String> requestParams = getParams ( request );
		
		//try and parse our variables that we will need
		x = Float.parseFloat ( requestParams.get( "x" ) );
		y = Float.parseFloat ( requestParams.get( "y" ) );
		width = Float.parseFloat ( requestParams.get( "width" ) );
		height = Float.parseFloat ( requestParams.get( "height" ) );
		auth_token = requestParams.get( "auth_token" );
		job_id = Integer.parseInt ( requestParams.get( "job_id" ) );
		userid = Integer.parseInt ( requestParams.get( "user_id" ) );
		paramOne = requestParams.get( "paramOne" );
		paramTwo = requestParams.get( "paramTwo" );
		objective = requestParams.get( "objective" );
		
		//make sure that we are authenticated
		if ( !this.tokenAuth.authenticate ( userid , auth_token ) )
			throw new HttpException("Authentication Failed.");
		
		//get the points in json format
		String points = graphFactory.getPoints ( userid ,  job_id , objective , paramOne , paramTwo , x , y , width , height );
		
		if ( points == null )
			throw new HttpException("ERROR: Graph does not exists.");
			
		//just return some dummy data at the moment
		response.setStatusCode( HttpStatus.SC_OK );
		
		//update the user id
		cleaner.updateUser ( userid );
			
		//set our string response just some dummy data
		StringEntity entity = new StringEntity(  points , ContentType.create( "text/html" , "UTF-8"));
		
		//set our response
		response.setEntity(entity);

	}
	
	/*
	 * This functiion is used to initiate a graph so that the user can start
	 * making requests to it to get points etc. We willl keep the javascript waiting
	 * for our answer when we actually create the graph under the hood
	*/
	public void initGraph ( HttpRequest request , HttpResponse response ) throws Exception
	{
		
		//set out inital variable values
		int userid = -1;
		int job_id = -1;
		String auth_token = "";
		String pOne = "";
		String pTwo = "";

		//get our map of paramters from the request
		Map<String,String> requestParams = getParams ( request );
		
		//try and parse our variables that we will need
		auth_token = requestParams.get( "auth_token" );
		pOne = requestParams.get( "paramOne" );
		pTwo = requestParams.get( "paramTwo" );
		job_id = Integer.parseInt ( requestParams.get( "job_id" ) );
		userid = Integer.parseInt ( requestParams.get( "user_id" ) );
		
		//make sure that we are authenticated
		if ( !this.tokenAuth.authenticate ( userid , auth_token ) )
			throw new HttpException("Authentication Failed.");
		
		//check if we are on the init list
		String initKey = new String ( userid + pOne + pTwo + job_id );
		
		//add our graph to the init list
		checkInit( initKey );
		
		//ask our graph factory to create the graph under the hood
		LayeredQuadTree graph = graphFactory.initGraph( userid , job_id , pOne , pTwo );
		
		//make sure we got something
		if ( graph == null ){
			
			//if we could init our graph then we need to log the error
			System.err.println("ERROR: Could not initiate the graph.");
			
			//if we could not init it then we should remove it
			initList.remove ( initKey );
			
			//now we need to set the response
			throw new HttpException("Failed to init the graph.");
		}

		//get the dimensions of our graph
		float[] dimensions = graph.getDimensions();
		
		//if we could init it then we should remove it
		initList.remove ( initKey );
	
		//jsut return some dummy data at the moment
		response.setStatusCode( HttpStatus.SC_OK );
		
		//update the user id
		cleaner.updateUser ( userid );		
		
		//set our string response just some dummy data
		StringEntity entity = new StringEntity(  "{ \"status\" : \"success\" , \"dimensions\" : {" +
													" \"x\" : " + dimensions[0] + 
													", \"y\" : " + dimensions[1] +
													", \"width\" : " + dimensions[2] +
													", \"height\" : " + dimensions[3] +
													"}}" , ContentType.create( "text/html" , "UTF-8"));
		
		//set our response
		response.setEntity(entity);
	}
	
	/*
	 * This function is used to check if  
	*/
	public void checkInit( String initKey )throws HttpException {
		
		//check if we have the graph in our init list
		if ( initList.contains ( initKey ) )
			throw new HttpException( "ERROR: Graph is already being initalized graph." );
		
		//add the fact that we have initalized our graph to the init list
		initList.add( initKey );
	}
	
	/*
	 * This functiion is used to free a single graph from the system when the user is done looking at it.
	 * Will be mostly used when a user presses the back button ewtc.
	*/
	public void freeGraph ( HttpRequest request , HttpResponse response ) throws Exception
	{
		
		//set out inital variable values
		int userid = -1;
		int job_id = -1;
		String auth_token = "";
		String pOne = "";
		String pTwo = "";

		//get our map of paramters from the request
		Map<String,String> requestParams = getParams ( request );
		
		//try and parse our variables that we will need
		auth_token = requestParams.get( "auth_token" );
		pOne = requestParams.get( "paramOne" );
		pTwo = requestParams.get( "paramTwo" );
		job_id = Integer.parseInt ( requestParams.get( "job_id" ) );
		userid = Integer.parseInt ( requestParams.get( "user_id" ) );
		
		//make sure that we are authenticated
		if ( !this.tokenAuth.authenticate ( userid , auth_token ) )
			throw new HttpException("Authentication Failed.");
			
		//update the user id
		cleaner.updateUser ( userid );
		
		//delete the graph
		graphFactory.freeGraph( userid , job_id , pOne , pTwo );
	
		//jsut return some dummy data at the moment
		response.setStatusCode( HttpStatus.SC_OK );
		
		//set our string response just some dummy data
		StringEntity entity = new StringEntity(  "{'status' : 'success' }" , ContentType.create( "text/html" , "UTF-8"));
		
		//set our response
		response.setEntity(entity);
	}
	
	/*
	 * This function is used to delete a security token from the token authenticator
	*/
	private void freeUser( HttpRequest request , HttpResponse response ) throws Exception
	{	
		//declare our variables
		int userid = -1;
		String auth_token = "";
			
		//get our map of paramters from the request
		Map<String,String> requestParams = getParams ( request );
		
		//try and parse our variables that we will need
		auth_token = requestParams.get( "auth_token" );
		userid = Integer.parseInt ( requestParams.get( "user_id" ) );
		
		//make sure that we are authenticated
		if ( !this.tokenAuth.authenticate ( userid , auth_token ) )
			throw new HttpException("Authentication Failed.");
			
		//now just remove the token
		this.tokenAuth.removeToken ( userid );
		
		//now free from our graph factor
		this.graphFactory.freeUser ( userid );
			
		//jsut return some dummy data at the moment
		response.setStatusCode( HttpStatus.SC_OK );
		
		//set our string response just some dummy data
		StringEntity entity = new StringEntity( "{ 'status' : 'removed' }", ContentType.create("text/html", "UTF-8"));
		
		//set our response
		response.setEntity(entity);

	}

	
	/*
	 * This function is used to filter our graph
	*/
	private void filterGraph( HttpRequest request , HttpResponse response ) throws Exception
	{	
		
		//set out inital variable values
		int userid = -1;
		int job_id = -1;
		int min = 0;
		int max = 0;
		String auth_token = "";
		String pOne = "";
		String pTwo = "";
		String objective;

		//get our map of paramters from the request
		Map<String,String> requestParams = getParams ( request );
		
		//try and parse our variables that we will need
		auth_token = requestParams.get( "auth_token" );
		pOne = requestParams.get( "paramOne" );
		pTwo = requestParams.get( "paramTwo" );
		job_id = Integer.parseInt ( requestParams.get( "job_id" ) );
		userid = Integer.parseInt ( requestParams.get( "user_id" ) );
		min = Integer.parseInt ( requestParams.get( "min" ) );
		max = Integer.parseInt ( requestParams.get( "max" ) );
		objective = requestParams.get( "objective" );
		
		//make sure that we are authenticated
		if ( !this.tokenAuth.authenticate ( userid , auth_token ) )
			throw new HttpException("Authentication Failed.");
			
		//update the user id
		cleaner.updateUser ( userid );
	
		//now lets filter our graph
		this.graphFactory.filterGraph ( userid , job_id , objective , pOne , pTwo , min , max );
		
		//jsut return some dummy data at the moment
		response.setStatusCode( HttpStatus.SC_OK );
		
		//set our string response just some dummy data
		StringEntity entity = new StringEntity(  "{'status' : 'success' }" , ContentType.create( "text/html" , "UTF-8"));

		//set our response
		response.setEntity(entity);
	}
	
	
	/*
	 * This function is used to filter our graph
	*/
	private void removeFilter( HttpRequest request , HttpResponse response ) throws Exception
	{	
		
		//set out inital variable values
		int userid = -1;
		int job_id = -1;
		String auth_token = "";
		String pOne = "";
		String pTwo = "";

		//get our map of paramters from the request
		Map<String,String> requestParams = getParams ( request );
		
		//try and parse our variables that we will need
		auth_token = requestParams.get( "auth_token" );
		pOne = requestParams.get( "paramOne" );
		pTwo = requestParams.get( "paramTwo" );
		job_id = Integer.parseInt ( requestParams.get( "job_id" ) );
		userid = Integer.parseInt ( requestParams.get( "user_id" ) );
		
		//make sure that we are authenticated
		if ( !this.tokenAuth.authenticate ( userid , auth_token ) )
			throw new HttpException("Authentication Failed.");
			
		//update the user id
		cleaner.updateUser ( userid );
	
		//now lets filter our graph
		this.graphFactory.removeFilter ( userid , job_id , pOne , pTwo );
		
		//jsut return some dummy data at the moment
		response.setStatusCode( HttpStatus.SC_OK );
		
		//set our string response just some dummy data
		StringEntity entity = new StringEntity(  "{'status' : 'success' }" , ContentType.create( "text/html" , "UTF-8"));
		
		//set our response
		response.setEntity(entity);
	}
	
	
	/*
	 * This function is used for getting the post variables from a http request
	*/
	private Map<String,String > getParams( HttpRequest request ) throws Exception
	{
			
		try {
			
			//create our map to hold the key value pairs
			Map<String,String> params = new HashMap<String,String>();
			
			//set our entity to null
			HttpEntity reqEntity = null;
			
			//get our enclosing request entity , ie our post variable string
			if ( request instanceof HttpEntityEnclosingRequest)
				reqEntity = ((HttpEntityEnclosingRequest)request).getEntity();
			
			//create our empty data array
			byte[] data;
			
			//if we didnt have an entity just set it to null
			if (reqEntity == null) {
				data = new byte [0];
				
				//log this error
				System.err.println( "ERROR: No post variables supplied to AjaxClientHandler" );
				
				//we will throw an exception here because we should get paramters from the client
				throw new Exception("ERROR: No POST paramters where supplied");
				
			} else {
				
				//otherwise get our data
				data = EntityUtils.toByteArray(reqEntity);
			}
			
			//now cast our data to a string :)
			String paramString = new String ( data );
			
			//split our string into the paramter pairs
			String paramPairs[] = paramString.split("&");
			
			//iterate through our list of key value pairs
			for ( String paramPair : paramPairs ){
								
				//now we need to split the list further
				String paramKeyValue[] = paramPair.split("=");
				
				//now get our key and value
				String key = paramKeyValue[0];
				String value = "";
				
				if ( paramKeyValue.length == 2 && paramKeyValue[1] != null )
					value = paramKeyValue[1];
				
				//add it to our paramters
				params.put( key , value.replace( "%24" , "$") );
			}
			
			//return our paramters
			return params;
			
			//now we have our paramters ( if any so cast it to a string )
		}catch ( Exception e ){
			
			//log our previous excpetion
			System.err.println( e.toString() );
			
			//if we got an exception says why
			throw new Exception("ERROR: Malformed POST variables.");
		}
	}
}
