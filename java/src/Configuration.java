/*
 * This class is used for configurating our java program.
 * When instantiated the class will fetch all records
 * from the configuration table in the database and then
 * set the appropriate fields in the settings hash map.
*/
import java.util.List;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;
import java.util.HashMap;
import java.io.File;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;
import java.io.FileInputStream;
import java.util.Scanner;
import com.tomgibara.cluster.gvm.flt.*;
import java.sql.*;

public final class Configuration {

	volatile Connection conn = null;
	
	//the key is the user id , the value is the token
	private static volatile Map< String , String > settings;

	public Configuration() throws Exception {
		//register our driver
		Class.forName("com.mysql.jdbc.Driver");
		
		//now initate our connection
		conn = DriverManager.getConnection( "jdbc:mysql://localhost/raven" , "root" , "elephant" );
		
		//create our tokens map
		settings = new HashMap< String , String>();

		//create a statement to search for the authentication id
		Statement stmt = conn.createStatement();
		
		//execute our query on the data base
		ResultSet rs = stmt.executeQuery( "SELECT setting,value FROM configuration;" );

		//loop through and add all of our settings
		while ( rs.next() ){

			//insert out setting into the map
			settings.put( rs.getString("setting") , rs.getString("value") );
		}
	}

	public int asInt ( String string ){

		if ( settings.containsKey( string ) )
			return Integer.parseInt( settings.get( string ) );

		return 0;
	}

	public float asFloat( String string ){

		if ( settings.containsKey( string ) )
			return Float.parseFloat( settings.get( string ) );

		return 0;
	}

	public boolean asBoolean ( String string ){

		if ( settings.containsKey( string ) )
			return settings.get( string ).toLowerCase().equals( "true" );

		return false;
	}

	public String asString ( String string ){

		if ( settings.containsKey( string ) )
			return settings.get( string );

		return "";
	}
}