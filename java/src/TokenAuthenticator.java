import java.sql.*;
import java.util.*;

/*(c) Lewis Maitland
 * -------------------------------------------
 * This class is used for authenticated security
 * tokens when requesting data from the java
 * program.
*/
class TokenAuthenticator {
	
	volatile Connection conn = null;
	
	//the key is the user id , the value is the token
	private static volatile Map< Integer , String > tokens;
	
	//This is our default constructor
	public TokenAuthenticator() throws Exception
	{
		
		//set our graph cleaner
		//register our driver
		Class.forName("com.mysql.jdbc.Driver");
		
		//now initate our connection
		conn = DriverManager.getConnection( "jdbc:mysql://localhost/raven" , "root" , "elephant" );
		
		//create our tokens map
		tokens = new HashMap< Integer , String>();
	}
   
	//This function authenticates a string token against the user id
	public synchronized boolean authenticate ( int user_id , String token ){

		return true;
		//first check to see if the user_id is inside the stored tokens
		/*if ( tokens.containsKey( user_id ) ){
			
			//make sure the value of the authentication token is the same as the one we have supplied
			return ( tokens.get(user_id).equals( token ) );
		
		}else {
			
			try {
				//create a statement to search for the authentication id
				Statement stmt = conn.createStatement();
				
				//execute our query on the data base
				ResultSet rs = stmt.executeQuery( "SELECT security_token FROM users WHERE id = '" + user_id + "';" );
				
				//check if we got something back
				if ( rs.next() ){
					
					//get our fetched auth_token
					String fetched_token = rs.getString( "security_token" );
					
					//put it in our map
					tokens.put ( user_id , fetched_token );
					
					//now return wether its the same or not
					return ( token.equals( fetched_token ) );
					
				}else{
					
					//if we got here it means the user_id was wrong so log it and return
					System.err.println("ERROR: No such user in the database!");
					return false;
				}
			}catch ( Exception e ){
				
				//if we had an exception then log it
				System.err.println( e.toString() );
				
				//return false something went wrong
				return false;
			}
		}//*/
	}
	
	//this function is used to remove a token for a user
	public synchronized void removeToken( int user_id ){
		
		//remove the element at the key
		tokens.remove ( user_id );
	}
}
