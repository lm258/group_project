import java.util.concurrent.ConcurrentHashMap;
import java.util.Iterator;
import java.util.Map;

/*
 * (c) Lewis Maitland
 * This class is used to clean up redundant graphs that are in the system. 
 * Just runs through the graphs in the graph factory and then removes users 
*/
class GraphCleaner extends Thread {
	
	//this holds our access list for users
	//it stores the user id and then the last time that they accessed the graph
	volatile ConcurrentHashMap< Integer , Long > accessMap;
	boolean running = true;
	static final int WAIT_TIME = 1; //how long will we wait between cleanups?
	static final int TIMEOUT_CLEAN = 25;//how long should it be before be clean something up?
	volatile GraphFactory graphFactory;
	volatile TokenAuthenticator tokenAuth;
	
	//set our graph factory and token authenticator
	public GraphCleaner (){
		
		//set our graph factory and token authenticator
		accessMap = new ConcurrentHashMap< Integer , Long > ();
	}
	
	//this function is used to set the graph factory for our cleaner class
	public void setGraphFactory ( GraphFactory gFactory ){
		this.graphFactory = gFactory;
	}
	
	//this function is used to set the token authenticator to clean
	public void setTokenAuthenticator ( TokenAuthenticator tok ){
		this.tokenAuth = tok;
	}
	
	@Override
	public void run() {
		
		while ( running ){
			
			try {		
				//make our thread sleep for WAIT_TIME minutes
				sleep( WAIT_TIME * 1000 * 60 );
				
				//loop through out entries in the acess map
				for ( Map.Entry < Integer , Long > entry : accessMap.entrySet() ) {
					
					//calculate the time ago it was last executed	
					long timeDiff = System.currentTimeMillis() - entry.getValue();
					
					//if it was greater than the cleanout time ago then
					if ( timeDiff >= ( TIMEOUT_CLEAN * 1000 * 60 ) ){
						
						//check before we call any functions on things
						//cleanup the graph
						if ( graphFactory != null )
							graphFactory.freeUser ( entry.getKey() );
							
						if ( tokenAuth != null )
							tokenAuth.removeToken ( entry.getKey() );
							
						//remove the pairs
						accessMap.remove ( entry.getKey() );
						
						//print a wee message so we know lol
						System.err.println("INFO: User cleaned from RAM [" + entry.getKey() + "]" );
						
						//break from our loop
						break;
					}
				}
				
			}catch ( Exception e ){
				
				//print the error message
				System.err.println( "ERROR:" + e );
			}
		}
		
	}
	
	//this is used to store the fact that a user has accessed the something
	public void updateUser( int user_id ){
		
		//put the current time for our user ids into the access list
		accessMap.put ( user_id , System.currentTimeMillis() );
	}
};
