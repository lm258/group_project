
<?php echo $this->Form->create('Config', array('url'=>array('controller'=>'config', 'action'=>'save')) ); ?>
<fieldset>
<?php
    foreach( $configs as $key => $config ){ ?>

        <div class="panel panel-default">
            <div class="panel-heading"><b><? echo $config['Config']['setting']; ?></b></div>
            <div class="panel-body">
                <p><? echo $config['Config']['description']; ?></p>

            <div class="input-group">
                <span class="input-group-addon">Value</span>
                <?
                    echo $this->Form->hidden('Config.'.$key.'.id', array('value' => $config['Config']['id'] , 'class' => 'form-control' , 'label' => false ));
                    echo $this->Form->input('Config.'.$key.'.value' , array('value' => $config['Config']['value'] , 'class' => 'form-control' , 'label' => false ));
                ?>
            </div>

            </div>
        </div>

        <?
    }
?>
</fieldset>
<div class="submit">
    <?php echo $this->Form->submit(__('Save'), array( 'div' => FALSE , 'class' => 'btn btn-primary' )); ?>
    <?php echo $this->Html->link( 'Restore Default', '/config/restore' , array('class' => 'btn btn-danger') ); ?>
</div>

<? $this->Form->end(); ?>
