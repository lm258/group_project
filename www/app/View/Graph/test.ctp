<script src="/js/d3.v3.min.js" ></script>
	<style>

	#frame {
		position: relative;
		width: 580px;
		height: 600px;
		border : solid 0.4px black;
	}

	svg {
      position: absolute;
      top: 0;
      left: 30px;
    }
    
	canvas {
		position: absolute;
		top: 0;
		left: 60px;
		background-color:#FFFFFF;
    }
    
    rect { fill: transparent; }
	.axis path,
	.axis line {
	  fill: none;
	  stroke: #778899;
	}

	.loading {
		font-family: sans-serif;
		font-size: 25px;
	}
	
	circle {
		opacity:0.7;
	}
	

	</style>
<script src="/js/graph.js" ></script>

<div id = "scatter-plot" ></div>
<script>
	var user_id = 1; //the user , this will be retreived from the database
	var job_id = 12; //job_id we want to view
	var graph_id = 0;//we want to get the first graph , only used of we have 'overlayed' graphs
	var auth_token = "test_token";
	var graph = new Graph ( job_id , user_id , "$x1" , "$x2" , auth_token );
	graph.init();
</script>
