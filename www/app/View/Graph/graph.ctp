<script src="/js/d3.v3.min.js" ></script>
<script src="/js/d3.tip.js" ></script>
<style>

    #frame {
        border : solid 0.4px black;
    }

    .scatter-plot {
        background-repeat:no-repeat;
        background-position:center;
        display:inline-block;
    }

    .input-group {
        width:190px;
        margin-left:20px;
    }

    #scatter-position{
        text-align:center;
    }

    svg {
    }

    .tick {
        font-size:10px;
    }

    rect { fill: transparent; }
    .axis path,
    .axis line {
      fill: none;
      stroke: #778899;
    }

    .loading {
        font-family: sans-serif;
        font-size: 25px;
    }

    circle {
        opacity:0.7;
    }

    #tooltip {
        display:block;
        width:128px;
        height:48px;
        background-color:#FF792A;
        border-radius:15px;
        border:1px solid #DE2E00;
        position:relative;
        display:none;
        top:0px;
        bottom:0px;
    }

    .d3-tip {
    line-height: 1;
    font-weight: bold;
    padding: 12px;
    background: rgba(0, 0, 0, 0.8);
    color: #fff;
    border-radius: 2px;
    }

    /* Creates a small triangle extender for the tooltip */
    .d3-tip:after {
    box-sizing: border-box;
    display: inline;
    font-size: 10px;
    width: 100%;
    line-height: 1;
    color: rgba(0, 0, 0, 0.8);
    content: "\25BC";
    position: absolute;
    text-align: center;
    }

    /* Style northward tooltips differently */
    .d3-tip.n:after {
    margin: -1px 0 0 0;
    top: 100%;
    left: 0;
    }

    .content-panels {
    width:95%;
    margin-right:auto;
    margin-left:auto;
    margin-top:10px;
    }

    .panel-heading{
    text-align:left;
    }

    .anchor {
        position:relative;
        top:-100px;
    }

    .hover {
    margin-right:8px;
    }

    .panel-body {
        background-color:#F9FCFF;
    }

    .panel-primary {
        background-color:#EAF7FF;
    }

    #remove:hover {
        color:#FF0000;
    }

    #duplicate:hover {
        color:#00FF00;
    }

    body { padding-top: 100px; }

</style>

<script src="/js/graph.js" ></script>
<script src="/js/panel.js" ></script>

<div id="scatter-position" >
<div id="tutorial" ></div>
<div id="tooltip" ><p></p></div>

<div class="content-panels" id="graph-panels" ></div>

<script>

    function displayTutorial(){
        var content = '<div class="container" style="margin-top:30px;">'
                        + '<div class="col-md-4"><div class="alert alert-info" ><h4>1- Add a scatter plot </h4><p>'
                        + '<button class="btn btn-success btn-xs lm_tooltip"><span class="glyphicon glyphicon-plus"></span></button>'
                        + '<span> Click on this button to add a new scatterplot. It will store it into a tabs.</span>'
                        +'</p></div></div>'
                        + '<div class="col-md-4"><div class="alert alert-info" ><h4>2- Select parameters </h4><p>'
                        + '<button id="ok" class="btn btn-default btn-sm ">Ok</button>'
                        + '<span> Then click on this button to validate the parameters</span>'
                        + '</p><p><button class="btn btn-primary btn-sm" type="button">Compare<b class="caret"></b></button>'
                        + '<span> Then click on this button to add a new graph to compare with</span>'
                        +'</p></div></div>'
                        + '<div class="col-md-4"><div class="alert alert-info" ><h4>3- Do actions with it </h4><p>'
                        + '<button class="btn btn-default btn-sm">Tools<b class="caret"></b></button>'
                        + '<span> This button allows to manipulate the graph...</span>'
                        +'</p></div></div>'
                        +'</div>';

        document.getElementById("tutorial").innerHTML = content;
    }
    
    displayTutorial();


    /* This piece of code is for logging us out after a set amount of time */
    window.timeOut = setTimeout( function(){ window.location.href="/users/login"; }  , 1500000 );
    window.onclick = function(e) {
        clearTimeout( window.timeOut );
        window.timeOut = setTimeout( function(){ window.location.href="/users/login"; }  , 1500000 ); 
    };

    var user_id = <?php echo $user_id; ?>; //the user , this will be retreived from the database
    var job_id = <?php echo $job_id; ?>; //job_id we want to view
    var auth_token = "<?php echo $auth_token; ?>";

    $(".merge-btn").prop('disabled',true);
    $("#split").prop('disabled',true);
    
    $('#ok').on ( 'click' , function() {
        
        var pOne = $("#param-1").val();
        var pTwo = $("#param-2").val();
        var obj = $("#objective").val();

        if ( window.PanelGlobals.CurrentPanel != null && window.PanelGlobals.CurrentPanel.hasFirstGraph() == false ){

            window.PanelGlobals.CurrentPanel.addFirstGraph( job_id , user_id , obj , pOne , pTwo , auth_token );

        }else if ( window.PanelGlobals.CurrentPanel != null && window.PanelGlobals.CurrentPanel.hasFirstGraph() == true ){

            window.PanelGlobals.CurrentPanel.updateFirstGraph ( job_id , user_id , obj , pOne , pTwo , auth_token );

        }

    });
    
    $('#ok2').on ( 'click' , function() {
        
        var pOne = $("#param-12").val();
        var pTwo = $("#param-22").val();
        var obj = $("#objective2").val();
        
        if ( window.PanelGlobals.CurrentPanel != null && window.PanelGlobals.CurrentPanel.hasSecondGraph() == false ){

            window.PanelGlobals.CurrentPanel.addSecondGraph( job_id , user_id , obj , pOne , pTwo , auth_token );

             $(window.PanelGlobals.CurrentPanel.panel).find("#padlock" + window.PanelGlobals.CurrentPanel.id).css('display' , 'inline-block');
            $(".merge-btn").prop('disabled',false);

        }else if ( window.PanelGlobals.CurrentPanel != null && window.PanelGlobals.CurrentPanel.hasSecondGraph() == true ){

            window.PanelGlobals.CurrentPanel.updateSecondGraph ( job_id , user_id , obj , pOne , pTwo , auth_token );

        }

    });
    
    $('#reset').on ( 'click' , function() {

        if ( window.PanelGlobals.CurrentPanel != null ){
            for ( var i = 0 ; i < window.GraphGlobals.Graphs.length ; i ++ ){
                if ( window.GraphGlobals.Graphs[i].panelID == window.PanelGlobals.CurrentPanel.id ){
                    window.GraphGlobals.Graphs[i].reset();
                }
            }
        }
    });
    
    $('#zoomIn').on ( 'click' , function() {
        if ( window.PanelGlobals.CurrentPanel != null ){
            for ( var i = 0 ; i < window.GraphGlobals.Graphs.length ; i ++ ){
                if ( window.GraphGlobals.Graphs[i].panelID == window.PanelGlobals.CurrentPanel.id )
                    window.GraphGlobals.Graphs[i].zoomIn();
            }
        }
    });
    
    $('#zoomOut').on ( 'click' , function() {
        if ( window.PanelGlobals.CurrentPanel != null ){
            for ( var i = 0 ; i < window.GraphGlobals.Graphs.length ; i ++ ){
                if ( window.GraphGlobals.Graphs[i].panelID == window.PanelGlobals.CurrentPanel.id )
                    window.GraphGlobals.Graphs[i].zoomOut();
            }
        }
    });
    
    $('#arrowL').on ( 'click' , function() {
        if ( window.PanelGlobals.CurrentPanel != null ){
            for ( var i = 0 ; i < window.GraphGlobals.Graphs.length ; i ++ ){
                if ( window.GraphGlobals.Graphs[i].panelID == window.PanelGlobals.CurrentPanel.id )
                    window.GraphGlobals.Graphs[i].move( [5,0] );
            }
        }
    });
    
    $('#arrowR').on ( 'click' , function() {
        if ( window.PanelGlobals.CurrentPanel != null ){
            for ( var i = 0 ; i < window.GraphGlobals.Graphs.length ; i ++ ){
                if ( window.GraphGlobals.Graphs[i].panelID == window.PanelGlobals.CurrentPanel.id )
                    window.GraphGlobals.Graphs[i].move( [-5,0] );
            }
        }
    });
    
    $('#arrowU').on ( 'click' , function() {
        if ( window.PanelGlobals.CurrentPanel != null ){
            for ( var i = 0 ; i < window.GraphGlobals.Graphs.length ; i ++ ){
                if ( window.GraphGlobals.Graphs[i].panelID == window.PanelGlobals.CurrentPanel.id )
                    window.GraphGlobals.Graphs[i].move([0,5]);
            }
        }
    });

    $('#arrowD').on ( 'click' , function() {
        if ( window.PanelGlobals.CurrentPanel != null ){
            for ( var i = 0 ; i < window.GraphGlobals.Graphs.length ; i ++ ){
                if ( window.GraphGlobals.Graphs[i].panelID == window.PanelGlobals.CurrentPanel.id )
                    window.GraphGlobals.Graphs[i].move([0,-5]);
            }
        }
    });

    $('#addPanel').on ( 'click' , function() {

        var pOne = $("#param-1").val();
        var pTwo = $("#param-2").val();
        var obj = $("#objective").val();
        var pOne2 = $("#param-12").val();
        var pTwo2 = $("#param-22").val();
        var obj2 = $("#objective2").val();

        var panel = new Panel();
        panel.init();
        panel.addFirstGraph ( job_id , user_id , obj , pOne , pTwo , auth_token );

        if ( window.PanelGlobals.CurrentPanel == null )
            panel.select();
    });
    
    $('#sync').on( 'click' , function() {
        window.GraphGlobals.SyncMovement = $('#sync').is(':checked');
    });
    
    $('#tip').on( 'click' , function() {
        window.GraphGlobals.ToolTip = $('#tip').is(':checked');
    });

    $('#remove').on ( 'click' , function() {

        if ( window.PanelGlobals.CurrentPanel != null && window.PanelGlobals.CurrentPanel.hasSecondGraph() ){

            window.PanelGlobals.CurrentPanel.removeSecondGraph();

            $(window.PanelGlobals.CurrentPanel.panel).find("#padlock" + window.PanelGlobals.CurrentPanel.id).css('display' , 'none');
            $("#lock-right").children("i").attr("class","fa fa-lock");
            $("#lock-left").children("i").attr("class","fa fa-lock");

            $(".merge-btn").prop('disabled',true);
            $("#split").prop('disabled',true);
        }

    });
    
    /* This needs done! */
    $("#filter").on('click' , function() {

        if ( window.PanelGlobals.CurrentPanel != null ){
            var fMin = parseInt ( $('#filterMin').val() , 10 );
            var fMax = parseInt ( $('#filterMax').val() , 10 );
            
            if ( fMin != "NaN" && fMax != "NaN" ){
                window.PanelGlobals.CurrentPanel.filterGraphs( fMin , fMax );
            }
        }
    });
    
    $("#merge-left").on ( 'click' , function() {

        if ( window.PanelGlobals.CurrentPanel != null  && window.PanelGlobals.CurrentPanel.hasSecondGraph() 
                && window.PanelGlobals.CurrentPanel.hasFirstGraph() && window.PanelGlobals.CurrentPanel.merged == false ){

            window.PanelGlobals.CurrentPanel.mergeFirstToSecond();
            $(window.PanelGlobals.CurrentPanel.panel).find("#padlock" + window.PanelGlobals.CurrentPanel.id).css('display' , 'none');

            $(".merge-btn").prop('disabled',true);
            $("#split").prop('disabled',false);
            $("#sync").attr("disabled",true);
            $("#remove").attr("disabled",true);
            $("#ok2").attr("disabled",true);
            $("#ok").attr("disabled",true);
            $(window.PanelGlobals.CurrentPanel.panel).find("#lock-left").attr("disabled",true);
            $(window.PanelGlobals.CurrentPanel.panel).find("#lock-right").attr("disabled",true);
        }

    });
    
    $("#merge-right").on ( 'click' , function() {

        if ( window.PanelGlobals.CurrentPanel != null  && window.PanelGlobals.CurrentPanel.hasSecondGraph() 
                && window.PanelGlobals.CurrentPanel.hasFirstGraph() && window.PanelGlobals.CurrentPanel.merged == false ){

            window.PanelGlobals.CurrentPanel.mergeSecondToFirst();
            $(window.PanelGlobals.CurrentPanel.panel).find("#padlock" + window.PanelGlobals.CurrentPanel.id).css('display' , 'none');

            $(".merge-btn").prop('disabled',true);
            $("#split").prop('disabled',false);
            $("#sync").attr("disabled",true);
            $("#remove").attr("disabled",true);
            $("#ok2").attr("disabled",true);
            $("#ok").attr("disabled",true);
            $('#sync').prop('checked', true);
            $(window.PanelGlobals.CurrentPanel.panel).find("#lock-left").attr("disabled",true);
            $(window.PanelGlobals.CurrentPanel.panel).find("#lock-right").attr("disabled",true);
        }
    });
    
    $("#split").on ( 'click' , function() {
        if ( window.PanelGlobals.CurrentPanel != null && window.PanelGlobals.CurrentPanel.merged == true ){
            
            window.PanelGlobals.CurrentPanel.split();

            $(window.PanelGlobals.CurrentPanel.panel).find("#padlock" + window.PanelGlobals.CurrentPanel.id).css('display' , 'inline-block');
            $(window.PanelGlobals.CurrentPanel.panel).find("#lock-right").children("i").attr("class","fa fa-lock");
            $(window.PanelGlobals.CurrentPanel.panel).find("#lock-left").children("i").attr("class","fa fa-lock");

            $(".merge-btn").prop('disabled',false);
            $(this).prop('disabled',true);
            $("#sync").attr("disabled",false);
            $('#sync').prop('checked', false);
            $("#remove").attr("disabled",false);
            $("#ok2").attr("disabled",false);
            $("#ok").attr("disabled",false);
            $(window.PanelGlobals.CurrentPanel.panel).find("#lock-left").attr("disabled",false);
            $(window.PanelGlobals.CurrentPanel.panel).find("#lock-right").attr("disabled",false);
        }
    });

    $( '#graph-tabs' ).on ( 'click' , '.tab-remove' , function() {
        
        var id = $(this).attr('id');
        var index = i;
        var panel = null;

        for(var i = 0 ; i < window.PanelGlobals.Panels.length ; i ++ ){

            if ( window.PanelGlobals.Panels[i].id == id ){
                panel = window.PanelGlobals.Panels[i];
                index = i;
                break;
            }
        }

        panel.remove();

        if ( window.PanelGlobals.Panels.length > 0 ){

            if ( index > 0 ){
                window.PanelGlobals.Panels[ index - 1 ].select();
                window.location.hash="tab-" + window.PanelGlobals.Panels[ index - 1 ].id; 
            }else{
                window.PanelGlobals.Panels[ 0 ].select();
                window.location.hash="tab-" + window.PanelGlobals.Panels[ 0 ].id; 
            }
        }
    });

    $( '#graph-tabs' ).on ( 'click' , '.tab-panel' , function() {
        

        var id = $(this).attr('id');

        var panel = jQuery.grep( window.PanelGlobals.Panels , function( n, i ) {
            return (n.id == id);
        });

        panel[0].select();
        window.PanelGlobals.CurrentPanel = panel[0];
    });

    $( '#graph-panels' ). on ( 'click' , '#lock-right' ,  function() {

        if ( window.PanelGlobals.CurrentPanel != null && window.PanelGlobals.CurrentPanel.merged == false && 
            window.PanelGlobals.CurrentPanel.hasSecondGraph() && window.PanelGlobals.CurrentPanel.hasFirstGraph() ){

             if ( window.PanelGlobals.CurrentPanel.lockedRight == false && window.PanelGlobals.CurrentPanel.lockedLeft == false ){

                window.PanelGlobals.CurrentPanel.lockRight();

                $(window.PanelGlobals.CurrentPanel.panel).find("#lock-left").attr("disabled",true);
                $(window.PanelGlobals.CurrentPanel.panel).find("#lock-right").children("i").attr("class","fa fa-unlock");

            }else{
                window.PanelGlobals.CurrentPanel.unlock();
                $(window.PanelGlobals.CurrentPanel.panel).find("#lock-right").children("i").attr("class","fa fa-lock");
                $(window.PanelGlobals.CurrentPanel.panel).find("#lock-right").attr("disabled",false);
                $(window.PanelGlobals.CurrentPanel.panel).find("#lock-left").attr("disabled",false);
            }
        }
    });


    $( '#graph-panels' ). on ( 'click' , '#lock-left' ,  function() {

        if ( window.PanelGlobals.CurrentPanel != null && window.PanelGlobals.CurrentPanel.merged == false && 
            window.PanelGlobals.CurrentPanel.hasSecondGraph() && window.PanelGlobals.CurrentPanel.hasFirstGraph() ){

             if ( window.PanelGlobals.CurrentPanel.lockedLeft == false && window.PanelGlobals.CurrentPanel.lockedRight == false ){

                window.PanelGlobals.CurrentPanel.lockLeft();

                $(window.PanelGlobals.CurrentPanel.panel).find("#lock-right").attr("disabled",true);
                $(window.PanelGlobals.CurrentPanel.panel).find("#lock-left").children("i").attr("class","fa fa-unlock");

            }else{
                window.PanelGlobals.CurrentPanel.unlock();
                $(window.PanelGlobals.CurrentPanel.panel).find("#lock-left").children("i").attr("class","fa fa-lock");
                $(window.PanelGlobals.CurrentPanel.panel).find("#lock-right").attr("disabled",false);
                $(window.PanelGlobals.CurrentPanel.panel).find("#lock-left").attr("disabled",false);
            }

        }
    });

    $( window ).mousemove( function(e) {
        window.GraphGlobals.Scroll[0] = $(window).scrollLeft();
        window.GraphGlobals.Scroll[1] = $(window).scrollTop();
    })
    
    
    function contains(a, obj) {
		for (var i = 0; i < a.length; i++) {
			if (a[i] === obj) {
				return true;
			}
		}
		return false;
	}
    
    $( window ).resize(function() {
        
        if(typeof window.timeOut != 'undefined'){ 
            clearTimeout(window.timeOut);
        }
        window.timeOut = setTimeout(function(){
            jQuery.map( window.GraphGlobals.Graphs , function(x) {	
				x.reload();
            });
            },300);               
    });
    
</script>
