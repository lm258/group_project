<div id="jobs-fancy" >

<div class="input-group">
	<input type="text" placeholder="Search" class="form-control search">
	<div class="input-group-btn">
		<button class="sort btn btn-primary" data-sort="name"> Sort by name </button>
		<button class="sort btn btn-primary" data-sort="total"> Sort by total </button>
	</div>
</div>
<br/>

<div class='list' >
<?php

	//loop through our jobs
	foreach ( $jobs as $job ){ ?>
		<div class="panel panel-default">
		  <div class="panel-heading name">
		  	<h3 class="panel-title" ><b><? echo $job['name']; ?></b></h3>
		  </div>
		  <div class="panel-body">
		  	<div class="well well-sm">
		    	<p><? echo $job['description']; ?></p>
		    </div>

		    <div class="btn-group">
				<button type="button" class="btn btn-default">Completed: 
					<span class="label label-success"><? echo $job['completed_simulations']; ?></span>
				</button>
				<button type="button" class="btn btn-default">Rejected:
					<span class="label label-warning"><? echo $job['rejected_simulations']; ?></span>
				</button>
				<button type="button" class="btn btn-default">Failed:
					<span class="label label-danger"><? echo $job['failed_simulations']; ?></span>
				</button>
				<button type="button" class="btn btn-default">Total: 
					<span class="label label-default total"><? echo $job['total_simulations']; ?></span>
				</button>
				<button type="button" class="btn btn-default">Rating:
					<? echo str_repeat( "&#9733;" , $job['rating'] ); ?>
				</button>
			</div>
		    <a href="/graph/graph/<? echo $job['id']; ?>" class="btn btn-info" role="button">View</a>
		  </div>
		</div>
		<?
	}
?>
</div>
</div>

<script type='text/javascript' >
var options = {
  valueNames: [ 'name', 'total' ]
};

var userList = new List('jobs-fancy',options);
</script>
