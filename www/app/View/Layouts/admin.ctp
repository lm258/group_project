<?php
/**
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.View.Layouts
 * @since         CakePHP(tm) v 0.10.0.1076
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

$description = __d('epistermy', 'Interactive Administrator Interface');
?>
<!DOCTYPE html>
<html>
<head>
	<?php echo $this->Html->charset(); ?>
	<title>
		<?php echo $description ?>:
		<?php echo $title_for_layout; ?>
	</title>
	<?php
		echo $this->Html->meta('icon');

		echo $this->Html->script('jquery-2.0.3.min');
		echo $this->Html->script('datatables.min.js');
		echo $this->Html->script('list.js');
		echo $this->Html->script('bootstrap');
		echo $this->Html->css('bootstrap');
		echo $this->Html->script('dataTables.tableTools');
		echo $this->Html->css('cake.generic');

		echo $this->fetch('meta');
		echo $this->fetch('css');
		echo $this->fetch('script');
	?>

</head>
<body>
	<div id="container">
		<div id="header">
			<h1><?php echo $this->Html->link($description, 'http://127.0.0.1/about'); ?></h1>
			<ul>
				<li><?php echo $this->Html->link( 'Users' , array('controller' => 'users', 'action' => 'users')); ?></li>
				<li><?php echo $this->Html->link( 'Jobs' , array('controller' => 'jobs', 'action' => 'jobs')); ?></li>
				<li><?php echo $this->Html->link( 'Account' , array('controller' => 'users', 'action' => 'account')); ?></li>
				<li><?php echo $this->Html->link( 'Configuration' , array('controller' => 'config', 'action' => 'index')); ?></li>
				<li><?php echo $this->Html->link( 'Logout' , array('controller' => 'users', 'action' => 'logout')); ?></li>
			</ul>
		</div>
		<div id="content">

			<?php echo $this->Session->flash(); ?>

			<?php echo $this->fetch('content'); ?>
		</div>
		<div id="footer">
			<?php echo $this->Html->link( '&copy; Epistemy 2013' , 'http://www.epistemy.com/' , array('escape'=>false) );
			?>
		</div>
	</div>
</body>
</html>
