<!DOCTYPE html>
<html>
<head>
    <link href="/css/bootstrap.css" rel="stylesheet">
    <link href="/css/font-awesome.css" rel="stylesheet">
    <script src="/js/jquery-2.0.3.min.js" type="text/javascript" ></script>
    <script src="/js/bootstrap.js"></script>
        
    
    <style>
        table.except {
            
        }
        
        ul.except, li.except {
        margin: 0 0px;
        }
        
        .input-group.button {
            width:auto;
        }
        
        a.dropdown-toggle {

        }
        
        .navbar-brand{
            padding-right:0px;
        }

        .padlocks {
            width:40px;
            height:100%;
            display:inline-block;
            margin-left:30px;
            vertical-align:top;
            display:none;
        }
        
        .dropdown-menu {background-color: rgba(255, 255, 255, 0.7);;}
        
        
    </style>

</head>
<body>

<script>

    $(function() {
        $("ul.dropdown-menu").on("click", "[data-stopPropagation]", function(e) {
            e.stopPropagation();
        });
    });

    $(function () {
        $('.dropdown').on({
            "shown.bs.dropdown": function() {
                $(this).data('closable', false);
            },
            "click": function() {
                $(this).data('closable', true);
            },
            "hide.bs.dropdown": function() {
                return $(this).data('closable');
            }
        });
    });

</script>           
            
            
<nav class="navbar navbar-default navbar-fixed-top" role="navigation" style="margin-bottom:0px;">
  <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="/graph/jobs/"><button type="button" class="btn btn-default btn-sm lm_tooltip" data-toggle="tooltip" data-placement="right" title="Return to jobs list" ><span class="glyphicon glyphicon-arrow-left"></span></button></a>
    </div>

  <!-- Collect the nav links, forms, and other content for toggling -->
    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul class="nav navbar-nav">
            <li>    
                <div  class="input-group input-group-sm" style="margin-top:15px;">
                    <span class="input-group-addon">Objective</span>
                    <select class="form-control" id="objective" >
                        <?php foreach( $objectives as $obj ) echo "<option>{$obj["name"]}</option>"; ?>
                    </select>
                </div>
            </li>  
            <li>  
                <div  class="input-group input-group-sm" style="margin-top:15px;">
                    <span class="input-group-addon">Param 1</span>
                    <select class="form-control" id="param-1" >
                        <?php foreach( $parameters as $param ) echo "<option>{$param["name"]}</option>"; ?>
                    </select>
                </div>
            </li>  
            <li>  
                <div  class="input-group input-group-sm" style="margin-top:15px;">
                    <span class="input-group-addon">Param 2</span>
                    <select class="form-control" id="param-2">
                        <?php foreach( $parameters as $param ) echo "<option>{$param["name"]}</option>"; ?>
                    </select>
                </div>
            </li>
            <li> 
                <div  class="input-group button input-group-sm" style="margin-top:15px;">
                    <button class="btn btn-default btn-sm lm_tooltip" data-toggle="tooltip" data-placement="bottom" title="Apply paramters" id="ok" >Ok</button>
                </div>
            </li> 
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" style="padding-top:15px;"  data-toggle="dropdown"> <button type="button" class="btn btn-primary btn-sm lm_tooltip" data-toggle="tooltip" data-placement="bottom" title="Compare against other graph">Compare <b class="caret"></b></button></a>
                <ul class="dropdown-menu" style="right: 0px; left: -695px;">
                    <li  data-stopPropagation="true">
                        <ul class="nav navbar-nav">
                            <li>
                                <div class="input-group input-group-sm" style="margin-top:15px;" >
                                    <span class="input-group-addon">Objective</span>
                                    <select class="form-control" id="objective2" >
                                        <?php foreach( $objectives as $obj ) echo "<option>{$obj["name"]}</option>"; ?>
                                    </select>
                                </div>
                            </li>
                            <li>
                                <div class="input-group input-group-sm" style="margin-top:15px;" >
                                    <span class="input-group-addon">Param 1</span>
                                    <select class="form-control" id="param-12" >
                                        <?php foreach( $parameters as $param ) echo "<option>{$param["name"]}</option>"; ?>
                                    </select>
                                </div>
                            </li>
                            <li>
                                <div class="input-group input-group-sm" style="margin-top:15px;">
                                    <span class="input-group-addon">Param 2</span>
                                    <select class="form-control" id="param-22">
                                        <?php foreach( $parameters as $param ) echo "<option>{$param["name"]}</option>"; ?>
                                    </select>
                                </div>
                            </li>
                            <li>
                                <div class="btn-group btn-sm lm_tooltip" data-toggle="tooltip" data-placement="bottom" title="Apply/Add parameters to second graph" style="margin-left:20px;margin-top:10px;">
                                    <button class="btn btn-default btn-sm" id="ok2" >Ok</button>
                                </div>
                            </li>
                            <li>
                                <div class="btn-group btn-sm lm_tooltip" data-toggle="tooltip" data-placement="bottom" title="Remove second graph" style="margin-top:10px;margin-bottom:10px;">
                                    <button type="button" class="btn btn-danger btn-sm" id="remove">Remove</span></button>
                                </div>
                            </li>
                        </ul>
                    </li>                   
                </ul>
            </li>
        </ul>   
        <ul class="nav navbar-nav navbar-right">
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" style="padding-top:15px;" data-toggle="dropdown"> <button type="button" class="btn btn-default btn-sm lm_tooltip" data-toggle="tooltip" data-placement="left" title="Show tools">Tools <b class="caret"></b></button></a>                         
                <ul class="dropdown-menu" style="min-width:170px;">
                    <li data-stopPropagation="true">        
                        <ul class="nav navbar-nav">
                            <div style="margin: 22px 25px 20px 25px;">
                                <li>
                                    <table id="pad" class="except" style="margin-left: 5px;">
                                        <tr>
                                            <td>
                                            </td>
                                            <td>
                                                <button class="btn btn-primary btn-sm" type="button" id="arrowU" class="arrows">
                                                    <span class="glyphicon glyphicon-circle-arrow-up"></span>
                                                </button>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <button class="btn btn-primary btn-sm" type="button" id="arrowL" class="arrows">
                                                    <span class="glyphicon glyphicon-circle-arrow-left"></span>
                                                </button>
                                            </td>
                                            <td>
                                            </td>
                                            <td>
                                                <button class="btn btn-primary btn-sm" type="button" id="arrowR" class="arrows">
                                                    <span class="glyphicon glyphicon-circle-arrow-right"></span>
                                                </button>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                            </td>
                                            <td>
                                                <button class="btn btn-primary btn-sm" type="button" id="arrowD" class="arrows">
                                                    <span class="glyphicon glyphicon-circle-arrow-down"></span>
                                                </button>
                                            </td>
                                            <td>
                                            </td>
                                        </tr>
                                    </table>
                                    <table id="actions" class="except" style="margin-top: 20px; margin-left: 5px;">                 
                                        <tr>
                                            <td>
                                                <button class="btn btn-primary btn-sm" type="button" id="zoomOut">
                                                    <span class="glyphicon glyphicon-zoom-out"></span>
                                                </button>
                                            </td>
                                            <td>
                                                <button class="btn btn-primary btn-sm" type="button" id="reset">
                                                    <span class="glyphicon glyphicon-refresh"></span>
                                                </button>
                                            </td>
                                            <td>
                                                <button class="btn btn-primary btn-sm" type="button" id="zoomIn">
                                                    <span class="glyphicon glyphicon-zoom-in"></span>
                                                </button>
                                            </td>
                                        </tr>
                                    </table>
                                </li>
                                <li class="divider"></li>
                                <li>
                                    <div class="checkbox" style="margin-top: 10px;">
                                        <label>
                                            <input type="checkbox" id="tip" checked> Tool Tip
                                        </label>
                                    </div>
                                    <div id="mix" style="">
                                        <button id="merge-right" type="button" class="btn btn-default btn-sm merge-btn lm_tooltip" data-toggle="tooltip" data-placement="left" title="Merge graphs right to left" style="margin-bottom: 20px;">
                                            <font color="#E680CC" ><span class="glyphicon glyphicon-th"></span></font><span class="glyphicon glyphicon-arrow-right"></span><font color="#5CBD5C" ><span class="glyphicon glyphicon-th"></span></font>
                                        </button>
                                        <button id="merge-left" type="button" class="btn btn-default btn-sm merge-btn lm_tooltip" data-toggle="tooltip" data-placement="left" title="Merge graphs left to right" style="margin-bottom: 20px;">
                                            <font color="#5CBD5C" ><span class="glyphicon glyphicon-th"></span></font><span class="glyphicon glyphicon-arrow-left"></span><font color="#E680CC" ><span class="glyphicon glyphicon-th"></span></font>
                                        </button>
                                        <button id="split" type="button" class="btn btn-default btn-sm" style="margin-bottom: 20px;">
                                            <span class="glyphicon glyphicon-transfer"></span> 
                                        </button>
                                    </div>
                                    <div class="img-thumbnail">
                                        <div class="input-group input-group-sm" style="margin-left:0; width: 110px;">
                                            <span class="input-group-addon">&lt;</span>
                                            <input type="text" class="form-control" placeholder="Top" id="filterMax" >
                                        </div>
                                        <div class="input-group input-group-sm" style="margin-left:0; width: 110px;margin-top: 10px;">
                                            <span class="input-group-addon">&gt;</span>
                                            <input type="text" class="form-control" placeholder="Floor" id="filterMin">
                                        </div>      
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox"> Remove Failed
                                            </label>
                                        </div>
                                        <button class="btn btn-default btn-sm lm_tooltip" data-toggle="tooltip" data-placement="left" title="Apply filter to graphs(s)" id="filter" >Apply</button>
                                    </div>
                                </li>
                            </div>  
                        </ul>           
                    </li>
                </ul>   
            </li>
        </ul>
    </div><!-- /.navbar-collapse -->

    <div id="graph-tabs" class="well well-sm" style="margin-bottom:0px;" >
        <button class="btn btn-success btn-xs lm_tooltip" data-toggle="tooltip" data-placement="right" title="Add new graph tab" id="addPanel" ><span class="glyphicon glyphicon-plus"></span></button>
    </div>
</nav>

<div>

<?php echo $this->fetch('content'); ?>

</div>

<script type="text/javascript" >
    $(".lm_tooltip").tooltip( { delay: { show: 1000, hide: 100 } } );
</script>
</body>
</html>
