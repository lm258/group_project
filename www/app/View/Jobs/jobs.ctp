
<?php if( $this->Session->read('User.role') === 'admin' ) { ?>

<script type="text/javascript">
    $(document).ready(function() {
        $('#adminJobList').dataTable({
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": "<?php echo $this->Html->Url(array('controller' => 'Jobs', 'action' => 'ajaxJobData' )); ?>",
             /*"fnRowCallback": function( nRow, aaData, iDisplayIndex ) {
				switch(aaData[4])
            
            {
				case "1":
				if(nRow.className == "even")
				{
					$(nRow).css('background-color', '#FF8187');   
				}    
				else
				{
					$(nRow).css('background-color', '#FE575F'); 
				}   
				break;
			case  "2" :
            
				if(nRow.className == "even")
				{
					$(nRow).css('background-color', '#C974FF'); 
				}    
				else
				{
					$(nRow).css('background-color', '#D697FF'); 
				}   
            break;
            case "3":
            
				if(nRow.className == "even")
				{
					$(nRow).css('background-color', '#E38431'); 
				}    
				else
				{
					$(nRow).css('background-color', '#F1A663'); 
				}  
            break;
            case "4":
            
				if(nRow.className == "even")
				{
					$(nRow).css('background-color', '#74C6FF'); 
				}    
				else
				{
					$(nRow).css('background-color', '#97D4FF'); 
				}   
            break;
            case "5":
            
				if(nRow.className == "even")
				{
					$(nRow).css('background-color', '#7AD9AB'); 
				}    
				else
				{
					$(nRow).css('background-color', '#24B26D'); 
				}   
            break;
		}
        },*/
                    "aaSorting": [[ 4, "asc" ]],
			"aoColumns": [ 
				{ "bVisible":    false },
				null,
				{ "bVisible":    false },
				{
					"mRender": function ( data, type, row ) {
						return "<a href='/jobs/users/" + row[0] + "' >" + row [3] + " User(s)</a>";
					}
				},
				null,
				null,
				null,
				null,
				null,		      
				{
					"mRender": function ( data, type, row ) {
						return "<a class='btn btn-default btn-xs' href='/jobs/edit/" + row[0] + "' >Edit</a>";
					}
				},
				   {
						"mRender": function ( data, type, row ) {
						return "<a class='btn btn-danger btn-xs' href='/jobs/delete/" + row[0] + "'  onclick='if (confirm(\"Are you sure you wish to delete job?\")) { return true; } return false;' >Delete</a>";
					}
				}
			],
			"aoColumnDefs": [
			  { "sWidth": "200px", "aTargets": [ 1 ] }
			]
        });
    });
</script>
 
<div class="alert alert-warning alert-dismissable" >
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	<strong>All Jobs</strong><br/>
	This view contains all jobs within the system. 
	To edit a job use the 'edit' link , they can also be deleted using the 'delete' link.<br/>
	To view the users assigned to a particular job click the link in the 'users' column , users can also be assigned to a job in this same way.
</div>
</br>
 
<table id="adminJobList" class="table" >
    <thead>
        <tr>
			<th>Id</th>
            <th>Name</th>
            <th>Description</th>
            <th>#Users</th>
            <th>Rating</th>
            <th>#Total</th>
            <th>#Completed</th>
            <th>#Rejected</th>          
            <th>#Failed</th>
            <th>Edit</th>
            <th>Delete</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td colspan="8" class="dataTables_empty">Loading data from server...</td>
        </tr>
    </tbody>
</table>

<?php } else { ?>

<script type="text/javascript">
    $(document).ready(function() {

        $('#userJobList').dataTable({
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": "<?php echo $this->Html->Url(array('controller' => 'Jobs', 'action' => 'ajaxJobData')); ?>",	
			"aoColumns": [ 
				{ "bVisible":    false },
				null,
				{ "bVisible":    false },
				null,
				null,
				null,
				null,
				null,
				{
					"mRender": function ( data, type, row ) {
						return "<a class='btn btn-default btn-xs' href='/jobs/edit/" + row[0] + "' >Edit</a>";
					}
				},
				   {
						"mRender": function ( data, type, row ) {
						return "<a class='btn btn-danger btn-xs' href='/jobs/delete/" + row[0] + "' onclick='if (confirm(\"Are you sure you wish to delete job?\")) { return true; } return false;' >Delete</a>";
					}
				}
			]
        });
    });
</script>
 
<h1>Jobs</h1>
 
<table id="userJobList" class="table"  cellspacing="0" cellpadding="0"> 
    <thead>
        <tr>
			<th>Id</th>
            <th>Name</th>
            <th>Description</th>
            <th>Rating</th>
            <th>#Total</th>
            <th>#Completed</th>
            <th>#Rejected</th>          
            <th>#Failed</th>
            <th>Edit</th>
            <th>Delete</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td colspan="8" class="dataTables_empty">Loading data from server...</td>
        </tr>
    </tbody>
</table>

<?php } ?>
