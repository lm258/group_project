<script type="text/javascript">

    $(document).ready(function() {
        $('#jobUserList').dataTable({
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": "<?php echo $this->Html->Url(array('controller' => 'Jobs', 'action' => 'ajaxJobUsersData'  , $id )); ?>",
            "fnRowCallback": function( nRow, aaData, iDisplayIndex ) {
            if ( aaData[3] == "admin" )
            {
				$(nRow).attr( 'class' , 'danger' );  
			}},
        "aaSorting": [[ 3, "asc" ]],
			"aoColumns": [ 
				{ "bVisible":    false },null,null,null,
				{
					"mRender": function ( data, type, row ) {
						return "<a href='/users/edit/"+row[0]+"' >Edit User</a>";
					}
				},
				{
						"mRender": function ( data, type, row ) {
						return "<a href='/jobs/unassign/<?echo $id; ?>/"+row[0]+"' >Un-Assign</a>";
					}
				}
			]
        });
	
    });
</script>
 
<div class="alert alert-warning alert-dismissable" >
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	<strong>Users for Job <? echo $name; ?></strong></br>
	This is all users assigned to the job <b><? echo $name; ?></b>.You can remove users from this job or assign new ones by using the 'unassign' and 'Set Users' links.
</div>
</br>
 
<table id="jobUserList" class="table"  cellspacing="0" cellpadding="0">
    <thead>
        <tr>
			<th>Id</th>
            <th>Name</th>
            <th>Email</th>
            <th>Role</th>
            <th>Edit</th>
            <th>Un-assign</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td colspan="8" class="dataTables_empty">Loading data from server...</td>
        </tr>
    </tbody>
</table>

<br/>

<div class='submit'>
	<a href='/jobs/addUsers/<? echo $id; ?>' class="btn btn-primary" >Set Users</a>
	<a href='/jobs/jobs/' class="btn btn-danger" >Back</a>
</div>

