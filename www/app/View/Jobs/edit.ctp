<!-- app/View/Users/account.ctp -->
<br/>
<div>
<?php echo $this->Form->create('Job', array('action' => 'edit')); ?>
    <fieldset>
        <legend><?php echo ('Update Job'); ?></legend>
        <?php echo $this->Form->input('name' , array( 'class' => 'form-control' ) );
        echo $this->Form->input('description' , array('type' => 'textarea' , 'class' => 'form-control' ) );
    ?>
    </fieldset>
        <div class="submit">
            <?php echo $this->Form->submit(__('Save'), array('name' => 'ok', 'div' => FALSE , 'class' => 'btn btn-primary' )); ?>
            <?php echo $this->Html->link( 'Cancel', $this->Session->read('User.Location') , array('class' => 'btn btn-danger') ); ?>
        </div>
        
     <? $this->Form->end(); ?>
</div>
