<script type="text/javascript">
	
	var oTable;
	var selectedUsers =  <? echo $selected_users; ?>;

    $(document).ready(function() {
        oTable = $('#adminJobList').dataTable({
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": "<?php echo $this->Html->Url(array('controller' => 'Users', 'action' => 'ajaxUsersData' )); ?>",
            "fnRowCallback": function( nRow, aData, iDisplayIndex ) {

				for( var i = 0 ; i < selectedUsers.length ; i ++ ){
					if ( selectedUsers[i] == aData[0] ){
						$(nRow).addClass("row_selected");
					}
				}
			
				return nRow;
			},
			"aoColumns": [ 
				/* id */          { "bVisible":    false },
								  null,
								  null,
							      null
			]
        });
        
        
	/* Click event handler */
	$('#adminJobList tbody').on( 'click' , 'tr',function () {
		
		var data = oTable.fnGetData( this );
		
		//$(this).toggleClass('row_selected');
		if ( $(this).hasClass('row_selected') ){

			selectedUsers = jQuery.grep( selectedUsers , function(value) {
			  return value != data[0];
			});

		}else{
			selectedUsers.push( data[0] );
		}

		$(this).toggleClass('row_selected');
		
		$("#input").val( selectedUsers );

	} );
	
    });
</script>

<table id="adminJobList" class="table" >
    <thead>
        <tr>
			<th>Id</th>
            <th>Name</th>
            <th>Email</th>
            <th>Role</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td colspan="8" class="dataTables_empty">Loading data from server...</td>
        </tr>
    </tbody>
</table>

<?php echo $this->Form->create('Job', array ( 'action' => 'addUsers' ) ); ?>
<input type='hidden' name="users" id='input' value='' />
<input type='hidden' name='job_id' value='<? echo $job_id; ?>' />

<div class='submit' >
	<? echo $this->Form->submit(__('Save') , array('name' => 'ok', 'div' => FALSE , 'class' => 'btn btn-primary' ) ); ?>
	<a href='/jobs/users/<? echo $job_id; ?>' class="btn btn-danger" >Cancel</a>
</div>

<?php echo $this->Form->end(); ?>
