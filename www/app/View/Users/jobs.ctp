<script type="text/javascript">

    $(document).ready(function() {
        $('#jobUserList').dataTable({
            "bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": "<?php echo $this->Html->Url(array('controller' => 'Users', 'action' => 'ajaxUserJobsData'  , $id )); ?>",
            /*"fnRowCallback": function( nRow, aaData, iDisplayIndex ) {
				switch(aaData[3])
            
            {
				case "1":
				if(nRow.className == "even")
				{
					$(nRow).css('background-color', '#FF8187');   
				}    
				else
				{
					$(nRow).css('background-color', '#FE575F'); 
				}   
				break;
			case  "2" :
            
				if(nRow.className == "even")
				{
					$(nRow).css('background-color', '#C974FF'); 
				}    
				else
				{
					$(nRow).css('background-color', '#D697FF'); 
				}   
            break;
            case "3":
            
				if(nRow.className == "even")
				{
					$(nRow).css('background-color', '#E38431'); 
				}    
				else
				{
					$(nRow).css('background-color', '#F1A663'); 
				}  
            break;
            case "4":
            
				if(nRow.className == "even")
				{
					$(nRow).css('background-color', '#74C6FF'); 
				}    
				else
				{
					$(nRow).css('background-color', '#97D4FF'); 
				}   
            break;
            case "5":
            
				if(nRow.className == "even")
				{
					$(nRow).css('background-color', '#7AD9AB'); 
				}    
				else
				{
					$(nRow).css('background-color', '#24B26D'); 
				}   
            break;
		}
        },*/
            "aaSorting": [[ 3, "asc" ]],
			"aoColumns": [ 
				{ "bVisible":    false },null,
				{ "bVisible":    false },null,null,null,null,null,
				{
					"mRender": function ( data, type, row ) {
						return "<a class='btn btn-default btn-xs' href='/jobs/edit/"+row[0]+"' >Edit</a>";
					}
				},
				{
						"mRender": function ( data, type, row ) {
						return "<a class='btn btn-danger btn-xs' href='/jobs/unassign/"+row[0]+"/"+row[8]+"' >Un-Assign</a>";
					}
				}
			],
			"aoColumnDefs": [
			  { "sWidth": "200px", "aTargets": [ 1 ] }
			]
        });
	
    });
</script>
 
<div class="alert alert-warning alert-dismissable">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	<strong>Jobs for <? echo $username; ?></strong>
	This is all jobs assigned to <b><? echo $username; ?></b>.You can remove jobs from this users or assign new ones by using the 'unassign' and 'Set Jobs' links.
</div>
</br>
 
<table id="jobUserList" class="table" cellspacing="0" cellpadding="0">
    <thead>
        <tr>
			<th>Id</th>
            <th>Name</th>
            <th>Description</th>
            <th>Rating</th>
            <th>#Total</th>
            <th>#Completed</th>
            <th>#Rejected</th>          
            <th>#Failed</th>
            <th>Edit</th>
            <th>Un-assign</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td colspan="8" class="dataTables_empty">Loading data from server...</td>
        </tr>
    </tbody>
</table>

<br/>

<div class='submit'>
	<a href='/users/addJobs/<? echo $id; ?>' class="btn btn-primary" >Set Jobs</a>
	<a href='/users/users/' class="btn btn-danger" >Back</a>
</div>


