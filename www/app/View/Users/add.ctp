<div class="col-md-8" >
<div class="col-md-8" >
<?php echo $this->Form->create('User'); ?>
    <fieldset>
        <legend><?php echo __('Add User'); ?></legend>
        <?php echo $this->Form->input('username' , array( 'class' => 'form-control' ) );
        echo $this->Form->input('password' , array( 'class' => 'form-control' ) );
        echo $this->Form->input('email' , array( 'class' => 'form-control' ) );
        echo $this->Form->input('role', array(
            'options' => array('admin' => 'Admin', 'user' => 'User' )
        , 'class' => 'form-control' ) );
    ?>
    </fieldset>
        <div class="submit">
            <?php echo $this->Form->submit(__('Add User'), array('name' => 'ok', 'div' => FALSE ,'class' => 'btn btn-primary' )); ?>
            <?php echo $this->Html->link( 'Cancel', '/users/users', array('class' => 'btn btn-danger' ) ); ?>
        </div>
        
     <? $this->Form->end(); ?>

</div>
</div>

<div class="col-md-4">
<div class="helpbox big" >
<p>
The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).
</p>
</div>
</div>
