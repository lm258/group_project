<script type="text/javascript">
	
	var oTable;
	var selected_jobs =  <? echo $selected_jobs; ?>;

    $(document).ready(function() {
		
        oTable = $('#adminJobList').dataTable({
            //"bProcessing": true,
            "bServerSide": true,
            "sAjaxSource": "<?php echo $this->Html->Url(array('controller' => 'Jobs', 'action' => 'ajaxJobData' )); ?>",
            "fnRowCallback": function( nRow, aData, iDisplayIndex ) {

				for( var i = 0 ; i < selected_jobs.length ; i ++ ){
					if ( selected_jobs[i] == aData[0] ){
						$(nRow).addClass("row_selected");
					}
				}
			
				return nRow;
			},		
			"aoColumns": [
				{ "bVisible":    false },
				null,{ "bVisible":    false },null,null,null,null,null ]
,
			"aoColumnDefs": [
			  { "sWidth": "200px", "aTargets": [ 1 ] }
			]
        });
        
        
		/* Click event handler */
		$('#adminJobList tbody').on( 'click' , 'tr',function () {

			var data = oTable.fnGetData( this );
			
			//$(this).toggleClass('row_selected');
			if ( $(this).hasClass('row_selected') ){

				selected_jobs = jQuery.grep( selected_jobs , function(value) {
				  return value != data[0];
				});

			}else{
				selected_jobs.push( data[0] );
			}

			$(this).toggleClass('row_selected');
			
			$("#input").val( selected_jobs );

		} );
	
    });

</script>

<table id="adminJobList" class="table" >
    <thead>
        <tr>
			<th>Id</th>
            <th>Name</th>
            <th>Description</th>
            <th>Rating</th>
            <th>#Total</th>
            <th>#Completed</th>
            <th>#Rejected</th>          
            <th>#Failed</th>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td colspan="8" class="dataTables_empty">Loading data from server...</td>
        </tr>
    </tbody>
</table>

<?php echo $this->Form->create('User', array ( 'action' => 'addJobs' ) ); ?>
<input type='hidden' name="jobs" id='input' value='<? echo $selected_jobs; ?>' />
<input type='hidden' name='user_id' value='<? echo $user_id; ?>' />

<div class='submit' >
	<? echo $this->Form->submit(__('Save') , array('name' => 'ok', 'div' => FALSE , 'class' => 'btn btn-primary' ) ); ?>
	<a href='/users/jobs/<? echo $user_id; ?>' class="btn btn-danger" >Cancel</a>
</div>

<?php echo $this->Form->end(); ?>
