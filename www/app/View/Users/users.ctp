<script type="text/javascript">
    $(document).ready(function() {

        var oTable = $('#adminJobList').dataTable({
            "bProcessing": false,
            "bServerSide": true,
            "sAjaxSource": "<?php echo $this->Html->Url(array('controller' => 'Users', 'action' => 'ajaxUsersData' )); ?>",
            "fnRowCallback": function( nRow, aaData, iDisplayIndex ) {
            if ( aaData[3] == "admin" )
            {
				$(nRow).attr( 'class' , 'danger' );  
			}},
        "aaSorting": [[ 3, "asc" ]],
			"aoColumns": [ 
				{ "bVisible":    false },
				null,
				null,
				null,
				{
					"mRender": function ( data, type, row ) {
						return "<a href='/users/jobs/" + row[0] + "' >View Jobs ("+row[4]+") </a>";
					}
				},			  
				{
					"mRender": function ( data, type, row ) {
						return "<a class='btn btn-default btn-xs' href='/users/edit/" + row[0] + "' >Edit</a>";
					}
				},
				   {
						"mRender": function ( data, type, row ) {
						return "<a class='btn btn-danger btn-xs' href='/users/delete/" + row[0] + "'  onclick='if (confirm(\"Are you sure you wish to this user?\")) { return true; } return false;' >Delete</a>";
					}
				}
			]
        });
    });
</script>

<div class="alert alert-warning alert-dismissable">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
	<strong>All Users</strong>
	<br/>
	Here you can view all users of the system , additional  users can be created or redundant ones deleted.
	Jobs can be assigned to a user by clicking the link in the 'jobs' column below.
</div>

<table id="adminJobList" class="table"  cellspacing="0" cellpadding="0" >
    <thead>
        <tr>
			<th>Id</th>
            <th>Name</th>
            <th>Email</th>
            <th>Role</th>
            <th>Jobs</th>
            <th>Edit</th>
            <th>Delete</t>
        </tr>
    </thead>
    <tbody>
        <tr>
            <td colspan="8" class="dataTables_empty">Loading data from server...</td>
        </tr>
    </tbody>
</table>


<br/>
<a class="btn btn-primary" href='/users/add' >New User</a>
<br/>
