<br/>
<div class="col-md-8" >
<div class="col-md-8" >
<?php echo $this->Form->create('User', array('action' => 'account')); ?>
    <fieldset>
        <legend><?php echo ('Update Account'); ?></legend>
        <?php echo $this->Form->input('username' , array( 'class' => 'form-control' ));
        echo $this->Form->input('email' , array( 'class' => 'form-control' ) );
        echo $this->Form->input('password' , array(  'class' => 'form-control' , 'value' => '' ) );
    ?>
    </fieldset>
        <div class="submit">
            <?php echo $this->Form->submit(__('Save'), array( 'class' => 'btn btn-primary' ,'div' => false ) ); ?>
            <? echo $this->Html->link('Delete Account', array('action' => 'delete'), array( 'class' => 'btn btn-danger' ), "Are you sure you wish to delete your account?"); ?>
        </div>
        
     <? $this->Form->end(); ?>
</div>
</div>

<div class="col-md-4">
<div class="alert alert-warning">
<p>
The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions have evolved over the years, sometimes by accident, sometimes on purpose (injected humour and the like).
</p>
</div>
</div>
