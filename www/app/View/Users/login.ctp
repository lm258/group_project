<!-- app/View/Users/login.ctp -->

<div class="users form">
<?php echo $this->Session->flash('auth'); ?>
<?php echo $this->Form->create('User'); ?>
    <fieldset>
        <legend>Login</legend>
        <?php
        echo $this->Form->input('username' , array( 'class' => 'form-control' ) );
        echo $this->Form->input('password' , array( 'class' => 'form-control' ) );
    	?>
    </fieldset>
    <?php echo $this->Form->submit(__('Login'), array( 'class' => 'btn btn-primary' ) ); ?>

<?php echo $this->Form->end(); ?>

</div>
