<?php

class GraphController extends AppController{
	
	public function jobs (){
		
		$this->changeLayout();
		//import our job model
		App::import('model','Job');
		
		//find all jobs assigned to the user id
		$user_id = $this->Session->read("User.id");
		$job = new Job();
		$jobs = $job->User->findAllById ( $user_id );
		
		//get the jobs from the array
		$jobs = $jobs[0]["Job"];
		
		//set the jobs for our view
		$this->set( "jobs" , $jobs );
	}
	
	public function graph ( $job_id ){
		
		$this->layout = 'graph';
		
		//import our job parameter model
		App::import('model','JobParameter');
		App::import('model','ObjectiveNames');
		
		//find all of our job ids
		$jobParameter = new JobParameter();
		$objectiveName = new ObjectiveNames();
		
		//this is used to order the parameters correctly ie $x1,$sx2 .. .etc
		$jobParameters = $jobParameter->findAllByJobId( $job_id  , array() , 'CAST( REPLACE( name ,\'$x\',\'\') as unsigned);' );
		$objectiveNames = $objectiveName->findAllByJobId ( $job_id );
		
		$parameters = array();
		$objectives = array();
		
		//go through and get all of our parameters
		foreach ( $jobParameters as $jobParameter ){
			$parameters[] = $jobParameter["JobParameter"];
		}
		
		foreach ( $objectiveNames as $objectiveName ){
			$objectives[] = $objectiveName["ObjectiveNames"];
		}
		
		//set our parameters
		$this->set("parameters" , $parameters );
		$this->set("objectives" , $objectives );
		$this->set( "job_id" , $job_id );
		$this->set( "user_id" , $this->Session->read ( "User.id" ) );
		$this->set ( "auth_token" , $this->Session->read ("User.authToken") );
	}
	
	public function test(){
		$this->layout = 'graph';
	}

}

?>
