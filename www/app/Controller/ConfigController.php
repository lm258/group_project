<?php

class ConfigController extends AppController {

    public function index() {

        $this->changeLayout();

        $configs = $this->Config->find( 'all' );

        $this->set ( "configs" , $configs );
    }

    public function save() {

        $this->Config->saveMany( $this->data['Config'] );

        AppController::setFlashSuccess(__('Configuration was saved!'));

        return $this->redirect(array('controller' => 'config', 'action'=>'index'));

    }

    public function restore(){

        $configs = $this->Config->find( 'all' );

        foreach( $configs as &$config ){
            $config['Config']['value'] = $config['Config']['default_value'];
        }

        $this->Config->saveMany($configs);


        AppController::setFlashSuccess(__('Configuration was restored!'));

        return $this->redirect(array('controller' => 'config', 'action'=>'index'));
    }


}

?>