<?php
class JobsController extends AppController {
	
    function index() { }
     
    function ajaxJobData() {
        $this->modelClass = "JobsAjax";
        $this->autoRender = false;          
        $output = $this->JobsAjax->AllJobs();
         
        echo json_encode($output);
    }
    
    function ajaxJobUsersData( $id ) {
		
        $this->modelClass = "JobsAjax";
        $this->autoRender = false;         
        $output = $this->JobsAjax->JobUsers( $id );

        echo json_encode($output);
    }
    
    function delete( $id ){
       if($this->Job->delete( $id )){
			AppController::setFlashSuccess('Job Deleted');
			$this->redirect(array('controller' => 'jobs', 'action'=>'jobs'));
	   }else{
			AppController::setFlashFailure('Job Not Deleted');
			$this->redirect(array('controller' => 'jobs', 'action'=>'jobs'));
	   }
	}
	
	function users( $id ){
		$this->changeLayout();
		$this->Job->id = $id;
		$data = $this->Job->read();
		
		//set our job id
		$this->set( 'id' , $id );
		$this->set( 'name' , $data['Job']['name'] );
		
		//set our location for redirect
		$this->Session->write( "User.Location" ,  $this->here );
		
		if(empty($this->data)){
			$this->data = $this->Job->read();
		} else {
		}
	}
	
	
	
	function unassign( $job_id , $user_id ){
		
		App::import('model','UserJob');
		$UserJob = new UserJob();
		$UserJob->user_id = $user_id;
		$UserJob->job_id = $job_id;


		if ( !$UserJob->deleteAll( array( "UserJob.user_id" => $user_id , "UserJob.job_id" => $job_id ) ) )
		{
			AppController::setFlashFailure('Could not remove user from job.');
		}else{
			AppController::setFlashSuccess('User removed from job.');
		}
		
		if ( $this->Session->check( "User.Location" ) )
			return $this->redirect($this->Session->read( "User.Location" ));
		
		return $this->redirect(array('controller' => 'jobs', 'action'=>'jobs'));
	}
	
	public function addUsers ( $job_id = null ){
		
		$this->changeLayout();
		$this->set( "selected_users" , "[]" );
		
		if ( $job_id != null ){
			$this->set ( "job_id" , $job_id );
			
			//this is used to select rows when assigning them to the job
			App::import('model','UserJob');
			$UserJob = new UserJob();
			$users = $UserJob->findAllByJobId( $job_id );
			$user_ids = array();
			
			//get our users
			foreach ( $users as $user ){
				$user_ids[] = $user["UserJob"]["user_id"];
			}

			//set our selected variable ... wooohooo
			$this->set( "selected_users" , "[" . implode("," , $user_ids ) . "]" );
		}
		
		if( !empty($this->data) ){
			
			App::import('model','UserJob');
			
			$user_ids = explode ( ',' , $this->data["users"] );
			$job_id = $this->data["job_id"];
			
			$UserJob = new UserJob();
			
			//find all existing user jobs
			$userJobs = $UserJob->find( 'all' ,  array ( 'conditions' => array('UserJob.job_id' => $job_id ) ) );
		
			//delete our user jobs
			foreach ( $userJobs as $job ){

				//delete it
				$UserJob->delete ( $job['UserJob'] );
			}
		
			
			foreach ( $user_ids as $user_id ){
				$UserJob = new UserJob();
				
				try{
					$UserJob->save( array( 'user_id' => $user_id , 'job_id' => $job_id ) );
				}catch ( Exception $e ){
					continue;
				}
			}

			AppController::setFlashSuccess('User(s) assigned to Job.');
							
			if ( $this->Session->check( "User.Location" ) )
				return $this->redirect($this->Session->read( "User.Location" ));
			
			return $this->redirect( array ( 'jobs' => 'jobs' ) );
		}
	}
	
	public function jobs(){
		$this->changeLayout();
		$this->Session->write( "User.Location" , $this->here );
	}
    
    function edit( $id ){
		$this->changeLayout();
		$this->Job->id = $id;
		
		if(empty($this->data)){
			$this->data = $this->Job->read();
		} else {
			if($this->Job->save($this->data)){
				AppController::setFlashSuccess ('Job has been updated.');

				if ( $this->Session->check( "User.Location" ) )
					return $this->redirect($this->Session->read( "User.Location" ));
				
				$this->redirect(array('controller' => 'jobs', 'action'=>'jobs'));
			}else{
				AppController::setFlashFailure('Job has not been updated');
			}
		}
	}
}
?>
