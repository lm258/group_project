<?php
/**
 * Application level Controller
 *
 * This file is application-wide controller file. You can put all
 * application-wide controller-related methods here.
 *
 * PHP 5
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */
App::uses('Controller', 'Controller');

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @package		app.Controller
 * @link		http://book.cakephp.org/2.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller {
	public $components = array(
        'Session',
        'Auth' => array(
            'logoutRedirect' => array('controller' => 'users', 'action' => 'login'),
			'authorize' => array('Controller')
        )
    );
   

	public function isAuthorized($user){

		if( $this->action === 'logout' || $this->action === 'account' || ( $this->name === 'Graph' && ($this->action === 'jobs' || $this->action === 'graph') ) ){
			return true;
		}
		
		if ( isset($user['role']) && $user['role'] === 'admin') {
			return true;
		}
		
		return false;
	}
	
	public function isAdmin() {
		$role = $this->Auth->user('role');
		
		if ($role === 'admin') {
			return true;
		}

		return false;
	}
	
	public function isUser(){
		$role = $this->Auth->user('role');
		
		if($role === 'user'){
			return true;
		}
		
		return false;
	}
	
		
	public function changeLayout(){
		
		if($this->isAdmin()){
			$this->layout = 'admin';
		}else{
			if($this->isUser()){
				$this->layout = 'user';	
			}else{
				$this->layout = 'default';
			}
		}
	}
    
    public function beforeFilter() {

		parent::beforeFilter();
		
		if($this->Auth->user('role') == 'admin'){
			 $this->Auth->loginRedirect = array('controller' => 'jobs', 'action' => 'jobs');
		}else{
			 $this->Auth->loginRedirect = array('controller' => 'graph', 'action' => 'jobs');
		}

		if($this->user){
			$this->changeLayout();
		}else{
			$this->layout = 'default';
		}
		
		$this->Auth->allow('login'); // Letting users view the login page
	}
	
	public function setFlashSuccess ( $message ){
		$this->Session->setFlash ( "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button> <strong>Success</strong><br/>" . $message , 'default' , array('class' => 'alert alert-success alert-dismissable') );
	}
	
	public function setFlashFailure ( $message ){
		$this->Session->setFlash ( "<button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button> <strong>Failure</strong><br/>" .$message , 'default' , array('class' => 'alert alert-danger alert-dismissable') );
	}
}
