<?php

// app/Controller/UsersController.php

class UsersController extends AppController{
	
	public function login() {
		if ($this->request->is('post')) {
			if ($this->Auth->login()) {
				
				//save our user role
				$this->Session->write('User.role', $this->Auth->user('role') );
				$this->Session->write('User.username', $this->Auth->user('username') );
				$this->User->id = $this->Auth->user('id');
				$this->Session->write('User.id', $this->Auth->user('id') );
				
				$token = md5( rand(0,100000)+"rand" );
				$this->Session->write('User.authToken', $token );
				$this->User->saveField('security_token' , $token ); 
				
				if($this->isAdmin()){
					return $this->redirect(array('controller' => 'users', 'action'=>'users'));
				}else{
					return $this->redirect(array('controller' => 'graph' , 'action' => 'jobs'));
				}
			}
			AppController::setFlashFailure(__('Invalid username or password, try again'));
		}
	}
	
    function ajaxUsersData() {
        $this->modelClass = "UsersAjax";
        $this->autoRender = false;          
        $output = $this->UsersAjax->AllUsers();
         
        echo json_encode($output);
    }
    
    function ajaxUserJobsData( $user_id ) {
        $this->modelClass = "UsersAjax";
        $this->autoRender = false;          
        $output = $this->UsersAjax->UserJobs($user_id);
         
        echo json_encode($output);
    }

	public function logout() {
		return $this->redirect($this->Auth->logout());
	}
    
    public function index() {
        $this->User->recursive = 0;
        $this->set('users', $this->paginate());
    }

    public function view($id = null) {
				
        $this->User->id = $id;
        if (!$this->User->exists()) {
            throw new NotFoundException(__('Invalid user'));
        }
        $this->set('user', $this->User->read(null, $id));
    }
    
    public function addJobs ( $user_id = null ){
		
		$this->changeLayout();
		$this->set( "selected_jobs" , "[]" );
		
		if ( $user_id != null ){
			$this->set ( "user_id" , $user_id );
			
			//this is used to select rows when assigning them to the job
			App::import('model','UserJob');
			$UserJob = new UserJob();
			$jobs = $UserJob->findAllByUserId( $user_id );
			$job_ids = array();
			
			//get our users
			foreach ( $jobs as $job ){
				$job_ids[] = $job["UserJob"]["job_id"];
			}

			//set our selected variable ... wooohooo
			$this->set( "selected_jobs" , "[" . implode("," , $job_ids ) . "]" );
		}
		
		if( !empty($this->data) ){
			
			App::import('model','UserJob');
			
			$job_ids = explode ( ',' , $this->data["jobs"] );
			$user_id = $this->data["user_id"];
			
			$UserJob = new UserJob();
			
			//find all existing user jobs
			$userJobs = $UserJob->find( 'all' ,  array ( 'conditions' => array('UserJob.user_id' => $user_id ) ) );
		
			//delete our user jobs
			foreach ( $userJobs as $job ){

				//delete it
				$UserJob->delete ( $job['UserJob'] );
			}
		
			
			foreach ( $job_ids as $job_id ){
				$UserJob = new UserJob();
				
				try{
					$UserJob->save( array( 'user_id' => $user_id , 'job_id' => $job_id ) );
				}catch ( Exception $e ){
					continue;
				}
			}

			AppController::setFlashSuccess('User(s) assigned to Job.');
							
			if ( $this->Session->check( "User.Location" ) )
				return $this->redirect($this->Session->read( "User.Location" ));
			
			return $this->redirect( array ( 'user' => 'users' ) );
	}
	}

	public function users(){
		$this->changeLayout();
		$this->Session->write( "User.Location" , $this->here );
	}
	
	public function jobs( $user_id ){
		$this->changeLayout();
		
		$this->User->id = $user_id;
		$data = $this->User->read();
		$this->set( 'id' , $user_id );
		$this->set( 'username' , $data['User']['username'] );
		
		//set our location for redirect
		$this->Session->write( "User.Location" ,  $this->here );
		
	}
	
	public function account(){
		$this->changeLayout();
		$this->User->id = $this->Auth->user('id');
		
		if(empty($this->data)){
			$this->data = $this->User->read();
		} else {
			if($this->User->save($this->data)){
				AppController::setFlashSuccess('Your account has been updated');
				$this->redirect(array('controller' => 'users', 'action'=>'account'));
			}else{
				AppController::setFlashFailure('Your account has not been updated');
			}
		}
	}
	
    public function add() {
		$this->changeLayout();
        if ($this->request->is('post')) {
            $this->User->create();
            
            if ($this->User->save($this->request->data)) {
				
                AppController::setFlashSuccess(__('The user has been saved'));
                
                if ( $this->Session->check( "User.Location" ) )
					return $this->redirect($this->Session->read( "User.Location" ));
                
                return $this->redirect(array('users' => 'users'));
            }
            AppController::setFlashFailure(__('The user could not be saved. Please, try again.'));
        }
    }

    public function edit( $id ) {
		$this->changeLayout();
		$this->User->id = $id;
		
		if(empty($this->data)){
			$this->data = $this->User->read();
		} else {
			if($this->User->save($this->data)){
				AppController::setFlashSuccess('User has been updated.');
				
				if ( $this->Session->check( "User.Location" ) )
					return $this->redirect($this->Session->read( "User.Location" ));
				
				return $this->redirect($this->referer());
			}else{
				AppController::setFlashFailure('Your account has not been updated');
			}
		}
    }
    
    public function delete( $id = null ) {
	
	if ( $id === null )
		$id = $this->Auth->user('id');
		
       if($this->User->delete( $id )){
			AppController::setFlashSuccess('Account Deleted');
			
			//return to our page of origin
			if ( $this->Session->check( "User.Location" ) && $id != $this->Auth->user('id') )
				return $this->redirect($this->Session->read( "User.Location" ));
					
			$this->redirect(array('users' => 'login'));
	   }else{
		   
		    //return to our page of origin
			if ( $this->Session->check( "User.Location" ) && $id != $this->Auth->user('id') )
				return $this->redirect($this->Session->read( "User.Location" ));
				
			AppController::setFlashFailure('Account Not Deleted');
			$this->redirect(array('action' => 'account'));
		}
    }
}

?>
