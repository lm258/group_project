<?php

// app/Model/User.php

class User extends AppModel {
	
     public $hasAndBelongsToMany = array(
        'Job' => array(
            'className' => 'Job',
            'joinTable' => 'users_jobs',
            'foreignKey' => 'user_id',
            'associationForeignKey' => 'job_id'
        )
    );
    
    public $validate = array(
        'username' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'A username is required'
            ),
            'unique' => array(
				'rule' => 'isUnique',
				'message' => 'That username is already taken please try another one'
			)
        ),
        'password' => array(
            'required' => array(
                'rule' => array('notEmpty'),
                'message' => 'A password is required'
            )
        ),
        'email' => 'email',
        'role' => array(
            'valid' => array(
                'rule' => array('inList', array('admin', 'user')),
                'message' => 'Please enter a valid role',
                'allowEmpty' => false
            )
        )
    );
    
    public function beforeSave($options = array()) {
		if (isset($this->data[$this->alias]['password'])) {
			$this->data[$this->alias]['password'] = AuthComponent::password($this->data[$this->alias]['password']);
		}
		return true;
	}
	
}
?>
