<?php

class Job extends AppModel {
     public $useTable = 'job';
     
     public $hasAndBelongsToMany = array(
        'User' => array(
            'className' => 'User',
            'joinTable' => 'users_jobs',
            'foreignKey' => 'job_id',
            'associationForeignKey' => 'user_id'
        )
    );
}

?>
