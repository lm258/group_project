//This is used for defining our glab variables
window.GraphGlobals  = {
        Graphs : [],
        SyncMovement : false,
        ToolTip : true,
        Scroll : [0,0]
};

//This javascript file is used for generating and fetching poinys
// from the java program. It is only used to make things faster when zooming etc.
function Graph ( job_id , user_id , objective , paramOne , paramTwo  , auth_token  , id  , panelID ) {
    
    //this is our graph class lets set our user id and job id and also the paramters for our graph
    this.job_id = job_id;
    this.user_id = user_id;
    this.paramOne = paramOne;
    this.paramTwo = paramTwo;
    this.score = 0; // get the first score
    this.auth_token;
    this.circles = false;
    this.svg = false;
    this.data = [];
    this.width = 400;
    this.height = 400;
    this.visible_width = 512;
    this.visible_height = 512;
    this.padding = 40;
    this.x_t;
    this.y_t;
    this.max_scale = 24;
    this.loading = false;
    this.objective = objective;
    this.objective_max = 1 ;
    this.objective_min = 0;
    this.objective_diff = 1;
    this.mouse_x = 0;
    this.mouse_y = 0;
    this.attached = false;
    this.id = id;
    this.last_layer = 1;
    this.last_scale = 0;
    this.last_center = [ 0 , 0 ];
    this.isFiltered = false;
    this.panelID = panelID;
    this.max_score;
	this.min_score ;

    //set our init to false because we havent inited a graph yet
    this.isInit = false;
        
    //first we will call the ajax function to actually initate our graph
    //this will make the java program load our points
    this.init = function init(){
        
        //small trick to pass this to the function
        var that = this;
        
        // initialisation of the graph's size
        if (window.innerWidth && window.innerHeight) {
            this.visible_width = 0.9*(window.innerWidth)*512/1366;
            this.visible_height = this.visible_width ;
        }
        
        //add our graph to the graphs
        window.GraphGlobals.Graphs.push ( this );
        
        //append our loading thing
        this.setLoading( true );
        $('#panel-'+ this.panelID + '> .panel-body > #scatter-plot' + this.id).width( this.visible_width );
        $('#panel-'+ this.panelID + '> .panel-body > #scatter-plot' + this.id).height( this.visible_height );
        
        //call the ajax function to get the points
        $.ajax({
          type: "POST",
          url: "/ajax/initGraph",
          async : true,
          data: { "user_id" : this.user_id , "job_id" : this.job_id , "auth_token" : this.auth_token , "paramTwo" : this.paramTwo  , "paramOne" : this.paramOne },
          success : function( json ) { 
            
            //Make sure that we change our inital size for the loading icon or else it will screw up the dize of our graphs
            that.setLoading( false );
            $('#panel-'+ that.panelID + '> .panel-body > #scatter-plot' + that.id).css("width","auto;");
            $('#panel-'+ that.panelID + '> .panel-body > #scatter-plot' + that.id).css("height","auto;");

            //if we managed to load our graph then set init to true
            that.isInit = true;

            //parse our json
            json = JSON.parse(json);

            //now lets get our first points
            that.getPointsStart ( json.dimensions.x , json.dimensions.y , json.dimensions.width , json.dimensions.height );

          },
          error : function(json) { console.log( "ERROR: Could not initiate the graph." ); }
        });
    }
    
    //this function is used to do the first run of our graph
    //just for populating it and calculating our min max values etc
    this.start = function start ( data ){

        var that = this;

        $('#panel-'+ this.panelID + '> .panel-body > #scatter-plot' + this.id).css('background-image', '' );

        //get our min max values etc
        this.max_vx = d3.max( data[0].map(function (d) {return d[0]; }));
        this.max_vy = d3.max( data[0].map(function (d) {return d[1]; }));
        this.max_vr = d3.max( data[0].map(function (d) {return d[2]; }));

        this.min_vx = d3.min( data[0].map(function (d) {return d[0]; }));
        this.min_vy = d3.min( data[0].map(function (d) {return d[1]; }));
        this.min_vr = d3.min( data[0].map(function (d) {return d[2]; }));
        
        this.max_score = d3.max( data[0].map(function (d) {return d[4]; }));
		this.min_score = d3.min( data[0].map(function (d) {return d[4]; }));

        //map our objective min max values to our thing
        this.objective_min = data[1][0];
        this.objective_max = data[1][1];
        this.objective_diff = this.objective_max - this.objective_min;
        
        //set our x _ y
        this.x_t = this.width/(this.min_vx - this.max_vx);
        this.y_t = this.height/(this.min_vy - this.max_vy);
        
        //get our radious function
        this.r = d3.scale.linear().domain([ that.min_vr, that.max_vr ]).range([ 5 , 5 ]);

        //set our data
        this.data = data[0];
        
        //create our x and y axis as apprioptiate
        this.x = d3.scale.linear().domain([that.min_vx, that.max_vx]).range([0, this.width]);
        this.y = d3.scale.linear().domain([that.min_vy, that.max_vy]).range([this.height, 0]);
        this.xAxis = d3.svg.axis().scale(this.x).orient("bottom").tickSize(-this.height);
        this.yAxis = d3.svg.axis().scale(this.y).orient("left").tickSize(-this.width);

        //create our zoom handler
        this.zoomFunc = d3.behavior.zoom().x(this.x)
            .y(this.y)
            .scaleExtent([1, 40])
            .size( [ this.width , this.height ] )
            .on( "zoom" , function (){
                
                if ( window.GraphGlobals.SyncMovement === true ){
                    for ( var i = 0 ; i < window.GraphGlobals.Graphs.length ; i ++ ){

                        if ( window.GraphGlobals.Graphs[i].panelID == that.panelID ){
                            window.GraphGlobals.Graphs[i].zoomFunc.translate( that.zoomFunc.translate() );
                            window.GraphGlobals.Graphs[i].zoomFunc.scale( that.zoomFunc.scale() );
                            window.GraphGlobals.Graphs[i].zoom( window.GraphGlobals.Graphs[i] );
                        }
                    }
                }else{
                    that.zoom(that);
                }
            } );
        
        //create our tooltip
        this.tip = d3.tip()
          .attr('class', 'd3-tip')
          .offset([-10, 0])
          .html(function(d) {
            return "<strong>Parameter One:</strong> <span style='color:red'>" + d[0] + "</span><br/><strong>Parameter Two:</strong> <span style='color:red'>" + d[1] + "</span>" +
                "</span><br/><strong>Mass:</strong> <span style='color:red'>" + d[2] + "</span>" + "</span><br/><strong>Score:</strong> <span style='color:red'>" + d[4] + "</span>";
          });
    
        this.svg = d3.select( '#panel-'+ this.panelID + '> .panel-body > #scatter-plot' + this.id).append("svg")
            .attr("id","svg" + this.id )
            .attr("width", this.visible_width )
            .attr("height", this.visible_height )
            .attr("preserveAspectRatio", "xMidYMid")
            .attr("viewBox", "0 -20 " + (this.width + this.padding ) + " " + (this.height + this.padding + 20 ) )
            .append("svg:g")
            .attr("transform", "translate(" + this.padding + "," + 0 + ")")
            .call(this.zoomFunc);
            
        this.svg.call(this.tip);
        
        //append the rectangle
        this.svg.append("rect")
            .attr("width", this.width)
            .attr("height", this.height);
        
        //create our circles
        this.circles = this.svg.append('svg').attr('width',this.width).attr('height',this.height).append('g').attr('id','circles1');

        //append our axis group
        this.svg.append("g")
            .attr("class", "x axis")
            .attr("transform", "translate(0," + this.height + ")")
            .call(this.xAxis);

        //append our rectangle
        this.svg.append("g")
            .attr("class", "y axis")
            .call(this.yAxis);
            
		 
		var labelx = this.svg.append("text")      // text label for the x axis
			.attr("x", this.height / 2 )
			.attr("y", 30 +(this.height) ) // true -y
			.attr("id", "badgex")
			.attr("class", "badge")
			.style("text-anchor", "middle")
			.text("P1 : "+this.paramOne); 

		this.svg.append("rect")
			.attr("x", labelx.node().getBBox().x)
			.attr("y", labelx.node().getBBox().y)
			.attr("rx", 5)
			.attr("ry", 5)
			.attr("class", "cadre")
			.attr("width", labelx.node().getBBox().width)
			.attr("height", labelx.node().getBBox().height)
			.style("fill", "#ccc")
			.style("fill-opacity", ".3")
			.style("stroke", "#666")    
			.style("stroke-width", "1px");   
			            
        var labely = this.svg.append("text")
            .attr("transform", "rotate(-90)")
            .attr("x", 0 -(this.height / 2)) //true -y
            .attr("y",-50  ) // true x
            .attr("class", "badge")
            .attr("dy", "2em")
            .style("text-anchor", "middle")
            .text("P2 : "+this.paramTwo);

        this.svg.append("rect")
            .attr("transform", "rotate(-90)")
            .attr("x", labely.node().getBBox().x)
            .attr("y", labely.node().getBBox().y)
            .attr("rx", 5)
            .attr("ry", 5)
            .attr("class", "cadre")
            .attr("width", labely.node().getBBox().width)
            .attr("height", labely.node().getBBox().height)
            .style("fill", "#ccc")
            .style("fill-opacity", ".3")
            .style("stroke", "#666")    
            .style("stroke-width", "1px");

        var zoomLabel = this.svg.append("text")      // text label for the x axis
            .attr("x", this.height - 4 )
            .attr("y", 30 +(this.height) ) // true -y
            .attr("id", "zoomLabel")
            .style("text-anchor", "end")
            .style("font-size" , "11px" )
            .text("Zoom: " + this.zoomFunc.scale().toFixed(2) ); 
     
		// legends
		this.svg.append("rect")
          .attr("x", 25)
          .attr("y", -13 )
          .attr("class", "legend")
          .attr("width", 10)
          .attr("height", 10)
          .style("fill", this.pointColour(this.min_score));
     
        this.svg.append("text")
          .attr("x", 38 )
          .attr("y",  -5 )
          .attr("class", "legend")
          .attr("height",30)
          .attr("width",100)
          .style("fill", this.pointColour(this.min_score))
          .text(this.min_score.toFixed(3));
          
        this.svg.append("rect")
          .attr("x", 120)
          .attr("y", -13)
          .attr("class", "legend")
          .attr("width", 10)
          .attr("height", 10)
          .style("float", "left")
          .style("fill", this.pointColour(this.max_score));
     
        this.svg.append("text")
          .attr("x", 133)
          .attr("y",  -5 )
          .attr("class", "legend")
          .attr("height",30)
          .attr("width",100)
          .style("fill", this.pointColour(this.max_score))
          .text(this.max_score.toFixed(3));
		
        this.draw();
    };
    
    //this function is used to get points from the java program
    this.getPointsStart = function getPointsStart ( x , y , width , height ){
        
        //this is used to call functions from thin our ajax call
        var that = this;

        //this part of the object will now make a silent ajax request to get the points from the java program and then load
        // into our storage structure
        $.ajax( { type: "POST" , async : false , url : "/ajax/getPoints" , data: { "user_id" : this.user_id , "objective" : this.objective , "job_id" : this.job_id , "paramTwo" : this.paramTwo  , "paramOne" : this.paramOne , "auth_token" : this.auth_token , "x" : x , "y" : y , "width" : width , "height" : height } 
        , success: function(json) { 
            
            //once we are at this point we can add our points into the graph
            that.start(JSON.parse(json));

        } } );
    }
    
    //this is our regular function for getting the points and then redrawing lol
    this.getPoints = function getPoints( x , y , width , height ){
        
        if ( this.loading == true )
            return;

        this.setLoading( true );
        var that = this;
        var translate = this.zoomFunc.translate();
        var scale = this.zoomFunc.scale();

        //this part of the object will now make a silent ajax request to get the points from the java program and then load
        // into our storage structure
        $.ajax( { type: "POST" , url : "/ajax/getPoints" , data: { "user_id" : this.user_id , "paramTwo" : this.paramTwo  , "paramOne" : this.paramOne , "job_id" : this.job_id , "objective" : this.objective , "auth_token" : this.auth_token , "x" : x , "y" : y , "width" : width , "height" : height } 
        , success: function(json) { 
            
            //once we are at this point we can add our points into the graph        
            that.data = JSON.parse( json );
            
            //set our loading to false so we can get the next load of points
            that.setLoading( false );

            //seto ur data correctly
            that.data = that.data[0];

            //reset our zoom and our translate
            that.zoomFunc.translate( [0,0] );
            that.zoomFunc.scale( 1 );
            
            //redraw our graph after we get the points
            that.draw();

            //reset our scale and translate
            that.zoomFunc.translate( translate );
            that.zoomFunc.scale( scale );

            //call our zoom function
            that.zoom(that);

        } ,
        error : function( json ) {
            that.loading = false;
            console.log("ERROR");
        } } );
    }
    
    
    //this is our function for drawing the points
    this.draw = function draw() {
        
        var that = this;
        
        this.circles.selectAll("circle").remove()
        this.circles.selectAll("circle").data(that.data)
        .enter()
        .append("circle")
        .attr("cx", function (d) { return that.x(d[0]); })
        .attr("cy", function (d) { return that.y(d[1]); })
        .attr("r", function (d) { return that.r(d[2]) })
        .style("fill", function(d){ return that.pointColour(d[3]);  })
        .on("mouseover", function(){ if ( window.GraphGlobals.ToolTip === true ){ that.tip.show( d3.select(this).data()[0]); } })//d3.select(this).style("fill", function(d){ that.tip.show(d); return d3.rgb(d[3]).darker(); });})
        .on("mouseout", function(){ if ( window.GraphGlobals.ToolTip === true ){ that.tip.hide(d3.select(this).data()[0]); } });//d3.select(this).style("fill", function(d){ that.tip.hide(d); return d[3]; });});
    
    }
    
    /* This is our function for changing the objective */
    this.setObjective = function setObjective ( obj ){
        this.objective = obj;
        
        try {
            this.getPoints( this.min_vx , this.min_vy , this.max_vx - this.min_vx , this.max_vy - this.min_vy );
        }catch ( e ){
            console.log("ERROR: Could not change objective.");
        }
    }
    
    /* This function is used for attaching ourselves to another graph wi */
    this.attach = function attach ( graph ){
        
        if ( this.attached != false || this.loading == true )
            return false;
        
        $(this.svg.node()).parent().remove();
        this.attached = graph;
        this.svg = graph.svg;
        this.tip = graph.tip;
        graph.hasAttached = true;
        
        //set our circles
        this.circles = this.svg.append('svg').attr('width',this.width).attr('height',this.height).append('g').attr('id','circles2');
        $('#panel-'+ this.panelID + '> .panel-body > #scatter-plot' + this.id).css("width","0px");
        $('#panel-'+ this.panelID + '> .panel-body > #scatter-plot' + this.id).css("height","0px");

        //get our translate and scale
        var scale = graph.zoomFunc.scale();
        var translate = graph.zoomFunc.translate();
        
        //we need syncronized movement here for this to work
        graph.zoomFunc.translate( [0,0] );
        graph.zoomFunc.scale( 1 );
        this.zoomFunc.translate( [0,0] );
        this.zoomFunc.scale( 1 );
        window.GraphGlobals.SyncMovement = true;
        
        this.zoom( this );
        graph.zoom( graph );
        this.draw();
        
        //lets set it to our original zoom factor .. yes ?
        graph.zoomFunc.scale(scale);
        graph.zoomFunc.translate(translate);
        this.zoomFunc.scale(scale);
        this.zoomFunc.translate(translate);
        
        this.zoom(this);
        graph.zoom(graph);
        
        this.svg.selectAll(".badge").remove();
        this.svg.selectAll(".cadre").remove();
        this.svg.selectAll(".legend").remove();

        
        var labelx = this.svg.append("text")      // text label for the x axis
			.attr("x", this.height / 2 +30)
			.attr("y", 30 +(this.height) ) // true -y
			.attr("class", "badge")
			.style("text-anchor", "middle")
			.style("fill",d3.rgb(100, 172, 0))
			.text("P1 : "+graph.paramOne); 
		this.svg.append("rect")
			.attr("x", labelx.node().getBBox().x)
			.attr("y", labelx.node().getBBox().y)
			.attr("rx", 5)
			.attr("ry", 5)
			.attr("class", "cadre")			
			.attr("width", labelx.node().getBBox().width)
			.attr("height", labelx.node().getBBox().height)
			.style("fill", "#ccc")
			.style("fill-opacity", ".3")
			.style("stroke", "#666")    
			.style("stroke-width", "1px");   	
		labelx = this.svg.append("text")      // text label for the x axis
			.attr("x", this.height / 2 -30)
			.attr("y", 30 +(this.height) ) // true -y
			.attr("class", "badge")
			.style("text-anchor", "middle")
			.style("fill",d3.rgb(131, 0, 100))
			.text("P1 : "+this.paramOne); 	
		this.svg.append("rect")
			.attr("x", labelx.node().getBBox().x)
			.attr("y", labelx.node().getBBox().y)
			.attr("rx", 5)
			.attr("ry", 5)
			.attr("class", "cadre")			
			.attr("width", labelx.node().getBBox().width)
			.attr("height", labelx.node().getBBox().height)
			.style("fill", "#ccc")
			.style("fill-opacity", ".3")
			.style("stroke", "#666")    
			.style("stroke-width", "1px");  
			
        var labely = this.svg.append("text")
            .attr("transform", "rotate(-90)")
            .attr("x", 0 -(this.height / 2) +30) //true -y
            .attr("y",-50  ) // true x
            .attr("class", "badge")
            .attr("dy", "2em")
            .style("text-anchor", "middle")
            .style("fill",d3.rgb(100, 172, 0))
            .text("P2 : "+graph.paramTwo);

        this.svg.append("rect")
            .attr("transform", "rotate(-90)")
            .attr("x", labely.node().getBBox().x)
            .attr("y", labely.node().getBBox().y)
            .attr("rx", 5)
            .attr("ry", 5)
			.attr("class", "cadre")            
            .attr("width", labely.node().getBBox().width)
            .attr("height", labely.node().getBBox().height)
            .style("fill", "#ccc")
            .style("fill-opacity", ".3")
            .style("stroke", "#666")    
            .style("stroke-width", "1px");   
        
        labely = this.svg.append("text")
            .attr("transform", "rotate(-90)")
            .attr("x", 0 -(this.height / 2)-30) //true -y
            .attr("y",-50  ) // true x
            .attr("class", "badge")
            .attr("dy", "2em")
            .style("text-anchor", "middle")
            .style("fill",d3.rgb(131, 0, 100))
            .text("P2 : "+this.paramTwo);

        this.svg.append("rect")
            .attr("transform", "rotate(-90)")
            .attr("x", labely.node().getBBox().x)
            .attr("y", labely.node().getBBox().y)
            .attr("rx", 5)
            .attr("ry", 5)
			.attr("class", "cadre")            
            .attr("width", labely.node().getBBox().width)
            .attr("height", labely.node().getBBox().height)
            .style("fill", "#ccc")
            .style("fill-opacity", ".3")
            .style("stroke", "#666")    
            .style("stroke-width", "1px");
            
        // legends
		this.svg.append("rect")
          .attr("x", 25)
          .attr("y", -13 )
          .attr("class", "legend")
          .attr("width", 10)
          .attr("height", 10)
          .style("fill", this.pointColour(this.min_score));
     
        this.svg.append("text")
          .attr("x", 38 )
          .attr("y",  -5 )
          .attr("class", "legend")
          .attr("height",30)
          .attr("width",100)
          .style("fill", this.pointColour(this.min_score))
          .text(this.min_score.toFixed(2));
          
        this.svg.append("rect")
          .attr("x", 120)
          .attr("y", -13 )
          .attr("class", "legend")
          .attr("width", 10)
          .attr("height", 10)
          .style("float", "left")
          .style("fill", this.pointColour(this.max_score));
     
        this.svg.append("text")
          .attr("x", 133)
          .attr("y",  -5 )
          .attr("class", "legend")
          .attr("height",30)
          .attr("width",100)
          .style("fill", this.pointColour(this.max_score))
          .text(this.max_score.toFixed(2));  
          
          
       // legends attached
		this.svg.append("rect")
          .attr("x", 25 +200)
          .attr("y", -13 )
          .attr("class", "legend")
          .attr("width", 10)
          .attr("height", 10)
          .style("fill", graph.pointColour(graph.min_score));
     
        this.svg.append("text")
          .attr("x", 38 +200)
          .attr("y",  -5 )
          .attr("class", "legend")
          .attr("height",30)
          .attr("width",100)
          .style("fill", graph.pointColour(graph.min_score))
          .text(graph.min_score.toFixed(2));
          
        this.svg.append("rect")
          .attr("x", 120+200)
          .attr("y", -13 )
          .attr("class", "legend")
          .attr("width", 10)
          .attr("height", 10)
          .style("float", "left")
          .style("fill", graph.pointColour(graph.max_score));
     
        this.svg.append("text")
          .attr("x", 133 +200)
          .attr("y",  -5 )
          .attr("class", "legend")
          .attr("height",30)
          .attr("width",100)
          .style("fill", graph.pointColour(graph.max_score))
          .text(graph.max_score.toFixed(2));                       			
    }
    
    /* This is used to get our colour */
    this.pointColour  = function pointColour ( val ){
		
		var x = d3.scale.linear().domain([this.min_score, this.max_score]).range([0, 255]);
		if(val>255){
			val = x(val);
		}
		
        if ( this.attached != false ){
            return d3.rgb( val , 0 , 100 );
        }else{
            return d3.rgb( 100 , val , 0 );
        }
    }
    
    this.dettach = function dettach(){
        
        if ( this.attached == false ){

            this.hasAttached = false;
			
            this.svg.selectAll(".badge").remove();
            this.svg.selectAll(".cadre").remove();
            this.svg.selectAll(".legend").remove();
            
			var labelx = this.svg.append("text")      // text label for the x axis
				.attr("x", this.height / 2 )
				.attr("y", 30 +(this.height) ) // true -y
				.attr("class", "badge")
				.style("text-anchor", "middle")
				.text("P1 : "+this.paramOne); 

			this.svg.append("rect")
				.attr("x", labelx.node().getBBox().x)
				.attr("y", labelx.node().getBBox().y)
				.attr("rx", 5)
				.attr("ry", 5)
				.attr("class", "cadre")				
				.attr("width", labelx.node().getBBox().width)
				.attr("height", labelx.node().getBBox().height)
				.style("fill", "#ccc")
				.style("fill-opacity", ".3")
				.style("stroke", "#666")    
				.style("stroke-width", "1px");   
			
			var labely = this.svg.append("text")
				.attr("transform", "rotate(-90)")
				.attr("x", 0 -(this.height / 2)) //true -y
				.attr("y",-50  ) // true x
				.attr("class", "badge")
				.attr("dy", "2em")
				.style("text-anchor", "middle")
				.text("P2 : "+this.paramTwo);

			this.svg.append("rect")
				.attr("transform", "rotate(-90)")
				.attr("x", labely.node().getBBox().x)
				.attr("y", labely.node().getBBox().y)
				.attr("rx", 5)
				.attr("ry", 5)
				.attr("class", "cadre")
				.attr("width", labely.node().getBBox().width)
				.attr("height", labely.node().getBBox().height)
				.style("fill", "#ccc")
				.style("fill-opacity", ".3")
				.style("stroke", "#666")    
				.style("stroke-width", "1px");  
				
			// legends
			this.svg.append("rect")
			  .attr("x", 25)
			  .attr("y", -13 )
			  .attr("class", "legend")
			  .attr("width", 10)
			  .attr("height", 10)
			  .style("fill", this.pointColour(this.min_score));
		 
			this.svg.append("text")
			  .attr("x", 38 )
			  .attr("y",  -5 )
			  .attr("class", "legend")
			  .attr("height",30)
			  .attr("width",100)
			  .style("fill", this.pointColour(this.min_score))
			  .text(this.min_score.toFixed(2));
			  
			this.svg.append("rect")
			  .attr("x", 120)
			  .attr("y", -13 )
			  .attr("class", "legend")
			  .attr("width", 10)
			  .attr("height", 10)
			  .style("float", "left")
			  .style("fill", this.pointColour(this.max_score));
		 
			this.svg.append("text")
			  .attr("x", 133)
			  .attr("y",  -5 )
			  .attr("class", "legend")
			  .attr("height",30)
			  .attr("width",100)
			  .style("fill", this.pointColour(this.max_score))
			  .text(this.max_score.toFixed(2));  
					
			return false;
		}
			
        //create our tooltip
        this.tip = d3.tip()
          .attr('class', 'd3-tip')
          .offset([-10, 0])
          .html(function(d) {
            return "<strong>Parameter One:</strong> <span style='color:red'>" + d[0] + "</span><br/><strong>Parameter Two:</strong> <span style='color:red'>" + d[1] + "</span>" +
                "</span><br/><strong>Mass:</strong> <span style='color:red'>" + d[2] + "</span>" + "</span><br/><strong>Score:</strong> <span style='color:red'>" + d[4] + "</span>";
          });
    
        this.svg = d3.select('#panel-'+ this.panelID + '> .panel-body > #scatter-plot' + this.id).append("svg")
            .attr("id","svg")
            .attr("width", this.visible_width )
            .attr("height", this.visible_height )
            .attr("preserveAspectRatio", "xMidYMid")
            .attr("viewBox", "0 -20 " + (this.width + this.padding ) + " " + (this.height+this.padding+20) )
            .append("svg:g")
            .attr("transform", "translate(" + this.padding + "," + 0 + ")")
            .call(this.zoomFunc);
            
        this.svg.call(this.tip);
        
        //append the rectangle
        this.svg.append("rect")
            .attr("width", this.width)
            .attr("height", this.height);
        
        //create our circles
        this.circles.remove();
        this.circles = this.svg.append('svg').attr('width',this.width).attr('height',this.height).append('g').attr('id','circles1');

        $('#panel-'+ this.panelID + '> .panel-body > #scatter-plot' + this.id).width( this.visible_width );
        $('#panel-'+ this.panelID + '> .panel-body > #scatter-plot' + this.id).height( this.visible_height );

        //append our axis group
        this.svg.append("g")
            .attr("class", "x axis")
            .attr("transform", "translate(0," + this.height + ")")
            .call(this.xAxis);

        //append our rectangle
        this.svg.append("g")
            .attr("class", "y axis")
            .call(this.yAxis);

        window.GraphGlobals.SyncMovement = false;
        this.zoomFunc.translate ( [0,0] );
        this.zoomFunc.scale( 1 );
        this.attached = false;
        this.draw();
        this.zoom( this );
                		 
		var labelx = this.svg.append("text")      // text label for the x axis
			.attr("x", this.height / 2 )
			.attr("y", 30 +(this.height) ) // true -y
			.attr("class", "badge")
			.style("text-anchor", "middle")
			.text("P1 : "+this.paramOne); 

		this.svg.append("rect")
			.attr("x", labelx.node().getBBox().x)
			.attr("y", labelx.node().getBBox().y)
			.attr("rx", 5)
			.attr("ry", 5)
            .attr("class", "cadre")   
			.attr("width", labelx.node().getBBox().width)
			.attr("height", labelx.node().getBBox().height)
			.style("fill", "#ccc")
			.style("fill-opacity", ".3")
			.style("stroke", "#666")    
			.style("stroke-width", "1px");   
		
		var labely = this.svg.append("text")
            .attr("transform", "rotate(-90)")
            .attr("x", 0 -(this.height / 2)) //true -y
            .attr("y",-50  ) // true x
            .attr("class", "badge")
            .attr("dy", "2em")
            .style("text-anchor", "middle")
            .text("P2 : "+this.paramTwo);

        this.svg.append("rect")
            .attr("transform", "rotate(-90)")
            .attr("x", labely.node().getBBox().x)
            .attr("y", labely.node().getBBox().y)
            .attr("rx", 5)
            .attr("ry", 5)
            .attr("class", "cadre")
            .attr("width", labely.node().getBBox().width)
            .attr("height", labely.node().getBBox().height)
            .style("fill", "#ccc")
            .style("fill-opacity", ".3")
            .style("stroke", "#666")    
            .style("stroke-width", "1px");

        var zoomLabel = this.svg.append("text")      // text label for the x axis
                .attr("x", this.height - 4 )
                .attr("y", 30 +(this.height) ) // true -y
                .attr("id", "zoomLabel")
                .style("text-anchor", "end")
                .style("font-size" , "11px" )
                .text("Zoom: " + this.zoomFunc.scale().toFixed(2) );    

        // legends
		this.svg.append("rect")
          .attr("x", 25)
          .attr("y", -13 )
          .attr("class", "legend")
          .attr("width", 10)
          .attr("height", 10)
          .style("fill", this.pointColour(this.min_score));
     
        this.svg.append("text")
          .attr("x", 38 )
          .attr("y",  -5 )
          .attr("class", "legend")
          .attr("height",30)
          .attr("width",100)
          .style("fill", this.pointColour(this.min_score))
          .text(this.min_score.toFixed(2));
          
        this.svg.append("rect")
          .attr("x", 120)
          .attr("y", -13 )
          .attr("class", "legend")
          .attr("width", 10)
          .attr("height", 10)
          .style("float", "left")
          .style("fill", this.pointColour(this.max_score));
     
        this.svg.append("text")
          .attr("x", 133)
          .attr("y",  -5 )
          .attr("class", "legend")
          .attr("height",30)
          .attr("width",100)
          .style("fill", this.pointColour(this.max_score))
          .text(this.max_score.toFixed(2));  
    }
    
    //this is our function for zooming
    this.zoom = function zoom( that ) {

        if ( this.loading == true )
            return;

        this.svg.select("#zoomLabel").text( "Zoom: " + this.zoomFunc.scale().toFixed(2) );
        
        if ( this.attached == false ){
            this.svg.select(".x.axis").call(this.xAxis);
            this.svg.select(".y.axis").call(this.yAxis);
        }

        if ( this.attached == false ){
            this.svg.select('#circles1').attr("transform",
                "translate(" + that.zoomFunc.translate() + ")" +
                "scale(" + that.zoomFunc.scale() + ")"
            );

        }else{
            
            this.svg.select('#circles2').attr("transform",
                "translate(" + that.zoomFunc.translate() + ")" +
                "scale(" + that.zoomFunc.scale() + ")"
            );
        }

        this.svg.selectAll("circle").attr("r" , 5 / this.zoomFunc.scale() );

        var break_layer = Math.floor(this.zoomFunc.scale()) % 2;
        var layer = Math.floor(this.zoomFunc.scale());
        var dir = this.zoomFunc.scale() - this.last_scale;
        var change_layer = false;
        var first = false;

        if ( layer == 1 )
            break_layer = 0;


        if ( break_layer == 0 ){

            if ( layer != this.last_layer && dir > 0 ){

                change_layer = true
                this.last_layer = layer;

            }else if ( dir < 0 && layer != this.last_layer ) {

                this.last_layer = layer;
                change_layer = true;

            }else if ( dir < 0 && layer == 1 && layer != this.last_layer ){

                this.last_layer = layer;
                change_layer = true;
            }else if ( dir < 0 && this.zoomFunc.scale() == 1 ){
                this.last_layer = 1;
                change_layer = true;
                first = true;
            }
        }

        this.last_scale = that.zoomFunc.scale();

        var x = this.x.domain()[0];
        var y = this.y.domain()[0];
        var width = (this.x.domain()[1]) - x;
        var height = (this.y.domain()[1]) - y;

        var dist = Math.sqrt( Math.pow( (x - this.last_center[0]) , 2) + Math.pow( (y - this.last_center[1]) , 2 ) ) * this.zoomFunc.scale();

        //make sure we are not currently loading anything before 
        //we get more points
        if ( this.loading == false && change_layer == true ){
            
            //get our new points
            try {

                if ( first == false ){
                    this.getPoints( x , y , width , height );
                }else{
                    this.getPoints( this.min_vx , this.min_vy , this.max_vx - this.min_vx , this.max_vy - this.min_vy );
                }
            }catch ( e ){
                console.log("ERROR: Fetching points!");
            }
        }else{

            if ( layer == this.last_layer && dist > 4 && layer > 1 ){
                this.last_center = [ x , y ]; 
                this.getPoints( x , y , width , height );
            }
        }
    }
    
    /* This is used to filter our graph */
    this.filterGraph = function filterGraph( min , max ) {

        /* Make sure that we are not loading first */
        if ( this.loading == true )
            return;

        var that = this;

        //we are loading lol
        this.setLoading(true);

        //this part of the object will now make a silent ajax request to get the points from the java program and then load
        // into our storage structure
        $.ajax( { type: "POST" , url : "/ajax/filterGraph" , data: { "user_id" : this.user_id , "objective" : this.objective , "paramTwo" : this.paramTwo  , "paramOne" : this.paramOne , "job_id" : this.job_id , "auth_token" : this.auth_token , "min" : min , "max" : max  } 
        , success: function(json) { 
            
            //we are done loading now
            that.setLoading ( false );

            //redraw our graph after we get the points
            that.getPoints( that.min_vx , that.min_vy , that.max_vx - that.min_vx , that.max_vy - that.min_vy );

        } ,
        error : function( json ) {
            that.loading = false;
            
            console.log("ERROR: Could not filter the graph.");
        } } );
    }
    
    /* This function is used to reset the graph */
    this.reset = function reset() {
        
        var that = this;
        
        //reset in a nice smooth way
        that.zoomFunc.translate([0,0]);
        that.zoomFunc.scale(1);
        that.zoom(that);
    }
    
    /*This function is used to change the zoom*/
    this.alterZoom  = function alterZoom( factor , dir ){
        var zoom = this.zoomFunc.scale() * factor;
        
        if ( zoom < this.zoomFunc.scaleExtent()[0] || zoom > this.zoomFunc.scaleExtent()[1] ){
            console.log("max zoom out");
            return false;
        }
		factor = 0.1;
		var target_zoom = 1,
			center = [this.width / 2, this.height / 2],
			extent = this.zoomFunc.scaleExtent(),
			translate = this.zoomFunc.translate(),
			translate0 = [],
			l = [],
			view = {x: translate[0], y: translate[1], k: this.zoomFunc.scale()};

		target_zoom = this.zoomFunc.scale() * (1 + factor * dir);

		if (target_zoom < extent[0] || target_zoom > extent[1]) { return false; }

		translate0 = [(center[0] - view.x) / view.k, (center[1] - view.y) / view.k];
		view.k = target_zoom;
		l = [translate0[0] * view.k + view.x, translate0[1] * view.k + view.y];

		view.x += center[0] - l[0];
		view.y += center[1] - l[1];
        
        this.zoomFunc.translate( [view.x, view.y] );
        this.zoomFunc.scale( view.k );
    }
    /////////////////////////////////////////////    
    
    
    /* This function is used to zoomIn on the graph */
    this.zoomIn = function zoomIn() {
        //this.alterZoom ( 1.1  , 1 );
        this.alterZoom ( 1.1  , 1 );
        this.zoom( this );
    }
    
    /* This function is used to zoomIn on the graph */
    this.zoomOut = function zoomOut() {
        this.alterZoom ( 0.9  , -1 );
        this.zoom( this );
    }
    
    /* This function is used to move our graph */
    this.move = function move ( translate ){
        var tNew = this.zoomFunc.translate();
        tNew[0] += (translate[0]*this.zoomFunc.scale());
        tNew[1] += (translate[1]*this.zoomFunc.scale());
        this.zoomFunc.translate ( tNew );
        this.zoom ( this );
    }
    
    /* This function is used to remove the graph */
    this.remove = function remove (){
        $('#panel-'+ this.panelID + '> .panel-body > #scatter-plot' + this.id ).empty();

        var that = this;
        $('#panel-'+ this.panelID + '> .panel-body > #scatter-plot' + this.id).css("width","0px");
        $('#panel-'+ this.panelID + '> .panel-body > #scatter-plot' + this.id).css("height","0px");


        window.GraphGlobals.Graphs = jQuery.grep( window.GraphGlobals.Graphs , function(value) {
            return value != that;
        });
    }

    /* This function is used to quickly set the graph to the loading state */
    this.setLoading = function setLoading( loading ) {

        if ( loading == true ){
            this.loading = true;
            $('#panel-'+ this.panelID + '> .panel-body > #scatter-plot' + this.id).css('background-image', 'url("/img/loader.gif")' );
        }else{
            this.loading = false;
            $('#panel-'+ this.panelID + '> .panel-body > #scatter-plot' + this.id).css('background-image', '' );
        }
    }


    this.reload = function reload(){
        
        // initialisation of the graph's size
        if (window.innerWidth && window.innerHeight) {
            this.visible_width = 0.9*(window.innerWidth)*512/1366;
            this.visible_height = this.visible_width;
        }

        d3.select(this.svg.node().parentNode).attr("width", this.visible_width );
        d3.select(this.svg.node().parentNode).attr("height", this.visible_height );

        if ( this.attached == false ){
            $('#panel-'+ this.panelID + '> .panel-body > #scatter-plot' + this.id).width( this.visible_width );
            $('#panel-'+ this.panelID + '> .panel-body > #scatter-plot' + this.id).height( this.visible_height );
        }
    }
       
}
