/*
 * This function is used to dynamically create a panel for holding graphs
*/
//This is used for defining our glab variables
window.PanelGlobals  = {
        Panels : [],
        CurrentId : 0,
        CurrentPanel : null
};

function Panel () {

    /* when we are instiantiated we need to call the init function */
    //this.init();
    this.id = 0;
    this.panel = null;
    this.graphOne = null;
    this.graphTwo = null;
    this.selected = false;
    this.merged = false;
    this.lockedRight = false;
    this.lockedLeft = false;


    /* This function is used for initatiating our panel */
    this.init = function init() {

        //first of all we need to insert out new blank panel
        this.id = window.PanelGlobals.CurrentId++;

        //then what we need to do is add out panel to windows global
        window.PanelGlobals.Panels.push( this );

        //now lets create our actual panel <a id="tab-'+ this.id +'">
        $("#graph-panels").append( $('<a class="anchor" id="tab-'+ this.id +'">&nbsp;</a>') , $('<div class="panel panel-default graph-panel" id="panel-' + this.id + '" ></div>').append(
                                   $('<div class="panel-body" ><div class="scatter-plot" id="scatter-plot1" ></div>' +
                                          '<div class="padlocks" id="padlock'+ this.id +'" ><button id="lock-right" class="btn btn-default btn-xs" ><i class="fa fa-lock"></i> <span class="glyphicon glyphicon-arrow-right" ></span></button><br/><br/>' +
                                          '<button id = "lock-left" class="btn btn-default btn-xs" ><span class="glyphicon glyphicon-arrow-left" ></span> <i class="fa fa-lock"></i></button></div>' +
                                          '<div class="scatter-plot" id="scatter-plot2" ></div></div>') ));

        $("#panel-" + this.id ).hide().fadeIn( 300 );

        //add our tab
        this.addTab();

        //get our panel
        this.panel = $("#panel-" + this.id ).get(0);

        //add our event for selecting our panel
        var that = this;

        // Set our panel manipulation functions .children('.panel-body')
        $( this.panel ).click( function() { that.select(); } );
        $( this.panel ).children('.panel-heading').children('#remove').click( function() { that.remove(); } );
        $( this.panel ).children('.panel-heading').children('#duplicate').click( function() { that.duplicate(); } );

        //remember and hide our help thing
        $('#tutorial').hide();

    }

    /* This function is used to add a graph to our panel*/
    this.addFirstGraph = function addFirstGraph( job_id , user_id , objective , paramOne , paramTwo  , auth_token  ){

        //create a new graph and add it to the proper location
        if ( this.hasFirstGraph() == false ){
            this.graphOne = new Graph( job_id , user_id , objective , paramOne , paramTwo  , auth_token , 1 , this.id )
            this.graphOne.init();
        }

    }

    /* This function is used to add a graph to our panel*/
    this.addSecondGraph = function addSecondGraph( job_id , user_id , objective , paramOne , paramTwo  , auth_token  ){

        //create a new graph and add it to the proper location
        if ( this.hasSecondGraph() == false ){
            this.graphTwo = new Graph( job_id , user_id , objective , paramOne , paramTwo  , auth_token , 2 , this.id )
            this.graphTwo.init();
        }

    }

    /* This function is used to say if we have graphOne set already */
    this.hasFirstGraph = function hasOneGraph(){
        return ( this.graphOne != null );
    }

    /* This function is used to say if we have graphTwo set already */
    this.hasSecondGraph = function hashTwoGraph(){
        return ( this.graphTwo != null );
    }

    /* This function is used for removing the second graph */
    this.removeFirstGraph = function removeFirstGraph(){

        if ( this.hasFirstGraph && this.merged == false ){
            this.graphOne.remove();
            this.graphOne = null;
        }
    }

    /* This function is used for removing the second graph */
    this.removeSecondGraph = function removeSecondGraph(){

        if ( this.hasSecondGraph && this.merged == false ){
            this.graphTwo.remove();
            this.graphTwo = null;
        }
    }

    /* This function is used to merge two graphs */
    this.mergeFirstToSecond = function mergeFirstToSecond(){
        if ( this.merged == false && this.hasFirstGraph() && this.hasSecondGraph() ){
            this.graphTwo.attach( this.graphOne );
            this.merged = true;
        }
    }

    /* This function is used to merge two graphs */
    this.mergeSecondToFirst = function mergeSecondToFirst(){
        if ( this.merged == false && this.hasFirstGraph() && this.hasSecondGraph() ){
            this.graphOne.attach( this.graphTwo );
            this.merged = true;
        }
    }

    /* This function is used to split the graphs from each other */
    this.split = function split(){
        this.graphOne.dettach();
        this.graphTwo.dettach();
        this.merged = false;
    }

    /* This function is used to update the first graph */
    this.updateFirstGraph = function updateFirstGraph( job_id , user_id , objective , paramOne , paramTwo  , auth_token  ){
        if ( this.hasFirstGraph() && this.merged == false ){

            if ( this.graphOne.paramOne != paramOne || this.graphOne.paramTwo != paramTwo ){
                this.graphOne.remove();
                this.graphOne = new Graph( job_id , user_id , objective , paramOne , paramTwo  , auth_token , 1 , this.id )
                this.graphOne.init();
            }else{
                this.graphOne.setObjective ( objective );
            }
        }
    }

    /* This function is used to update the first graph */
    this.updateSecondGraph = function updateSecondGraph( job_id , user_id , objective , paramOne , paramTwo  , auth_token  ){
        if ( this.hasSecondGraph() && this.merged == false ){

            if ( this.graphTwo.paramOne != paramOne || this.graphTwo.paramTwo != paramTwo ){
                this.graphTwo.remove();
                this.graphTwo = new Graph( job_id , user_id , objective , paramOne , paramTwo  , auth_token , 2 , this.id )
                this.graphTwo.init();
            }else{
                this.graphTwo.setObjective ( objective );
            }
        }
    }

    /* This function is used to filter our graphs */
    this.filterGraphs  = function filterGraphs ( min , max ){

        if ( this.hasFirstGraph() == true )
            this.graphOne.filterGraph( min , max );

        if ( this.hasSecondGraph() == true )
            this.graphTwo.filterGraph( min , max );
    }

    /* This function is used to set the object of the first graph */
    this.remove = function remove() {
        
        /* Remove our graphs etc */
        this.removeTab();
        if ( this.hasFirstGraph() )
            this.graphOne.remove();

        if ( this.hasSecondGraph() )
            this.graphTwo.remove();

        /* Remove our panel from the list of panels */
        var that = this;
        window.PanelGlobals.Panels = jQuery.grep( window.PanelGlobals.Panels , function(value) {
            return value != that;
        });

        $(this.panel).fadeOut(500, function() { 
                $(that.panel).remove();

                if ( window.PanelGlobals.Panels.length == 0 ){
                    $('#tutorial').show();
                }
        });

        if ( this.selected == true )
            window.PanelGlobals.CurrentPanel = null;
    }

    /* This function is used to duplibcate the panel */
    this.duplicate = function duplicate() {
        console.log("Duplicate");
    }

    /* This function is used to unlock our panels */
    this.unlock = function unlock(){

        this.lockedLeft = false;
        this.lockedRight= false;
        window.GraphGlobals.SyncMovement = false;
    }

    /* This function is used */
    this.lockRight = function lockRight(){

        if ( this.hasFirstGraph() == true && this.hasSecondGraph() == true ){
            this.lockedRight = true;
            window.GraphGlobals.SyncMovement = true;
            this.graphOne.zoomFunc.event(this.graphOne.svg);
        }
    }

    /* This function is used to lock left */
    this.lockLeft = function lockLeft() {

        if ( this.hasFirstGraph() == true && this.hasSecondGraph() == true ){
            this.lockedLeft = true;
            window.GraphGlobals.SyncMovement = true;
            this.graphTwo.zoomFunc.event(this.graphTwo.svg);
        }
    }

    /* This function is used to add our tab for this panel */
    this.addTab = function addTab(){

        $("#graph-tabs").append( '<div class="btn-group" id="tab-g-'+this.id+'" >'+
                            '<button class="btn btn-danger btn-xs tab-remove lm_tooltip" data-toggle="tooltip" data-placement="bottom" title="Remove this tab" id="' + this.id + '" ><span class="glyphicon glyphicon-remove" ></span></button>'+
                            '<a class="btn btn-default btn-xs tab-panel" id="' + this.id + '" href="#tab-' + this.id + '" > Panel '+ this.id +'</a></div>  ' );
    }

    /* This function is used to remove the tab from the tabs */
    this.removeTab = function removeTab(){
        $("#tab-g-" + this.id).remove();
    }

    this.select = function select() {

        if ( this.selected == false ){

            if ( window.PanelGlobals.CurrentPanel != null )
                window.PanelGlobals.CurrentPanel.selected = false;

            $(".graph-panel").attr("class" , "panel panel-default graph-panel" );
            $ (this.panel ).attr("class" , "panel panel-primary graph-panel" );

            this.selected = true;
            window.PanelGlobals.CurrentPanel = this;
        }

        if ( this.lockedLeft == false && this.lockedRight == false ){

            if ( this.merged == false ){
                window.GraphGlobals.SyncMovement = false;
            }else{
                window.GraphGlobals.SyncMovement = true;
            }

        }else{

            window.GraphGlobals.SyncMovement = true;
        }

        $('.tab-panel').attr('class' , 'btn btn-default btn-xs tab-panel' );
        $('#tab-g-'+this.id+' > .tab-panel').attr('class' , 'btn btn-primary btn-xs tab-panel' );

        if ( window.PanelGlobals.CurrentPanel.merged == true ){
            $(".merge-btn").prop('disabled',true);
            $("#split").prop('disabled',false);
            $("#remove").attr("disabled",true);
            $("#ok2").attr("disabled",true);
            $("#ok").attr("disabled",true);
            $(this.panel).find("#padlock" + this.id).css('display' , 'none' );
        }else{
            $(".merge-btn").prop('disabled',false);
            $(this).prop('disabled',true);
            $("#remove").attr("disabled",false);
            $("#ok2").attr("disabled",false);
            $("#ok").attr("disabled",false);

            if ( this.hasFirstGraph() == true && this.hasSecondGraph() == true ){

                if ( this.merged == false ){
                    $(this.panel).find("#padlock" + this.id).css('display' , 'inline-display' );
                }else{
                    $(this.panel).find("#padlock" + this.id).css('display' , 'none' );
                }
            }else{
                $(this.panel).find("#padlock" + this.id).css('display' , 'none' );
            }
        }
    }

}